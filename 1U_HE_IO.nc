#include <ADDRDEFS.H>
#include <ACCESS.H>
#include <NETMGMT.H>
#include <access.h>
#include <control.h>
#include <snvt_rq.h>
#include <msg_addr.h>
#include <addrdefs.h>
#include <stdlib.h>
#include <io_types.h>
#include <string.h>
#include <float.h>
#include "HECommandCat.h"
#include "HEResult.h"
#include "OPSubCommand.h"
#include "OPCommandCat.h"
#include "OPGroupSelect.h"
#include "MasterResult.h"
#include "HE_FaultType.h"

#pragma disable_warning 174
#pragma disable_warning 310
#pragma scheduler_reset

#pragma num_alias_table_entries 1
#pragma set_node_sd_string "&3.1@0Node Obj,20000NM,20021HE"

#pragma app_buf_out_count 11
#pragma net_buf_out_count 11

#pragma app_buf_in_count 11
#pragma net_buf_in_count 11

// ***********TO DO
//Should have input nvs for oprequest to Catch pages and also gave messagepkay in. 

#define SM2_VER_MAJOR 3
#define SM2_VER_MINOR 0
#define SM2_VER_PATCH 2

//******************************Current Software version *****************
#define NEURON_SW_VERSION_MAJOR 1
#define NEURON_SW_VERSION_MINOR 4
#define NEURON_SW_VERSION_PATCH 6
#define NEURON_VER_ENUM 4
//************************************************************************
//Revision history
//Version 0.0.1
//************************************************************************
//V 0.0.2
//
//Version 1.01	First production release
//1.1 Added repating mute masking. Added fault relay
//1.2 Added support for nform. Added rev b pcb support. Added ver and model read
//1.3 Added support for sending fault info on nform. Also sending swith status to MPI in nform
//1.4 added ups fault inputs
//1.4.1	added support for nform facp multi zone page
//1.4.2 Adedd mute music to mute masking. Adjusted interval between mute masking and mute music. Fixed issue related to loss of ethernet cable and mac id. Changed multisource io's to 5 from 6
//1.4.3 Added neuron version to serial send to protocessoras an enum
//1.4.4 Added combo unit functionality
//1.4.5 Added support for quick setup-per dan
//1.4.6 Removed supervision setup for all IO for combo
//Todo 
// max op should be 199
// add pw
// add IP

//version enums
//0=1.4.2
//1=1.4.3
//2=1.4.4
//3=1.4.5
//4=1.4.6


//#define INET_VERSION

#define DEBUG_MESSAGES FALSE
//#define COMBO_UNIT TRUE

#define MAX_CHANNELS 4

#define DEVICE_ID_KEYPAD 1
#define DEVICE_ID_MPI 2
#define DEVICE_ID_MASTERSS 3
#define DEVICE_ID_SLAVESS 4
#define DEVICE_ID_LOC 5
#define DEVICE_ID_OP 6
#define DEVICE_ID_ILON 7
#define DEVICE_ID_1UHE_IO 8
#define DEVICE_ID_SMART_RTR 9
#define DEVICE_ID_TALKBACK 10

// device ids greater than 50 are OP's op1=51

#define CONFIG_FETCH_DELAY 100

#define OP_WAIT_VOL 200

#define LCD_BLUE 0x04
#define LCD_GREEN 0x02
#define LCD_RED 0x01
#define DELAY37USEC 1
#define DELAY1POINT52MS 2
#define MAX_COLUMNS 16

//superbounce must be less than 10, because we use this for facp faultheartbeat
#define SUPER_BOUNCE 2



//i2c Bus's
#define BUS_DIRECT 0
#define BUS_A 1
#define BUS_B 2
#define BUS_C 3
#define BUS_D 4
#define BUS_E 5
#define BUS_F 6
#define BUS_G 7
#define BUS_H 8


// I2C addresses
#define CONTROL_ADDR 0x27 // address of the LCD control Interface
#define DATA_ADDR 0x26 // address of the LCD Data Interface
#define I2C_MUX_SW_ADDR 0x77	//address of i2c mux switch
#define FX_AUDIO_ADDR 0x25
#define POT_ADDR 0x3c

#define DIG_IN_LED_ADDR 0x21
#ifdef COMBO_UNIT
	#define AUX_RELAY_ADDR 0x25
	#define CFG_FLED_LED_ADDR 0x21
	#define CFG_BUS BUS_B
	#define VOLTAGE_A2D_ADDR 0x4b
	#define VOLT_IO_A2D_ADDR 0x48
	#define VOLTAGE_BUS BUS_B
#else
	#define AUX_RELAY_ADDR 0x20
	#define CFG_FLED_LED_ADDR 0x22
	#define CFG_BUS BUS_A
	#define VOLTAGE_A2D_ADDR 0x4b
	#define VOLTAGE_BUS BUS_A
#endif
#define LCD_IO_ADDR_1 0x20
#define LCD_IO_ADDR_2 0x21

//inputs were backwards left to right
//#define DIG_IN_A_ADDR	0x48

#ifdef COMBO_UNIT
	#define DIG_IN_A_ADDR	0x48
#else
	#define DIG_IN_A_ADDR	0x4a
#endif

#define DIG_IN_B_ADDR	0x49
#define DIG_IN_C_ADDR	0x48
//#define DIG_IN_C_ADDR	0x4a
#define UART_ADDR 0x57



#define KEY_MODE 0x77
#define KEY_MUTE 0xb7
#define KEY_UP 0xd7
#define KEY_UNMUTE 0xE7
#define KEY_FUNCTION 0x7b
#define KEY_LEFT 0xbb
#define KEY_ENTER 0xDB
#define KEY_RIGHT 0xeb
#define KEY_CHANNEL 0x7d
#define KEY_ZONE 0xBD
#define KEY_DOWN 0xdd
#define KEY_CLEAR 0xed
#define KEY_NONE 0xff

#define KEY_SCAN_NORMAL 200
#define KEY_SCAN_SLOW 400

#define NO_INPUT_TIMEOUT 30

#define OPRESPONSE_INDEX 5

//HE Setup subcommands
#define READBACK_SETTINGS 10
#define WRITE_SETTINGS 11

#define MASTER_REQ_SEL_HI 0x10
#define MASTER_REQ_SEL_LOW 0x00
#define KEYPAD_FLT_SEL_HI 0x10
#define KEYPAD_FLT_SEL_LOW 0x02
#define ILON_OP_REQ_SEL_HI 0x04
#define ILON_OP_REQ_SEL_LOW 0x00
#define OP_TEMP_FAULT_SEL_HI 0x04
#define OP_TEMP_FAULT_SEL_LOW 0x03
#define OP_SPKR_FAULT_SEL_HI 0x04
#define OP_SPKR_FAULT_SEL_LOW 0x04
#define OP_AMP_FAULT_SEL_HI 0x04
#define OP_AMP_FAULT_SEL_LOW 0x05
#define MASTER_KEY_REQ_SEL_HI 0x10
#define MASTER_KEY_REQ_SEL_LOW 0x03
#define KEYPAD_REQ_SEL_HI 0x10
#define KEYPAD_REQ_SEL_LOW 0x04
#define KEYPAD_RSP_SEL_HI 0x10
#define KEYPAD_RSP_SEL_LOW 0x05
#define FAULT_PING_SEL_HI 0x12
#define FAULT_PING_SEL_LOW 0x00
#define NEURON_FAULT_HIGH 0x10
#define NEURON_FAULT_LOW 0x02

#define HE_LISTENER_SUBNET 1
#define HE_LISTENER_NODE 20


#define FX_MESSAGE_1 0xfe
#define FX_MESSAGE_2 0xfd
#define FX_MESSAGE_3 0xfb


#define LED_1A_MASK 0x40
#define LED_2A_MASK 0x80
#define LED_3A_MASK 0x10
#define LED_4A_MASK 0x20
#define LED_1B_MASK 0x04
#define LED_2B_MASK 0x08
#define LED_3B_MASK 0x01
#define LED_4B_MASK 0x02
#define LED_1C_MASK 0x40
#define LED_2C_MASK 0x80
#define LED_3C_MASK 0x10
#define LED_4C_MASK 0x20


#define LED_RLY1_MASK 0x04
#define LED_RLY2_MASK 0x08
#define LED_RLY3_MASK 0x01

#ifdef COMBO_UNIT
	#define LED_AUX_MASK 0x08
	#define LED_CPUF_MASK 0x10
	#define LED_GENF_MASK 0x20
	#define LED_POWER_MASK 0x02
#else
	#define LED_AUX_MASK 0x02
	#define LED_CPUF_MASK 0x40
	#define LED_GENF_MASK 0x80
#endif


#define LED_SER_MASK 0x20
#ifdef COMBO_UNIT
	#define RELAY1_MASK 0x08
#else
	#define RELAY1_MASK 0x01
	#define RELAY2_MASK 0x02
	#define RELAY3_MASK 0x04
#endif


//#define RELAY1_MASK 0x04
//#define RELAY2_MASK 0x02
//#define RELAY3_MASK 0x01

//a2d selectors
//#define A2D_V1POINT5 0
#ifdef COMBO_UNIT
	#define A2D_V3POINT3 0
	#define A2D_V5 4
	#define A2D_V1POINT26 1
	#define A2D_V15 5
	#define A2D_V3POINT3_2 2
	#define A2D_V5_2 6
	#define A2D_V1POINT26_2 3
	#define A2D_V15_2 7
	#define A2D_V3POINT3_H 17
	#define A2D_V5_H 16
	#define A2D_V5_ISO_H 13
	#define A2D_VM5 12
#else
	#define A2D_V3POINT3 0
	#define A2D_V5 4
	#define A2D_V_ISO5 1
	//#define A2D_VMINUS5 2
	#define A2D_V24 5
#endif


#define MUTE_TYPE_MASK 1
#define MUTE_TYPE_MUSIC 2



void SetupI2C(void);
void SetupI2C2(void);

void DefaultDisplay(void);
void WaitMessage(void);
void SendCommand(unsigned int databyte);
void SendData(unsigned int databyte);
void LCD_String(unsigned row, unsigned col);
void WriteLCDControl(void);
boolean SelectBus(short Bus);
void ClearDisplay(void);
void CPUFault(unsigned int Neuron,unsigned int Fault);
void ClearFaults(unsigned int Neuron);
void CPUFaultLED(unsigned int State);
void GenFault(unsigned int Neuron,unsigned int Fault);
void GenFaultLED(unsigned int State);
void LaunchFault(void);
void SendListenerFault(unsigned FaultType,unsigned Neuron, unsigned Fault);
void FaultAlert(void);
unsigned int GetNextCPUFaultDown(void);
unsigned int GetNextCPUFaultUp(void);
void StartMessage(void);
int ConvNumToBuf(long Num, int places,int decpos);
void ProcessSw(enum SWITCHES sw);
boolean DebouncePass(unsigned short Device,unsigned short ioIndex);
void ProcessSuperInputs(void);
void GetSupervisedInputs(void);
void BounceWatch(unsigned short Device,unsigned short ioIndex);
void SetGenFault(unsigned int SwIndex,unsigned int Device, unsigned int Fault);
boolean InputSupervised(unsigned short Device,unsigned short Index);
void SendDebug(unsigned DebugByte,unsigned DataByte);
void UpdateLEDStates(unsigned Led, boolean State);
//void WriteAuxRelaysLEDs2(unsigned Led, boolean State);
void WriteAuxRelays(unsigned Led, boolean State);
unsigned short ReadRelays(void);
unsigned short ReadSettingsIO(void);
void PaintLeds(void);
void SetSwFault(unsigned short Device, unsigned short SwIndex, boolean State);
void Switch2Led(unsigned short Device, unsigned short SwIndex, boolean State);
void WriteFaultSerLeds(unsigned Led, boolean State);
void PaintMaskVolume(void);
void PaintMaskContour(void);
void PaintMaskMute(void);
void PaintMusicVolume(void);
void PaintMusicMute(void);
void PaintPageVolume(void);
void PaintPageMute(void);
void PaintIP_Address(void);
void PaintVoltages(void);
void PaintFaults(void);
void PaintUnitNumber(void);
void PaintFunctions(void);
void FunctionChange(void);
void PaintOPNum(void);
void PaintZoneNum(void);
void PaintOPOption(void);
void PaintIPOption(void);
void PaintSubOptionBlank(void);
void PaintChNum(void);
void PaintMaskdbVol(short val);
void PaintMaskContdb(short val);
void PaintPageVoldb(short val);
void PaintMusicVoldb(short val);
void OptionChange(void);
void PaintSelection(void);
void SelectChange(void);
void PaintMute(boolean state);
void ItemUp(void);
void IncreaseZoneOP(void);
void DecreaseZoneOP(void);
void IncreaseChannel(void);
void DecreaseChannel(void);
void PaintVoltageValues(void);
void PaintCurrentFault(void);
void ItemDown(void);
void SetupUart(void);
void FetchResult(void);
void FetchNv(int NVIndex);
void ProcessNV(void);
void DecodeVolumes(int Command);
void SendGetVolumes(int Command);
void SendGetContours(void);
void DecodeContours(void);
void SendOPMess(void);
void SetupWait(void);
void PaintCommError(void);
int NextTx(void);
void GetOPValues(void);
void SendMaskingVol(unsigned OP,unsigned Ch, unsigned Vol);
void SendMaskingCont(unsigned OP,unsigned Ch, unsigned Cont);
void SendMusicVol(unsigned OP,unsigned Ch, unsigned Vol);
void SendPageVol(unsigned OP,unsigned Ch, unsigned Vol);
void SendMaskingMute(unsigned OP,unsigned Ch, unsigned Mute);
void SendTestTone(unsigned OP,unsigned Ch, unsigned Tone);
void SendPageMute(unsigned OP,unsigned Ch, unsigned Mute);
void SendMusicMute(unsigned OP,unsigned Ch, unsigned Mute);
void PaintTestToneState(boolean state);
void PaintTestTone(void);
void Functions(void);
void SilenceAlarm(void);
void ClearCurrentFault(void);
void ClearFaultAlarm(void);
boolean InputNormal(unsigned short Device,unsigned short Index);
void PaintIO(void);
void PaintIOMode(enum IO_MODES mode);
void PaintIONum(void);
void DecreaseIOCh(void);
void IncreaseIOCh(void);
void SendGlobalMute(boolean MuteState,unsigned MuteType);
void PaintVersions(void);
void PaintVerOption(void);
void GlobalMute(boolean MuteState);
void PlayFX(unsigned int Message);
boolean WriteFXAudio(unsigned Value);
boolean WriteFXAudio2(void);
boolean ReadFXPlayContact(void);
void SetAmpMute(boolean Mute);
void CheckVoltageRanges(void);
void GetVoltage(unsigned Selector);
void PreInstall(void);
boolean CheckFaultFlags(void);
void SendAlarmHEKP(void);
void FunctionChangeInet(void);
void FunctionChangeNform(void);
void SendMPICommand(unsigned SwitchNum,unsigned SwitchState);
void SetForSend(unsigned short Device,unsigned short SuperIndex,unsigned short SwState);
void CheckSendIO(void);
unsigned short SwitchNumber(unsigned short Device, unsigned short SuperIndex);
void PingSMC(void);
void GetSMCAddress(void);
void DecodeSerialData(void);
void HexToStr(unsigned short Value);
void SendOpsFireZones(void);
void ClearSwitchStates(void);
boolean WriteMCP45HV(short Address,short Bus, unsigned short Pot);
void PaintRCAVal(void);
void PaintRCA(void);
void ReadLCDBits(enum OPS OP);
char LookupLCD(unsigned short segments);
void PaintLCD_Info(void);



/*
void DecodeSetupCommand(void);
void SendHEFeedback(boolean result);
void ReadKeyPad(void);
void WriteAuxRelays(unsigned Relay, boolean State);
//boolean WriteAuxRelays(unsigned Value);

boolean KeyEnabled(unsigned  Key);
void FunctionChange(void);


boolean Initialize(void);
void PreInstall(void);
void EnterClick(void);
void PaintFaults(void);
void ClearClick(void);
void ClearCurrentFault(void);
void ClearFaultAlarm(void);
void SilenceAlarm(void);
*/


#define RELAY_OFF 0
#define RELAY_ON 1
#define LED_OFF 0
#define LED_ON 1
#define TONE_OFF 0
#define TONE_ON 1

typedef struct OPChInfo {
unsigned short MaskVol;
unsigned short MaskContour;
unsigned short PageVol;
unsigned short MusicVol;
boolean MaskMute;
boolean PageMute;
boolean MusicMute;
boolean TestTone;
} OPChInfo;

enum OPS
{
	OP_1=0,
	OP_2
};

enum SUPER_IO_STATES
{
	CONTACT_OPEN=0,
	CONTACT_CLOSED,
	SHORT_C1_TO_C2,
	SHORT_C1_TO_GND,
	SHORT_C2_TO_GND,
	OPEN,
	UNKNOWN_IO,
	SENT_IO_UPDATE
};

enum SELECT_LOCATION
{
	SELECT_OPTION,
	SELECT_SUBOPTION,
	SELECT_VALUE
};

enum RELAYS
{
	RELAY_ALL=0,
	RELAY_1,
	RELAY_2,
	RELAY_3,
	RELAY_4
};

enum SWITCHES
{
	SWITCH_NONE=0,
	SWITCH_1,
	SWITCH_2,
	SWITCH_3,
	SWITCH_4,
	SWITCH_5
};

enum LEDS
{
	LED_ALL=0,
	LED_FTR_1A,
	LED_FTR_1B,
	LED_FTR_1C,
	LED_FTR_2A,
	LED_FTR_2B,
	LED_FTR_2C,
	LED_FTR_3A,
	LED_FTR_3B,
	LED_FTR_3C,
	LED_FTR_4A,
	LED_FTR_4B,
	LED_FTR_4C,
	LED_RLY1,
	LED_RLY2,
	LED_RLY3,
	LED_AUX,
	LED_CPU_F,
	LED_GEN_F,
	LED_SERIAL,
	LED_POWER
};


enum MODES
{
	DIAG_MODE=0,
	STARTUP_MODE
};
#define NUM_MODES 2


enum DIAG_FUNCTIONS
{
	DIAG_VOLTS=0,
	DIAG_TEMPS,
	DIAG_AUDIO,
//	DIAG_REBOOT,
	DIAG_PORN_LOOP,
	DIAG_ALL_CALL,
	DIAG_AUDIO_S_LEV,
	DIAG_FAULTS
};
#define NUM_DIAG_FUNC 7

#ifdef COMBO_UNIT

	enum FUNCTIONS
	{
		MASK_VOL=0,
		MASK_CONTOUR_F,
		MASK_MUTE,
		MUSIC_VOL,
		MUSIC_MUTE,
		PAGE_VOL,
		PAGE_MUTE,
		IP_ADD,
		VOLTAGES,
		FAULTS,
		IO_SETUP,
		VERSIONS,
		RCA_VOLUME,
		LCD_INFO,
		TEST_TONE,
		UNIT_NUMBER
	};
	#define NUM_FUNC 15

#else

	enum FUNCTIONS
	{
		MASK_VOL=0,
		MASK_CONTOUR_F,
		MASK_MUTE,
		MUSIC_VOL,
		MUSIC_MUTE,
		PAGE_VOL,
		PAGE_MUTE,
		IP_ADD,
		VOLTAGES,
		FAULTS,
		IO_SETUP,
		VERSIONS,
		TEST_TONE,
		UNIT_NUMBER
	};
	#define NUM_FUNC 13

#endif

#define NUM_CPU_FAULTS 14

enum CPU_FAULTS
{
	NO_CPU_FAULTS=0,
	INITIALIZE,
	SUP_INPUTS_I2C,
	LCD_CONTROL_I2C,
	AUX_RELAY_I2C,
	CFG_FLED_I2C,
	I2C_MUX_SW_I2C,
	VOLTAGE_A2D_I2C,
	DIG_IN_LED_I2C,
	LCD_DATA_I2C,
	FX_AUDIO_I2C,
	VOLTAGE_RANGE,
	UART_I2C,
	POT_I2C
};

#define NUM_GEN_FAULTS 62

enum OP_OPTIONS
{
	OPTION_OP,
	OPTION_OP_ALL,
	OPTION_ZONE
		
};

enum IP_OPTIONS
{
	OPTION_ADDRESS,
	OPTION_SUBNET,
	OPTION_MAC,
	OPTION_GW
		
};

enum VER_OPTIONS
{
	OPTION_NEURON,
	OPTION_SM2
		
};

#ifdef COMBO_UNIT

	enum VOLTAGE_OPTIONS
	{
		V_15V,
		V_5V,
		V_126V,
		V_33V,
		V_5V_M,
		V_5V_2,
		V_5V_H,
		V_5V_ISO_H,
		V_126V_2,
		V_33V_2,
		V_33V_H
	};
#else
	enum VOLTAGE_OPTIONS
	{
		V_24V,
		V_5V,
		V_ISO_5V,
		V_33V
	};
#endif


enum GEN_FAULTS
{
	SIO_A1_SC1TOC2,
	SIO_A1_SC1TOGND,
	SIO_A1_SC2TOGND,
	SIO_A1_OPEN,
	SIO_A1_UNKNOWN,
	SIO_A2_SC1TOC2,
	SIO_A2_SC1TOGND,
	SIO_A2_SC2TOGND,
	SIO_A2_OPEN,
	SIO_A2_UNKNOWN,
	SIO_A3_SC1TOC2,
	SIO_A3_SC1TOGND,
	SIO_A3_SC2TOGND,
	SIO_A3_OPEN,
	SIO_A3_UNKNOWN,
	SIO_A4_SC1TOC2,
	SIO_A4_SC1TOGND,
	SIO_A4_SC2TOGND,
	SIO_A4_OPEN,
	SIO_A4_UNKNOWN,
	SIO_B1_SC1TOC2,
	SIO_B1_SC1TOGND,
	SIO_B1_SC2TOGND,
	SIO_B1_OPEN,
	SIO_B1_UNKNOWN,
	SIO_B2_SC1TOC2,
	SIO_B2_SC1TOGND,
	SIO_B2_SC2TOGND,
	SIO_B2_OPEN,
	SIO_B2_UNKNOWN,
	SIO_B3_SC1TOC2,
	SIO_B3_SC1TOGND,
	SIO_B3_SC2TOGND,
	SIO_B3_OPEN,
	SIO_B3_UNKNOWN,
	SIO_B4_SC1TOC2,
	SIO_B4_SC1TOGND,
	SIO_B4_SC2TOGND,
	SIO_B4_OPEN,
	SIO_B4_UNKNOWN,
	SIO_C1_SC1TOC2,
	SIO_C1_SC1TOGND,
	SIO_C1_SC2TOGND,
	SIO_C1_OPEN,
	SIO_C1_UNKNOWN,
	SIO_C2_SC1TOC2,
	SIO_C2_SC1TOGND,
	SIO_C2_SC2TOGND,
	SIO_C2_OPEN,
	SIO_C2_UNKNOWN,
	SIO_C3_SC1TOC2,
	SIO_C3_SC1TOGND,
	SIO_C3_SC2TOGND,
	SIO_C3_OPEN,
	SIO_C3_UNKNOWN,
	SIO_C4_SC1TOC2,
	SIO_C4_SC1TOGND,
	SIO_C4_SC2TOGND,
	SIO_C4_OPEN,
	SIO_C4_UNKNOWN,
	UPS_AC_FAIL,
	UPS_BATT_FAIL
	
};

enum FAULT_ARRAY
{
	ARRAY_NONE,
	ARRAY_MPI_CPU,
	ARRAY_MPI_GEN,
	ARRAY_KEYPAD_CPU,
	ARRAY_KEYPAD_GEN,
	ARRAY_MASTER_CPU,
	ARRAY_STARTOFLIST,
	ARRAY_ENDOFLIST
};

enum ARRAY_DIRECTION
{
	DIRECTION_START,
	DIRECTION_DOWN,
	DIRECTION_UP
};

enum OP_STATE
{
	OP_ALIVE,
	OP_FETCHING,
	OP_OFFLINE
};

enum NM_COMMANDS
{
	IDLE=0,
	FETCHING_MASK_VOL,
	FETCHING_PAGE_VOL,
	FETCHING_MUSIC_VOL,
	FETCHING_PAGE_SOURCE,
	FETCHING_MUSIC_SOURCE,
	FETCHING_MASK_CONTOUR
};

enum IO_MODES
{
	IO_DISABLED,
	IO_NORMAL,
	IO_SUPERVISED
};

enum SERIAL_MODES
{
	SERIAL_IDLE,
	SERIAL_GET_PING,
	SERIAL_GET_ADDR
};

enum SERIAL_MODES SerialState=SERIAL_IDLE;

enum FAULT_ARRAY CurrentFaultArray= ARRAY_NONE;
unsigned int CurrentFaultIndex;

enum MODES CurrentMode= STARTUP_MODE;
#ifdef COMBO_UNIT
enum FUNCTIONS CurrentFunc=LCD_INFO;
#else
enum FUNCTIONS CurrentFunc=MASK_VOL;
#endif

far nv_struct nv_copy; //NV config table structure
far address_struct addr_copy;	//address table copy

far unsigned int I2C_OutBuff[64];
far unsigned int I2C_InBuff[64];
unsigned int i;
unsigned int InBuff[10];
unsigned int OutBuff[10];
unsigned int FaultLeds = 0x80;	//doesnt matter for IOHE but must be 0x80 for combo
unsigned int LCDControl = 0x00;	//bits for lcd control
unsigned int SilenceLoop=0;

//#define NUM_MPI_FAULTS 27

far unsigned int CpuFaults[NUM_CPU_FAULTS];	//keypad Faults
far unsigned int GenFaults[NUM_GEN_FAULTS];	//General Faults
boolean HaveHEIOCPUFaults=FALSE;
boolean HaveHEIOGENFaults=FALSE;
boolean HaveCPUFaults=FALSE;
boolean HaveGenFaults=FALSE;


boolean FirstKey=TRUE;	//if this is the first keypress then dont increment mode or func
unsigned KeyPressed;
char LCDBuff[MAX_COLUMNS+1];
//unsigned int LCDColor= 0;
unsigned int LCDColor= LCD_BLUE|LCD_RED|LCD_GREEN;

boolean result;

unsigned IO_N2_Aux_B=0;
unsigned short RelayState=0xff;	//all relays off
unsigned short RelayLEDState=0xff;	//all leds off
boolean WaitToneOff=FALSE;

#define PW_COUNT 4
const unsigned short Password[PW_COUNT]={KEY_MODE,KEY_FUNCTION,KEY_CHANNEL,KEY_ZONE};
unsigned Password_pointer=0;	//current key we are waiting for
boolean PasswordValid=FALSE;

far boolean RunningFault=FALSE;

far UNVT_HE_Request HE_Request2;	//used to cache 
boolean MessageWaiting=FALSE;
far unsigned short TempAmbient;
//far unsigned short Temps[NUM_TEMPS];
far domain_struct my_domain;   // Used to write our own address.



//far float_type Scaler24B;
far unsigned int LoopCounter=0;
far unsigned short CurrentFX_A=0xfD;
far boolean CurrentFXMute = FALSE;
far unsigned short SuperIOState[3][4]={{UNKNOWN_IO,UNKNOWN_IO,UNKNOWN_IO,UNKNOWN_IO},{UNKNOWN_IO,UNKNOWN_IO,UNKNOWN_IO,UNKNOWN_IO},{UNKNOWN_IO,UNKNOWN_IO,UNKNOWN_IO,UNKNOWN_IO}};
far unsigned short LastSuperIOState[3][4]={{UNKNOWN_IO,UNKNOWN_IO,UNKNOWN_IO,UNKNOWN_IO},{UNKNOWN_IO,UNKNOWN_IO,UNKNOWN_IO,UNKNOWN_IO},{UNKNOWN_IO,UNKNOWN_IO,UNKNOWN_IO,UNKNOWN_IO}};	// the previous state
far unsigned short CountSuperIOState[3][4]={{0,0,0,0},{0,0,0,0},{0,0,0,0}};	//the number of counts of back to back states
far unsigned long A2DValue[3][8];
far unsigned short StatusIOState[3][4]={{UNKNOWN_IO,UNKNOWN_IO,UNKNOWN_IO,UNKNOWN_IO},{UNKNOWN_IO,UNKNOWN_IO,UNKNOWN_IO,UNKNOWN_IO},{UNKNOWN_IO,UNKNOWN_IO,UNKNOWN_IO,UNKNOWN_IO}};

unsigned short SerBuffer[10];
unsigned short Ser_in_buffer[10];
unsigned short x=0;
unsigned short y=0;

boolean waiting=FALSE;

unsigned short LedsA=0xff;	//A and B channel leds

unsigned short LedsB=0xff;	//C channel leds plus Relay and aux leds
unsigned short FaultBitsA=0x00;
unsigned short FaultBitsB=0x00;

unsigned short FaultSerLEDs=0xff;
boolean FaultToggle;

unsigned short RelayStates=0xff;



boolean Bouncing=FALSE;
#ifdef COMBO_UNIT
	enum OP_OPTIONS CurrentOPOption=OPTION_OP_ALL;	//for quick setup
#else
	enum OP_OPTIONS CurrentOPOption=OPTION_OP;
#endif

enum IP_OPTIONS CurrentIPOption=OPTION_ADDRESS;
enum VER_OPTIONS CurrentVerOption=OPTION_NEURON;
unsigned short CurrentOPNum=1;
unsigned short CurrentZoneNum=1;
unsigned short CurrentOctet=0;
unsigned short AddrOctet[4]={0,0,0,0};
unsigned short MACOctet[6]={0,0,0,0,0,0};
unsigned short SubOctet[4]={0,0,0,0};
unsigned short GwOctet[4]={0,0,0,0};
#ifdef COMBO_UNIT
	unsigned short CurrentOPChNum=0;	//for quick setup
#else
	unsigned short CurrentOPChNum=1;
#endif

unsigned short CurrentUnitNum=1;
enum SELECT_LOCATION CurrentSelectLocation=SELECT_OPTION;
#ifdef COMBO_UNIT
	enum VOLTAGE_OPTIONS CurrentDiagVoltage=V_15V;
#else
	enum VOLTAGE_OPTIONS CurrentDiagVoltage=V_24V;
#endif

far OPChInfo OPInfo[5];	//4 channels worth of op data plus ALL which is index 0

far OPChInfo ZoneInfo;	//zone storage
#ifdef COMBO_UNIT
far enum OP_STATE OP_State =OP_ALIVE;	//need to do this so all OP option doeesnt show offline
#else
far enum OP_STATE OP_State =OP_OFFLINE;
#endif

far float_type V3Point3Volt;
far float_type V5Volt;

far float_type V5High;
far float_type V5Low;
far float_type V3P3High;
far float_type V3P3Low;
#ifdef COMBO_UNIT
far float_type V1Point26Volt;
far float_type V1Point26Volt_2;
far float_type V3Point3Volt_2;
far float_type V3Point3Volt_H;
far float_type V5Volt_2;
far float_type V5Volt_H;
far float_type V5Volt_ISO_H;
far float_type V15Volt;
far float_type V5Volt_M;
far float_type V15Low;
far float_type V15High;
far float_type V126Low;
far float_type V126High;
far float_type Scaler3;
far float_type Scaler4;
far float_type Scaler15;
#else
far float_type V5isoVolt;
far float_type Scaler;
far float_type Scaler2;
far float_type V24Volt;
far float_type Scaler24;
far float_type V24High;
far float_type V24Low;

#endif
far unsigned short LCD_Digit1[4];
far unsigned short LCD_Digit2[4];
far unsigned short LCD_Digit3[4];
far unsigned short StatusLED[4];

far int TryCount;
far enum NM_COMMANDS CurrentNMFunction= IDLE;
far NM_nv_fetch_response MyFetchedNV;
far unsigned int CurrentTX=0;
far UNVT_OP_Request TempCommand;
far unsigned short CurrentMaskVol[MAX_CHANNELS+1];	//1 through 16 is for each channel 0 is for all
far unsigned short CurrentMaskCont[MAX_CHANNELS+1];
far unsigned short CurrentPageVol[MAX_CHANNELS+1];	//1 through 16 is for each channel 0 is for all
far unsigned short CurrentMusicVol[MAX_CHANNELS+1];	//1 through 16 is for each channel 0 is for all
far int UsingZone=FALSE;	//determines if we are using zone or channel
far unsigned short OP_Node=1;
far unsigned short OP_Subnet=2;
//far unsigned short MyOpNum=0;
far unsigned short CurrentIO=0;
far boolean CurrentMuteState;
far unsigned short MuteCount;
far boolean DeviceInet=FALSE;
far unsigned short PcbVer=0;
far unsigned short VoltFaultCount=0;
//far unsigned short CurrentPosition=1;	//index of IO unit
far boolean SMCAlive=FALSE;
far boolean MacIDGood=FALSE;
far boolean HaveSMCData=FALSE;
far boolean AUXLedState=FALSE;
far unsigned short FireTx=0;
far boolean DeviceFACPZoneInput;	// is this unit configured to be a contact input for facp multizone page
far unsigned CurrentMuteType;

eeprom far unsigned short SuperEnableBits[3]={0xff,0xff,0xff};	//enables for supervision bit 0 is sw 1. 
#ifdef COMBO_UNIT
	#define NUM_IO 1
	eeprom far enum IO_MODES IO_Setting[NUM_IO]={IO_NORMAL};
	#define NUM_VOLUME_STEPS 39
	unsigned int AudioVolume;	//current volume of each source
	far const unsigned short VolumeSteps[39]={0,1,2,3,4,5,6,7,8,9,10,11,13,14,16,18,20,23,25,29,32,36,40,45,51,57,64,72,81,90,101,114,128,143,161,180,203,227,255};
	far unsigned int CurrentVolStep=NUM_VOLUME_STEPS-1;
	eeprom far unsigned short Pot_Audio=NUM_VOLUME_STEPS-3;
	far const unsigned short BarSteps[39]={0,0,1,1,2,2,3,3,3,4,4,5,5,5,6,6,7,7,8,8,8,9,9,10,10,11,11,11,12,12,13,13,13,14,14,15,15,16,16};

#else
	#define NUM_IO 12
	eeprom far enum IO_MODES IO_Setting[NUM_IO]={IO_NORMAL,IO_DISABLED,IO_DISABLED,IO_DISABLED,IO_DISABLED,IO_DISABLED,IO_DISABLED,IO_DISABLED,IO_DISABLED,IO_DISABLED,IO_DISABLED,IO_DISABLED};
#endif



//mtimer repeating KeyScan = KEY_SCAN_NORMAL;
//stimer KeyDeadTimer=NO_INPUT_TIMEOUT;
stimer KeyDeadTimer=0;
stimer CheckFXPlayContact=0;
mtimer FXBounce=0;
//stimer repeating ReadSignal = 0;
//stimer ScreenSetback=0;
//stimer repeating SilenceTimeout=0;	//24hr delay for silenced alarm
stimer repeating TroubleTick=0;	//typically a 10 second delay between trouble tones
//mtimer repeating FetchInputs=0;
mtimer repeating FetchInputs=500;
stimer repeating SerialPing=3;
mtimer ButtonBounce=0;
mtimer ConfigTimer=0;
mtimer FetchTimer=0;
stimer repeating SilenceTimeout=0;	//24hr delay for silenced alarm
#define FETCH_WAIT_TIME 750
mtimer FetchWait=0;
#define MUTE_REPEAT 200
#define NUM_MUTE_MESSAGES 2
mtimer repeating MuteRepeat=0; //cycle time for sending mute masking
//mtimer repeating ColorTimer=1000;
stimer repeating VoltageScan= 0;
mtimer FaultWait=0;
#ifdef COMBO_UNIT
#define LCDScan 1
stimer Readlcd;
#endif

//IO_0 input nibble ioRowRead;
//IO_4 output nibble ioColumnWrite;
//IO_0 output nibble ioRowWrite;
//IO_4 input nibble ioColumnRead;
IO_0 i2c ioI2C;
//IO_10 input bit irqsmc;
//IO_11 input bit SerIRQ;
IO_2 input bit SW1;
IO_3 input bit SW2;
IO_4 input bit SW3;
IO_5 input bit SW4;
IO_6 input bit SW5;
IO_8 sci baud(SCI_38400) SerialPort;
IO_7 output bit RTS_Out;
IO_9 input bit CTS_In;

msg_tag PokeNV;
msg_tag NM;


network input sd_string("@0|1") SNVT_obj_request nviRequest0;
network output sd_string("@0|2") bind_info(ackd) SNVT_obj_status nvoStatus0;

// Mandatory Network Variables for Network Management Object: 2 Nm
network input sd_string("@1|1") UNVT_NM_Request NM_Request;
network output sd_string("@1|2") UNVT_NM_Response NM_Response;

// Mandatory Network Variables for Object: 1 HE
network input sd_string ("@2|1") UNVT_HE_Request HE_Request;
network output sd_string ("@2|2") UNVT_HE_Response HE_Response;


network output SNVT_str_asc RtnMess;

network output SNVT_count MyCount;

network input SCPTnwrkCnfg config_prop ConfigSource=0;	//determine who will do network managemnet- default to self install

far network input UNVT_NeuronFault NeuronFault;	//input from all other neurons

far network input UNVT_OP_Request OP_Request;	//input to monitor any paging traffic


far network input UNVT_Master_Request Master_Request;	//input to get temp data

const network input cp cp_info(device_specific) SCPTdevMajVer VersionMajor;
const network input cp cp_info(device_specific) SCPTdevMinVer VersionMinor;


//<Device CP Declarations>
device_properties {
	VersionMajor={NEURON_SW_VERSION_MAJOR},
	VersionMinor={NEURON_SW_VERSION_MINOR}
};

interrupt (IO_11, -)
{
//	SendDebug(0x33,0x55);
	HaveSMCData=TRUE;
	interrupt_control(INTERRUPT_TC | INTERRUPT_REPEATING);	//I believe this disables IO interrupt
	
	
	//ReadButtons();	//handle the buttons now and then prevent reactivation for a while
	//Debounce=200;
}

when(HaveSMCData)
{

	boolean BytesInRec=FALSE;
	unsigned int NumBytes;
	unsigned int i;

	SelectBus(BUS_C);
	
	//see how many bytes in the fifo
	I2C_OutBuff[0]=0x48;	//reg RXLVL	
	result = io_out(ioI2C, &I2C_OutBuff, UART_ADDR, 1);	//set the reg address
	result = io_in(ioI2C, I2C_InBuff, UART_ADDR, 1);//reda in teh reg value
	if(!result)
	{
		CPUFault(DEVICE_ID_1UHE_IO,UART_I2C);
		return;
	}

	NumBytes=I2C_InBuff[0];
	
	//SendDebug(0x40,I2C_InBuff[0]);

//	I2C_OutBuff[0]=0x28;	//reg 0x05 LSR	
//	result = io_out(ioI2C, &I2C_OutBuff, UART_ADDR, 1);	//set the reg address
//	result = io_in(ioI2C, I2C_InBuff, UART_ADDR, 1);//reda in teh reg value. 
//	if(!result)
//	{
//		CPUFault(DEVICE_ID_1UHE_IO,UART_I2C);
//		return;
//	}
//
//	SendDebug(0x41,I2C_InBuff[0]);


	if(NumBytes >0)
		BytesInRec=TRUE;	//this should always be true if we got here
	else
	{
//		interrupt_control(0xff);
		HaveSMCData=FALSE;
		return;
	}

	
	I2C_OutBuff[0]=0x0;	//reg 0x00	
	result = io_out(ioI2C, &I2C_OutBuff, UART_ADDR, 1);	//set the reg address
	result = io_in(ioI2C, I2C_InBuff, UART_ADDR, NumBytes);//reda in teh reg value. 
	if(!result)
	{
		CPUFault(DEVICE_ID_1UHE_IO,UART_I2C);
		return;
	}

	DecodeSerialData();
//	interrupt_control(0xff);
	HaveSMCData=FALSE;
}

void DecodeSerialData()
{
	if(	(I2C_InBuff[0]==1)&&(I2C_InBuff[15]==4))
	{
		switch(SerialState)
		{
			case SERIAL_GET_PING:
				if(I2C_InBuff[1]==1)
				{
					SMCAlive=TRUE;
					// we got a ping back so we know protocessor is alive. But the mac may still be 0 due to no cable
					UpdateLEDStates(LED_AUX,TRUE);
					AUXLedState=TRUE;
					SerialState=SERIAL_IDLE;
					MACOctet[0]=I2C_InBuff[3];
					MACOctet[1]=I2C_InBuff[4];
					MACOctet[2]=I2C_InBuff[5];
					MACOctet[3]=I2C_InBuff[6];
					MACOctet[4]=I2C_InBuff[7];
					MACOctet[5]=I2C_InBuff[8];
					if((MACOctet[0]==0) &&(MACOctet[1]==0) && (MACOctet[2]==0) && (MACOctet[3]==0) && (MACOctet[4]==0) && (MACOctet[5]==0))
					{
						MacIDGood=FALSE;
					}
					else
					{
						MacIDGood=TRUE;
					}
				}
				else
				{
						UpdateLEDStates(LED_AUX,FALSE);
						AUXLedState=FALSE;
						SMCAlive=FALSE;
						MacIDGood=FALSE;

				}
				break;
			case SERIAL_GET_ADDR:
				if(I2C_InBuff[1]==2)
				{
					SMCAlive=TRUE;
					UpdateLEDStates(LED_AUX,TRUE);
					AUXLedState=TRUE;
					SerialState=SERIAL_IDLE;
					AddrOctet[0]=	I2C_InBuff[3];			
					AddrOctet[1]=	I2C_InBuff[4];			
					AddrOctet[2]=	I2C_InBuff[5];			
					AddrOctet[3]=	I2C_InBuff[6];			
					SubOctet[0]=I2C_InBuff[7];
					SubOctet[1]=I2C_InBuff[8];
					SubOctet[2]=I2C_InBuff[9];
					SubOctet[3]=I2C_InBuff[10];
					GwOctet[0]=I2C_InBuff[11];
					GwOctet[1]=I2C_InBuff[12];
					GwOctet[2]=I2C_InBuff[13];
					GwOctet[3]=I2C_InBuff[14];
				}
				else
				{
						UpdateLEDStates(LED_AUX,FALSE);
						AUXLedState=FALSE;
						SMCAlive=FALSE;
						MacIDGood=FALSE;
				}
				break;
					
		}
		
	}
	else
	{
			UpdateLEDStates(LED_AUX,FALSE);
			AUXLedState=FALSE;
			SMCAlive=FALSE;
			MacIDGood=FALSE;
	}
	
}


when(reset)
{
/*
	ClearFaults(DEVICE_ID_KEYPAD);

	CPUFault(DEVICE_ID_KEYPAD,NO_CPU_FAULTS);
	GenFaultLED(0);
*/
	if(ConfigSource==0)
		PreInstall();	//test if this is a virgin neuron

	SetupI2C();
	SetupUart();
	DefaultDisplay();
	ReadSettingsIO();
	if(PcbVer>=1)
	{
		SetupI2C2();	//only if hw is there
		VoltageScan= 5;
	}

	if(!DeviceInet)
	{
		CurrentFunc=IP_ADD;
		
	}
/*
	io_set_direction(ioRowRead, IO_DIR_IN);
	io_set_direction(ioColumnWrite, IO_DIR_OUT);
	io_out(ioColumnWrite,0); //set outputs low to look for lows on rows
*/
//	WaitMessage();
	StartMessage();
	
	io_change_init(SW1);
	io_change_init(SW2);
	io_change_init(SW3);
	io_change_init(SW4);
	io_change_init(SW5);
/*	
	io_set_direction(ioRowRead, IO_DIR_IN);
	io_set_direction(ioColumnWrite, IO_DIR_OUT);
	io_out(ioColumnWrite,0); //set outputs low to look for lows on rows

	result=Initialize();
	if(!result)
		CPUFault(DEVICE_ID_KEYPAD,INITIALIZE);

	PreInstall();
	CurrentFaultArray=ARRAY_STARTOFLIST;
	*/
	
//	UpdateLEDStates(LED_FTR_4A,1);
//	UpdateLEDStates(LED_FTR_4B,1);
//	UpdateLEDStates(LED_FTR_4C,1);
//	WriteAuxRelaysLEDs2(LED_ALL,1);
	PlayFX(FX_MESSAGE_1);
	WriteAuxRelays(RELAY_1,1);	//must be on by default. use NC contact to signal fault

#ifdef COMBO_UNIT
	UpdateLEDStates(LED_POWER,1);
	PaintLeds();
#else
	WriteAuxRelays(RELAY_2,0);
	WriteAuxRelays(RELAY_3,0);
#endif

//UpdateLEDStates(LED_AUX,1);

//	FaultBitsA=0x01;
	WriteFaultSerLeds(LED_ALL,0);
//	WriteAuxRelays(RELAY_1,1);

	fl_from_ascii("5.50E0",&V5High);
	fl_from_ascii("4.50E0",&V5Low);
	fl_from_ascii("3.63E0",&V3P3High);
	fl_from_ascii("2.97E0",&V3P3Low);
#ifdef COMBO_UNIT
	fl_from_ascii("1.3428E-3",&Scaler3);	//200 over 2k
	fl_from_ascii("2.6856E-3",&Scaler4);	//2 x 200 over 2k
	fl_from_ascii("4.7241E-3",&Scaler15);	//2.87k over 1k
	fl_from_ascii("1.386E0",&V126High);
	fl_from_ascii("1.134E0",&V126Low);
	fl_from_ascii("1.650E1",&V15High);
	fl_from_ascii("1.350E1",&V15Low);

	CurrentVolStep=Pot_Audio;	//pull back flash versions
	AudioVolume=VolumeSteps[CurrentVolStep];
	
	result=WriteMCP45HV(POT_ADDR,BUS_A,AudioVolume);

#else
	fl_from_ascii("1.2207E-3",&Scaler);
	fl_from_ascii("2.4414E-3",&Scaler2);
	fl_from_ascii("1.3428E-2",&Scaler24);
	fl_from_ascii("2.76E1",&V24High);
	fl_from_ascii("2.04E1",&V24Low);

#endif

	OPInfo[0].MaskVol=48;
	OPInfo[0].MaskContour=15;
	OPInfo[0].PageVol=24;
	OPInfo[0].MusicVol=24;
#ifndef COMBO_UNIT
	if(DeviceInet)
		GetOPValues(); //see if op is here
#endif
//	else
	CurrentSelectLocation=SELECT_VALUE;
	Functions();
//	GenFaultLED(1);	
//	CPUFaultLED(1);	
	//WriteFaultSerLeds(LED_SERIAL,1);
	interrupt_control(0xff);

}




when(timer_expires(SerialPing))
{
	SerBuffer[0]=x;
	io_out_request(SerialPort,SerBuffer,1);
	io_in_request(SerialPort, Ser_in_buffer, 1);
	io_out(RTS_Out,(x&0x01));
	waiting = TRUE;
	ReadRelays();
	
//	SelectBus(BUS_C);
	//SendDebug(0x55,io_in(SerIRQ));

	if(SerialState!=SERIAL_IDLE)
	{
		SMCAlive=FALSE;	//we didnt hear back from the prior command
		MacIDGood=FALSE;
	}
		

//	if(SMCAlive)
	if(MacIDGood)
		GetSMCAddress();
	else
	{
//		if(AUXLedState==TRUE)
//		{
//			UpdateLEDStates(LED_AUX,FALSE);
//			AUXLedState=FALSE;
//		}
//		else
//		{
//			UpdateLEDStates(LED_AUX,TRUE);
//			AUXLedState=TRUE;
//		}
		PingSMC();
	}
	
		
//		I2C_OutBuff[0]=0x00;	//reg 0x00	
//		I2C_OutBuff[1]=1;	//esc
//		//I2C_OutBuff[2]=2;	//stop
//		//I2C_OutBuff[3]='s';	//cr
//		io_out(ioI2C, &I2C_OutBuff, UART_ADDR, 2);	//set the reg address
//		//PlayingMessage=FALSE; //use this until we get a live ptt signal

}

void PingSMC()
{
		SelectBus(BUS_C);
	//SendDebug(0x55,io_in(SerIRQ));
		SerialState=SERIAL_GET_PING;
		I2C_OutBuff[0]=0x00;	//reg 0x00	

		I2C_OutBuff[1]=0xc0 | NEURON_VER_ENUM;	//Special Check online command- Flag as having version enum
		//I2C_OutBuff[1]=1;	// Check online command- with version enum
		result=io_out(ioI2C, &I2C_OutBuff, UART_ADDR, 2);	//set the reg address
		if(!result)
		{
			CPUFault(DEVICE_ID_1UHE_IO,UART_I2C);
			return;
		}
		interrupt_control(0xff);
}

void GetSMCAddress()
{
		SelectBus(BUS_C);
	//SendDebug(0x55,io_in(SerIRQ));
		SerialState=SERIAL_GET_ADDR;
	
		I2C_OutBuff[0]=0x00;	//reg 0x00	
		I2C_OutBuff[1]=2;	// Get address info
		result=io_out(ioI2C, &I2C_OutBuff, UART_ADDR, 2);	//set the reg address
		if(!result)
		{
			CPUFault(DEVICE_ID_1UHE_IO,UART_I2C);
			return;
		}
		interrupt_control(0xff);

}

when(waiting)
{
	if(io_in_ready(SerialPort))
	{
		x++;
		waiting=FALSE;
		//SendDebug(0x03,Ser_in_buffer[0]);
		y=io_in(CTS_In);
		//SendDebug(0x04,y);
	}
}

when (timer_expires(VoltageScan))
{
#ifdef COMBO_UNIT
	GetVoltage(A2D_V3POINT3);
	GetVoltage(A2D_V5);
	GetVoltage(A2D_V1POINT26);
	GetVoltage(A2D_V15);
	GetVoltage(A2D_V3POINT3_2);
	GetVoltage(A2D_V3POINT3_H);
	GetVoltage(A2D_V5_2);
	GetVoltage(A2D_V5_H);
	GetVoltage(A2D_V5_ISO_H);
	GetVoltage(A2D_V1POINT26_2);
	GetVoltage(A2D_VM5);
#else
	GetVoltage(A2D_V3POINT3);
	GetVoltage(A2D_V5);
	GetVoltage(A2D_V_ISO5);
	GetVoltage(A2D_V24);
#endif
	CheckVoltageRanges();
	

}

when(timer_expires(FetchInputs))
{
	GetSupervisedInputs();
	if(FaultToggle)
	{
		FaultToggle=FALSE;
	}
	else
	{
		FaultToggle=TRUE;
	}
	if(!SMCAlive)
	{
		if(AUXLedState==TRUE)
		{
			UpdateLEDStates(LED_AUX,FALSE);
			AUXLedState=FALSE;
		}
		else
		{
			UpdateLEDStates(LED_AUX,TRUE);
			AUXLedState=TRUE;
		}
		
	}

	PaintLeds();
	if(!DeviceInet)
		CheckSendIO();	//see if we need to send any feature inputs.
	
}

//when( timer_expires(ColorTimer))
//{
//	LCDColor++;
//	if(LCDColor>=8)
//		LCDColor=0;
//	
//	WriteLCDControl();
//	StartMessage();
//}


when (io_changes(SW1)to 0) {
	if(!Bouncing)
		ProcessSw(SWITCH_1);
}

when (io_changes(SW2)to 0) {
	if(!Bouncing)
		ProcessSw(SWITCH_2);
}

when (io_changes(SW3)to 0) {
	if(!Bouncing)
		ProcessSw(SWITCH_3);
}

when (io_changes(SW4)to 0) {
	if(!Bouncing)
		ProcessSw(SWITCH_4);
}

when (io_changes(SW5)to 0) {
	if(!Bouncing)
		ProcessSw(SWITCH_5);
}



when(nv_update_occurs(NM_Request))
{
	switch (NM_Request.Command)
	{

		case 4:
//			HeadendPresent=TRUE;	//we got at least one ping so we shoudl assuem HE is connected
//			if(FirstPassDone)
//			{
				if(!DeviceInet)
				{
					if(CheckFaultFlags())
						FaultWait=((unsigned long)(CurrentUnitNum+200)*100UL);	//only set delay if we have some faults. 
				}
//			}
			break;
	}
	
		
}

when(timer_expires(FaultWait))
{
	SendAlarmHEKP();
}


boolean CheckFaultFlags()
{
	unsigned int i;
	boolean FaultFlag=FALSE;

	if(HaveCPUFaults)
		FaultFlag=TRUE;

	if(HaveGenFaults)
		FaultFlag=TRUE;
	

	return(FaultFlag);	
}

void SendAlarmHEKP()
{
	
	unsigned char SelHi;
	unsigned char SelLow;
	unsigned j;
	unsigned long UpdateTag;
	unsigned short ComboFaults=0xa0;	//upper bit must be set like op, bit 6,5 is device type =01
	
	// set outgoing address to be Subnet node based
	msg_out.dest_addr.snode.domain = 0;
	msg_out.dest_addr.snode.node = HE_LISTENER_NODE;
	msg_out.dest_addr.snode.subnet = HE_LISTENER_SUBNET;
		
	msg_out.dest_addr.snode.type=SUBNET_NODE;
	
	msg_out.tag = PokeNV;
  msg_out.service = UNACKD;

	msg_out.code = 0x80 | NEURON_FAULT_HIGH;
	msg_out.data[0] = NEURON_FAULT_LOW;	//set index
	msg_out.data[1]=(char)CurrentUnitNum+50;	//send the current IO number
	if(HaveGenFaults)
		ComboFaults=ComboFaults|0x02;
	if(HaveCPUFaults)
		ComboFaults=ComboFaults|0x01;
	
	msg_out.data[2]=(char)ComboFaults;	//send the combination of fault IDs


  msg_send();
  post_events();


}

when (resp_arrives( NM ))
{
	//DebugMessage('4');
	switch ( resp_in.code)
	{
//		case 0x2a:
//			//succesful query domain
//			//memcpy( &MyOPDomain, resp_in.data, sizeof(MyOPDomain) );
//			memcpy( &MyOPDomain, resp_in.data, 15 );
//			ConfigTimer=0;
//			CalcOp();
//		
//			TryCount=0;
//			if(!GetDomainOnly)
//				SendGetVolumes(MASK_ATTENUATION);
//			else
//				GetDomainOnly=FALSE;
//			//DebugMessage('1');
//
//			break;
//		case 0x0a:
//			//failed query domain
//			break;
		case 0x33:
			//successful nv fetch
			//memcpy( &MyFetchedNV, resp_in.data, sizeof(MyFetchedNV) );
			memcpy( &MyFetchedNV, resp_in.data, 32 );
			ConfigTimer=0;
			ProcessNV();
			//DebugMessage('2');
			break;
		case 0x13:
			//Failed nv fetch
			break;
		default:
			break;			
			
	}
	
}

when (timer_expires(FetchTimer))
{
	FetchResult();	
	SetupWait();

}

when(timer_expires(FetchWait))
{
		GetOPValues();
}

when(timer_expires(ConfigTimer))
{
	TryCount++;
	if(TryCount >5)
	{
		PaintCommError();
		//GetDomainOnly=FALSE;	//in case we were doing fetch just for domain
	}
	else
	{
		switch(CurrentNMFunction)
		{
//			case 	FETCHING_DOMAIN:
//				GetDomain();
//				break;
			case FETCHING_MASK_VOL:
				SendGetVolumes(MASK_ATTENUATION_CONTOUR);	
				break;
			case FETCHING_PAGE_VOL:
				SendGetVolumes(PAGE_ATTENUATION);
				break;
			case FETCHING_MUSIC_VOL:
				SendGetVolumes(MUSIC_ATTENUATION);
				break;
//			case FETCHING_PAGE_SOURCE:
//				SendGetSources(PAGE_ATTENUATION);
//				break;
//			case FETCHING_MUSIC_SOURCE:
//				SendGetSources(MUSIC_ATTENUATION);
//				break;
			case FETCHING_MASK_CONTOUR:
				SendGetContours();
				break;
//			case FETCHING_TEMPS:
//				SendGetTemps();
//				break;
//			case FETCHING_FACP_INFO:
//				SendGetFACPCh();
//				break;
			
		}
	}
	
}


void CheckVoltageRanges()
{
	boolean Fault=FALSE;

#ifdef COMBO_UNIT
	if(fl_gt(&V5Volt, &V5High))
		Fault=TRUE;
	if(fl_lt(&V5Volt, &V5Low))
		Fault=TRUE;

	if(fl_gt(&V5Volt_2, &V5High))
		Fault=TRUE;
	if(fl_lt(&V5Volt_2, &V5Low))
		Fault=TRUE;

	if(fl_gt(&V5Volt_H, &V5High))
		Fault=TRUE;
	if(fl_lt(&V5Volt_H, &V5Low))
		Fault=TRUE;

	if(fl_gt(&V5Volt_ISO_H, &V5High))
		Fault=TRUE;
	if(fl_lt(&V5Volt_ISO_H, &V5Low))
		Fault=TRUE;


	if(fl_gt(&V1Point26Volt, &V126High))
		Fault=TRUE;
	if(fl_lt(&V1Point26Volt, &V126Low))
		Fault=TRUE;
	if(fl_gt(&V1Point26Volt_2, &V126High))
		Fault=TRUE;
	if(fl_lt(&V1Point26Volt_2, &V126Low))
		Fault=TRUE;


	if(fl_gt(&V3Point3Volt, &V3P3High))
		Fault=TRUE;
	if(fl_lt(&V3Point3Volt, &V3P3Low))
		Fault=TRUE;

	if(fl_gt(&V3Point3Volt_2, &V3P3High))
		Fault=TRUE;
	if(fl_lt(&V3Point3Volt_2, &V3P3Low))
		Fault=TRUE;

	if(fl_gt(&V3Point3Volt_H, &V3P3High))
		Fault=TRUE;
	if(fl_lt(&V3Point3Volt_H, &V3P3Low))
		Fault=TRUE;


	if(fl_gt(&V15Volt, &V15High))
		Fault=TRUE;
	if(fl_lt(&V15Volt, &V15Low))
		Fault=TRUE;

//	if(fl_gt(&V15Volt_2, &V15High))
//		Fault=TRUE;
//	if(fl_lt(&V15Volt_2, &V15Low))
//		Fault=TRUE;

#else
	if(fl_gt(&V5Volt, &V5High))
		Fault=TRUE;
	if(fl_lt(&V5Volt, &V5Low))
		Fault=TRUE;

	if(fl_gt(&V5isoVolt, &V5High))
		Fault=TRUE;
	if(fl_lt(&V5isoVolt, &V5Low))
		Fault=TRUE;


	if(fl_gt(&V3Point3Volt, &V3P3High))
		Fault=TRUE;
	if(fl_lt(&V3Point3Volt, &V3P3Low))
		Fault=TRUE;


	if(fl_gt(&V24Volt, &V24High))
		Fault=TRUE;
	if(fl_lt(&V24Volt, &V24Low))
		Fault=TRUE;
#endif	



	if(Fault)
	{
		if(VoltFaultCount>2)
			CPUFault(DEVICE_ID_1UHE_IO,VOLTAGE_RANGE);	//only flag fault after 3 consecutive faults
		else
			VoltFaultCount++;
	}
	else
		VoltFaultCount=0;	//all faults are good so clear counter
		
//	CPUFault(DEVICE_ID_1UHE_IO,VOLTAGE_RANGE);	//only flag fault after 3 consecutive faults
		
		
}


void FetchResult()
{
	FetchNv(OPRESPONSE_INDEX);
	
}

void FetchNv(int NVIndex)
{

	unsigned MySub=2;
	unsigned MyNode=1;
	int i;
	
	
	
//			msg_out.dest_addr.nrnid.domain = 0;
//		for(i=0;i<6;i++)
//			msg_out.dest_addr.nrnid.nid[i] = DspNeuronID[i];
//
//		msg_out.dest_addr.nrnid.subnet = 2;
//		msg_out.dest_addr.nrnid.type=NEURON_ID;
//
//	msg_out.dest_addr.nrnid.tx_timer=4;

		// set outgoing address to be Subnet node based
		msg_out.dest_addr.snode.domain = 0;
		msg_out.dest_addr.snode.node = OP_Node;
		msg_out.dest_addr.snode.subnet = OP_Subnet;
		msg_out.dest_addr.snode.type=SUBNET_NODE;
		msg_out.dest_addr.snode.retry =1;
		msg_out.dest_addr.snode.tx_timer=6;
	

	
	msg_out.code = 0x73; // fetch nv code
	msg_out.data[0] = NVIndex;	//domain index
	// Copy into msg_out data array
	msg_out.service = REQUEST; // Expect a response
	msg_out.tag = NM; // Destination address
	msg_send( ); // Send the message	

}


void ProcessNV()
{
	switch(CurrentNMFunction)
	{
		case IDLE:
			break;
		case FETCHING_MASK_VOL:
			if(MyFetchedNV.short_index.data[4]==(CurrentTX))
			{
				DecodeVolumes(MASK_ATTENUATION_CONTOUR);
				TryCount=0;
				SendGetVolumes(PAGE_ATTENUATION);
			}
			else
			{
				TryCount++;
				if(TryCount>=5)
					PaintCommError();
				else
					SendGetVolumes(MASK_ATTENUATION_CONTOUR);	//we didnt get the settings, try again
			}
			break;
		case FETCHING_PAGE_VOL:
			if(MyFetchedNV.short_index.data[4]==(CurrentTX))
			{
				DecodeVolumes(PAGE_ATTENUATION);
				TryCount=0;
				SendGetVolumes(MUSIC_ATTENUATION);
			}
			else
			{
				TryCount++;
				if(TryCount>=5)
					PaintCommError();
				else
					SendGetVolumes(PAGE_ATTENUATION);	//we didnt get the settings, try again
			}
			break;
		case FETCHING_MUSIC_VOL:
			if(MyFetchedNV.short_index.data[4]==(CurrentTX))
			{
				DecodeVolumes(MUSIC_ATTENUATION);
				TryCount=0;
				CurrentNMFunction=IDLE;	//done
				OP_State=OP_ALIVE;	//got all the data from the op
				PaintFunctions();
			}
			else
			{
				TryCount++;
				if(TryCount>=5)
					PaintCommError();
				else
					SendGetVolumes(MUSIC_ATTENUATION);	//we didnt get the settings, try again
			}
			break;
//		case FETCHING_PAGE_SOURCE:
//			if(MyFetchedNV.short_index.data[4]==(CurrentTX))
//			{
//				DecodeSources(PAGE_ATTENUATION);
//				TryCount=0;
//				SendGetSources(MUSIC_ATTENUATION);
//			}
//			else
//			{
//				TryCount++;
//				if(TryCount>=5)
//					PaintCommError();
//				else
//					SendGetSources(PAGE_ATTENUATION);	//we didnt get the settings, try again
//			}
//			break;
//		case FETCHING_MUSIC_SOURCE:
//			if(MyFetchedNV.short_index.data[4]==(CurrentTX))
//			{
//				DecodeSources(MUSIC_ATTENUATION);
//				TryCount=0;
//				SendGetTemps();
//			}
//			else
//			{
//				TryCount++;
//				if(TryCount>=5)
//					PaintCommError();
//				else
//					SendGetSources(MUSIC_ATTENUATION);	//we didnt get the settings, try again
//			}
//			break;
//		case FETCHING_TEMPS:
//			if(MyFetchedNV.short_index.data[4]==(CurrentTX))
//			{
//				DecodeTemps();
//				TryCount=0;
//				SendGetSpkrCnt();
//			}
//			else
//			{
//				TryCount++;
//				if(TryCount>=5)
//					PaintCommError();
//				else
//					SendGetTemps();	//we didnt get the settings, try again
//			}
//			break;
//		case FETCHING_SPKRS:
//			if(MyFetchedNV.short_index.data[4]==(CurrentTX))
//			{
//				DecodeSpkrs();
//				TryCount=0;
//				SendGetSpkrCntDef();
//			}
//			else
//			{
//				TryCount++;
//				if(TryCount>=5)
//					PaintCommError();
//				else
//					SendGetSpkrCnt();	//we didnt get the settings, try again
//			}
//			break;
//		case FETCHING_SPKRS_DEF:
//			if(MyFetchedNV.short_index.data[4]==(CurrentTX))
//			{
//				DecodeSpkrsDef();
//				TryCount=0;
//				SendGetContours();
//			}
//			else
//			{
//				TryCount++;
//				if(TryCount>=5)
//					PaintCommError();
//				else
//					SendGetSpkrCntDef();	//we didnt get the settings, try again
//			}
//			break;
//		case FETCHING_MASK_CONTOUR:
//			if(MyFetchedNV.short_index.data[4]==(CurrentTX))
//			{
//				DecodeContours();
//				TryCount=0;
//				CurrentNMFunction=IDLE;
//				CurrentMode=MASK_MODE;
//				FirstKey=TRUE;	//allows the call to mode change without changing mode
//				ModeChange();
//				KeyDeadTimer=NO_INPUT_TIMEOUT;
//			}
//			else
//			{
//				TryCount++;
//				if(TryCount>=5)
//					PaintCommError();
//				else
//					SendGetContours();	//we didnt get the settings, try again
//			}
//			break;
//		case FETCHING_FACP_INFO:
//			if(MyFetchedNV.short_index.data[4]==(CurrentTX))
//			{
//				DecodeFACP();
//				//StartMessage();
//				CurrentNMFunction=IDLE;
//				CurrentMode=MASK_MODE;
//				FirstKey=TRUE;	//allows the call to mode change without changing mode
//				ModeChange();
//				KeyDeadTimer=NO_INPUT_TIMEOUT;
//			}
//			else
//			{
//				TryCount++;
//				if(TryCount>=5)
//					PaintCommError();
//				else
//					SendGetFACPCh();	//we didnt get the settings, try again
//			}
//			break;
			
	}
	
	
	
}

void DecodeVolumes(int Command)
{
	int i,z;
	
	switch(Command)
	{
		case MASK_ATTENUATION:
			for(i=0; i<MAX_CHANNELS;i++)
				//CurrentMaskVol[i+1]=MyFetchedNV.short_index.data[i+6];
				OPInfo[i+1].MaskVol=MyFetchedNV.short_index.data[i+6];
			break;
		case MASK_ATTENUATION_CONTOUR:
			for(i=0,z=0; i<MAX_CHANNELS;i++,z=z+2)
				//CurrentMaskVol[i+1]=MyFetchedNV.short_index.data[z+6];
				OPInfo[i+1].MaskVol=MyFetchedNV.short_index.data[z+6];
			for(i=0,z=0; i<MAX_CHANNELS;i++,z=z+2)
				//CurrentMaskCont[i+1]=MyFetchedNV.short_index.data[z+7];
				OPInfo[i+1].MaskContour=MyFetchedNV.short_index.data[z+7];
			break;
		case PAGE_ATTENUATION:
			for(i=0; i<MAX_CHANNELS;i++)
				//CurrentPageVol[i+1]=MyFetchedNV.short_index.data[i+6];
				OPInfo[i+1].PageVol=MyFetchedNV.short_index.data[i+6];
			break;
		case MUSIC_ATTENUATION:
			for(i=0; i<MAX_CHANNELS;i++)
				//CurrentMusicVol[i+1]=MyFetchedNV.short_index.data[i+6];
				OPInfo[i+1].MusicVol=MyFetchedNV.short_index.data[i+6];
			break;
			
	}
	
}

void SendGetVolumes(int Command)
{
	int i;
	
		TempCommand.OPAddress = CurrentOPNum;
		TempCommand.OPGroupSelect = INDIVIDUAL;
		TempCommand.OPCommandCat= Command;
		TempCommand.OPSubCommand= 10;
		
		TempCommand.OPTransactionID= NextTx();
		TempCommand.ByteCount=6;
		for(i=0;i<25;i++)
			TempCommand.OPData[i]=0;

	switch(Command)	
	{
		case MASK_ATTENUATION:
			CurrentNMFunction= FETCHING_MASK_VOL;
			break;
		case MASK_ATTENUATION_CONTOUR:
			CurrentNMFunction= FETCHING_MASK_VOL;
			break;
		case PAGE_ATTENUATION:
			CurrentNMFunction= FETCHING_PAGE_VOL;
			break;
		case MUSIC_ATTENUATION:
			CurrentNMFunction= FETCHING_MUSIC_VOL;
			break;
	}
			
		SendOPMess();
		FetchTimer=OP_WAIT_VOL;
	
}

void SendGetContours()
{
	int i;
	
		TempCommand.OPAddress = CurrentOPNum;
		TempCommand.OPGroupSelect = INDIVIDUAL;
		TempCommand.OPCommandCat= MASK_CONTOUR;
		TempCommand.OPSubCommand= 85;
		
		TempCommand.OPTransactionID= NextTx();
		TempCommand.ByteCount=6;
		for(i=0;i<25;i++)
			TempCommand.OPData[i]=0;

			CurrentNMFunction= FETCHING_MASK_CONTOUR;
			
		SendOPMess();
		FetchTimer=OP_WAIT_VOL;
	
}

void DecodeContours()
{
	int i;
	
			for(i=0; i<MAX_CHANNELS;i++)
				CurrentMaskCont[i+1]=MyFetchedNV.short_index.data[i+6];
			
	
	
}

void SetupWait()
{
//	TryCount=0;
	ConfigTimer=CONFIG_FETCH_DELAY;
	
}

void PaintCommError()
{
	OP_State=OP_OFFLINE;	
	PaintFunctions();
//	strcpy(LCDBuff,"Network Error   ");
//	LCD_String(0,0);
//	strcpy(LCDBuff,"Enter to retry  ");
//	LCD_String(1,0);
}

int NextTx()
{
	CurrentTX++;
	if(CurrentTX>255)
		CurrentTX=0;
	return CurrentTX;
}


void SendOPMess(void)
{

	unsigned char SelHi;
	unsigned char SelLow;
	unsigned i,j;
	unsigned long UpdateTag;
	
	if(1)
	{
		SelHi = 0x04;
		SelLow = 0x00;

		
		// set outgoing address to be Subnet node based
//		msg_out.dest_addr.snode.domain = 0;
//		msg_out.dest_addr.snode.node = 12;
//		msg_out.dest_addr.snode.subnet = 2;
//		msg_out.dest_addr.snode.type=SUBNET_NODE;
	
		if((CurrentOPOption==OPTION_ZONE) ||(CurrentOPOption==OPTION_OP_ALL))
		{

			msg_out.dest_addr.bcast.domain = 0;
			msg_out.dest_addr.bcast.subnet = 0;	//domain broadcast
			msg_out.dest_addr.bcast.type=BROADCAST;
		
			msg_out.dest_addr.bcast.tx_timer=4;
		}	
		else
		{
//			msg_out.dest_addr.nrnid.domain = 0;
//			for(i=0;i<6;i++)
//				msg_out.dest_addr.nrnid.nid[i] = DspNeuronID[i];
//			msg_out.dest_addr.nrnid.subnet = 2;
//			msg_out.dest_addr.nrnid.type=NEURON_ID;
//		
//			msg_out.dest_addr.nrnid.tx_timer=4;
			// set outgoing address to be Subnet node based
			msg_out.dest_addr.snode.domain = 0;
			msg_out.dest_addr.snode.node = OP_Node;
			msg_out.dest_addr.snode.subnet = OP_Subnet;
			msg_out.dest_addr.snode.type=SUBNET_NODE;
			msg_out.dest_addr.snode.retry =1;
			msg_out.dest_addr.snode.tx_timer=6;
			
		}
		
		
		msg_out.tag = PokeNV;
	    msg_out.service = UNACKD;
	    msg_out.code = 0x80 | SelHi;
	
		msg_out.data[0] = SelLow;	//set index
	
		msg_out.data[1] =TempCommand.OPAddress; //op num
		msg_out.data[2] =TempCommand.OPGroupSelect;	//indiv
		msg_out.data[3] =TempCommand.OPCommandCat;	//mask
		msg_out.data[4] =TempCommand.OPSubCommand; //ch1
		msg_out.data[5] =TempCommand.OPTransactionID;	//tx
		msg_out.data[6] =TempCommand.ByteCount;	//byte count
		for(i=0;i<25;i++)
		{
			msg_out.data[7+i]=TempCommand.OPData[i];
			
		}
	
	
	    msg_send();
  }
  
  
}

unsigned short ReadRelays()
{
	boolean result;
#ifdef COMBO_UNIT
	return 0;
#endif
		
	SelectBus(BUS_A);
	I2C_OutBuff[0]=0x13;	// this is the address of GPIOA. B is right after it. So, just do a write to set teh address register to 12
	result = io_out(ioI2C, &I2C_OutBuff, AUX_RELAY_ADDR, 1);	// this just sends the address byte. no data bytes are written
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,AUX_RELAY_I2C);

	result = io_in(ioI2C, I2C_InBuff, AUX_RELAY_ADDR, 1);	// now read in the A gpio register
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,AUX_RELAY_I2C);
	//SendDebug(0x05,I2C_InBuff[0]);
	return 1;
}

unsigned short ReadSettingsIO()
{
	boolean result;
	
	SelectBus(CFG_BUS);
	I2C_OutBuff[0]=0x13;	// this is the address of GPIOB. . So, just do a write to set teh address register to 12
	result = io_out(ioI2C, &I2C_OutBuff, CFG_FLED_LED_ADDR, 1);	// this just sends the address byte. no data bytes are written
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,CFG_FLED_I2C);

	result = io_in(ioI2C, I2C_InBuff, CFG_FLED_LED_ADDR, 1);	// now read in the A gpio register
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,CFG_FLED_I2C);
	
	PcbVer=I2C_InBuff[0] & 0x0f;

#ifdef COMBO_UNIT
	if(PcbVer==0)
		PcbVer++;	//fake it out to look like the second version of 1u headend
#endif

	//Sw 4 is inet vs nform off=inet, on = nform
	if(I2C_InBuff[0] & 0x10)
		DeviceInet=TRUE;
	else
		DeviceInet=FALSE;
	
	//sw 3 is for nform facp multizone page io input on=enabled

	if(I2C_InBuff[0] & 0x20)
		DeviceFACPZoneInput=FALSE;
	else
		DeviceFACPZoneInput=TRUE;

	

//	SendDebug(0x06,I2C_InBuff[0]);
//	SendDebug(0x07,DeviceInet);
//	SendDebug(0x08,PcbVer);
	
	return 1;
}

when(timer_expires(ButtonBounce))
{
	Bouncing=FALSE;	
}


void ProcessSw(enum SWITCHES sw)
{
	Bouncing=TRUE;
	ButtonBounce=100;
	
	switch(sw)
	{
		case SWITCH_1:
			if(DeviceInet)
				FunctionChangeInet();
			else
				FunctionChangeNform();
//			FunctionChange();
			break;
		case SWITCH_2:
			OptionChange();
			break;
		case SWITCH_3:
			SelectChange();
			break;
		case SWITCH_4:
			ItemUp();
			break;
		case SWITCH_5:
			ItemDown();
			break;
	}
	
}

void GetVoltage(unsigned Selector)
{
	//ch 0 = 3.3v
	//ch 1 = 5v
	//ch 2 =iso 5v
	//ch 3 = 24v
	unsigned long A2DValue;
	float_type ScaledVal;
	unsigned int SelectedBus;
	unsigned int SelectedAddress;
	


#ifdef COMBO_UNIT
	switch(Selector)
	{
		case A2D_V1POINT26:
		case A2D_V1POINT26_2:
		case A2D_V3POINT3:
		case A2D_V3POINT3_2:
		case A2D_V5:
		case A2D_V5_2:
		case A2D_V15:
		case A2D_V15_2:
				SelectedBus=VOLTAGE_BUS;
				SelectedAddress=VOLTAGE_A2D_ADDR;
				break;
		case A2D_V3POINT3_H:
		case A2D_V5_H:
		case A2D_V5_ISO_H:
		case A2D_VM5:
				SelectedBus=VOLTAGE_BUS;
				SelectedAddress=VOLT_IO_A2D_ADDR;
				break;
		
	}
#else
	switch(Selector)
	{
		case A2D_V3POINT3:
		case A2D_V5:
		case A2D_V_ISO5:
		case A2D_V24:
				SelectedBus=VOLTAGE_BUS;
				SelectedAddress=VOLTAGE_A2D_ADDR;
				break;
	}	
#endif
	

	SelectBus(SelectedBus);
	
#ifdef COMBO_UNIT
	if(SelectedAddress==VOLT_IO_A2D_ADDR)
		I2C_OutBuff[0]=0x84 |((Selector-10)<<4);	//start conversion on ch 0 we had to offset the selectors so that the case statemnets didnt have teh same values
	else
#endif
		I2C_OutBuff[0]=0x84 |(Selector<<4);	//start conversion on ch 0



	
	result = io_out(ioI2C, &I2C_OutBuff, SelectedAddress, 1);
	result = io_in(ioI2C, I2C_InBuff, SelectedAddress, 2);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,VOLTAGE_A2D_I2C);
	//SendDebug(I2C_InBuff[0],I2C_InBuff[1]);
	A2DValue=I2C_InBuff[0];
	A2DValue=A2DValue<<8;
	A2DValue=A2DValue|I2C_InBuff[1];
	
	switch(Selector)
	{
#ifdef COMBO_UNIT
		case A2D_V1POINT26:
			fl_from_ulong(A2DValue,&V1Point26Volt);
			fl_mul(&V1Point26Volt,&Scaler3, &V1Point26Volt);
			break;
		case A2D_V1POINT26_2:
			fl_from_ulong(A2DValue,&V1Point26Volt_2);
			fl_mul(&V1Point26Volt_2,&Scaler3, &V1Point26Volt_2);
			break;
		case A2D_V3POINT3:
			fl_from_ulong(A2DValue,&V3Point3Volt);
			fl_mul(&V3Point3Volt,&Scaler3, &V3Point3Volt);
			break;
		case A2D_V3POINT3_2:
			fl_from_ulong(A2DValue,&V3Point3Volt_2);
			fl_mul(&V3Point3Volt_2,&Scaler3, &V3Point3Volt_2);
			break;
		case A2D_V3POINT3_H:
			fl_from_ulong(A2DValue,&V3Point3Volt_H);
			fl_mul(&V3Point3Volt_H,&Scaler3, &V3Point3Volt_H);
			break;
		case A2D_V5:
			fl_from_ulong(A2DValue,&V5Volt);
			fl_mul(&V5Volt,&Scaler3, &V5Volt);
			break;
		case A2D_V5_2:
			fl_from_ulong(A2DValue,&V5Volt_2);
			fl_mul(&V5Volt_2,&Scaler3, &V5Volt_2);
			break;
		case A2D_V5_H:
			fl_from_ulong(A2DValue,&V5Volt_H);
			fl_mul(&V5Volt_H,&Scaler3, &V5Volt_H);
			break;
		case A2D_VM5:
			fl_from_ulong(A2DValue,&V5Volt_M);
			fl_mul(&V5Volt_M,&Scaler4, &V5Volt_M);
			break;
		case A2D_V5_ISO_H:
			fl_from_ulong(A2DValue,&V5Volt_ISO_H);
			fl_mul(&V5Volt_ISO_H,&Scaler3, &V5Volt_ISO_H);
			break;
		case A2D_V15:
			fl_from_ulong(A2DValue,&V15Volt);
			fl_mul(&V15Volt,&Scaler15, &V15Volt);
			break;
//		case A2D_V15_2:
//			fl_from_ulong(A2DValue,&V15Volt_2);
//			fl_mul(&V15Volt_2,&Scaler15, &V15Volt_2);
//			break;
#else
		case A2D_V3POINT3:
			fl_from_ulong(A2DValue,&V3Point3Volt);
			fl_mul(&V3Point3Volt,&Scaler, &V3Point3Volt);
			break;
		case A2D_V5:
			fl_from_ulong(A2DValue,&V5Volt);
			fl_mul(&V5Volt,&Scaler2, &V5Volt);
			break;
		case A2D_V_ISO5:
			fl_from_ulong(A2DValue,&V5isoVolt);
			fl_mul(&V5isoVolt,&Scaler2, &V5isoVolt);
			break;
		case A2D_V24:
			fl_from_ulong(A2DValue,&V24Volt);
			fl_mul(&V24Volt,&Scaler24, &V24Volt);
			break;

#endif
	
	}
	
}


void SetupUart()
{
	boolean result;
	char temp;
	int i;
	

	SelectBus(BUS_C);
//read reg 3
	I2C_OutBuff[0]=0x18;	//reg 0x03
//	I2C_OutBuff[1]=0xE0;	//make mix for A
//	I2C_OutBuff[2]=0x00;	//make all out For B
	result = io_out(ioI2C, &I2C_OutBuff, UART_ADDR, 1);	//set the reg address
	if(!result)
	{
		CPUFault(DEVICE_ID_1UHE_IO,UART_I2C);
		return;
	}
	result = io_in(ioI2C, InBuff, UART_ADDR, 1);//reda in teh reg value
	if(!result)
	{
		CPUFault(DEVICE_ID_1UHE_IO,UART_I2C);
		return;
	}

//write reg 3 back 
	I2C_OutBuff[0] = 0x18;	//reg 0x03
	I2C_OutBuff[1] = InBuff[0]|0x80;	//select enable for baud rate reg
	temp=InBuff[0];
	result = io_out(ioI2C, &I2C_OutBuff, UART_ADDR, 2);	//set the reg address
	if(!result)
	{
		CPUFault(DEVICE_ID_1UHE_IO,UART_I2C);
		return;
	}

	I2C_OutBuff[0]=0x18;	//reg 0x03
//	I2C_OutBuff[1]=0xE0;	//make mix for A
//	I2C_OutBuff[2]=0x00;	//make all out For B
	result = io_out(ioI2C, &I2C_OutBuff, UART_ADDR, 1);	//set the reg address
	if(!result)
	{
		CPUFault(DEVICE_ID_1UHE_IO,UART_I2C);
		return;
	}
	result = io_in(ioI2C, InBuff, UART_ADDR, 1);//reda in teh reg value
	if(!result)
	{
		CPUFault(DEVICE_ID_1UHE_IO,UART_I2C);
		return;
	}

//write reg 0 Low
	I2C_OutBuff[0] = 0x00;	//reg 0x00 dll
	I2C_OutBuff[1] = 1;	//baud rate div 1
//	I2C_OutBuff[2] = 0;	//baud rate div 1
	result = io_out(ioI2C, &I2C_OutBuff, UART_ADDR, 2);	//set the divisor
	if(!result)
	{
		CPUFault(DEVICE_ID_1UHE_IO,UART_I2C);
		return;
	}

//write reg 1 High
	I2C_OutBuff[0] = 0x08;	//reg 0x00 dll
	I2C_OutBuff[1] = 0;	//baud rate div 1
//	I2C_OutBuff[2] = 0;	//baud rate div 1
	result = io_out(ioI2C, &I2C_OutBuff, UART_ADDR, 2);	//set the divisor
	if(!result)
	{
		CPUFault(DEVICE_ID_1UHE_IO,UART_I2C);
		return;
	}


//read it back low
	I2C_OutBuff[0]=0x0;	//reg 0x00	
//	I2C_OutBuff[1]=0xE0;	//make mix for A
//	I2C_OutBuff[2]=0x00;	//make all out For B
	result = io_out(ioI2C, &I2C_OutBuff, UART_ADDR, 1);	//set the reg address
	if(!result)
	{
		CPUFault(DEVICE_ID_1UHE_IO,UART_I2C);
		return;
	}
	result = io_in(ioI2C, InBuff, UART_ADDR, 1);//reda in teh reg value
	if(!result)
	{
		CPUFault(DEVICE_ID_1UHE_IO,UART_I2C);
		return;
	}
	
//read it back high
	I2C_OutBuff[0]=0x08;	//reg 0x00	
//	I2C_OutBuff[1]=0xE0;	//make mix for A
//	I2C_OutBuff[2]=0x00;	//make all out For B
	result = io_out(ioI2C, &I2C_OutBuff, UART_ADDR, 1);	//set the reg address
	if(!result)
	{
		CPUFault(DEVICE_ID_1UHE_IO,UART_I2C);
		return;
	}
	result = io_in(ioI2C, InBuff, UART_ADDR, 1);//reda in teh reg value
	if(!result)
	{
		CPUFault(DEVICE_ID_1UHE_IO,UART_I2C);
		return;
	}

//write reg 3 back 
	I2C_OutBuff[0] = 0x18;	//reg 0x03
	I2C_OutBuff[1] = 0x13;	//set new lcr
	result = io_out(ioI2C, &I2C_OutBuff, UART_ADDR, 2);	//set the reg address
	if(!result)
	{
		CPUFault(DEVICE_ID_1UHE_IO,UART_I2C);
		return;
	}


//read  back lcr
	I2C_OutBuff[0]=0x18;	//reg 0x00	
//	I2C_OutBuff[1]=0xE0;	//make mix for A
//	I2C_OutBuff[2]=0x00;	//make all out For B
	result = io_out(ioI2C, &I2C_OutBuff, UART_ADDR, 1);	//set the reg address
	if(!result)
	{
		CPUFault(DEVICE_ID_1UHE_IO,UART_I2C);
		return;
	}
	result = io_in(ioI2C, InBuff, UART_ADDR, 3);//reda in teh reg value
	if(!result)
	{
		CPUFault(DEVICE_ID_1UHE_IO,UART_I2C);
		return;
	}

	
//	result = io_in(ioI2C, InBuff, UART_ADDR, 1);//reda in teh reg value
	
//set IER	reg 1 0x08 after shift
	I2C_OutBuff[0] = 0x08;	//reg 0x03
	I2C_OutBuff[1] = 0x01;	//set new IER
//	I2C_OutBuff[2] = 0x01;	//set new FCR	sets 8 char for the fifo and enable fifo. maybe set tlr
	result = io_out(ioI2C, &I2C_OutBuff, UART_ADDR, 2);	//set the reg address

//set FCR reg 2 0x10 after shift
	I2C_OutBuff[0] = 0x10;	//reg fcr
//	I2C_OutBuff[1] = 0x01;	//set new IER
//	I2C_OutBuff[1] = 0x01;	//set new FCR	sets 8 char for the fifo and enable fifo. maybe set tlr
	I2C_OutBuff[1] = 0x43;	//set new FCR	sets 16 char for the fifo and enable fifo. reset receive fifo
	result = io_out(ioI2C, &I2C_OutBuff, UART_ADDR, 2);	//set the reg address
	
	return;
	
}

void GetSupervisedInputs()
{
	boolean result;
	unsigned long Buttons;
	unsigned short Channel=0;
	signed long Differential,C1,C2;
	unsigned short SuperIndex;
	unsigned short Device;
	unsigned short I2CAdd;
	
#define AD_NO_HIGH_TRIP 2621
#define AD_NO_LOW_TRIP 1065
#define AD_NC_HIGH_TRIP 2048
#define AD_NC_LOW_TRIP 1393
#define AD_SHORTED_HIGH_TRIP 1638
#define AD_SHORTED_LOW_TRIP 410
#define AD_C1_SHORTED_LOW_TRIP 819
#define AD_C2_SHORTED_LOW_TRIP 819
#define AD_OPEN_HIGH_TRIP 3277
#define AD_OPEN_LOW_TRIP 819

	SelectBus(BUS_B);
	
#ifdef COMBO_UNIT
	for(Device=1;Device<=1;Device++)
#else
	for(Device=1;Device<=3;Device++)
#endif
	{
		switch(Device)
		{
			case 1:
				I2CAdd=	DIG_IN_A_ADDR;
				break;
			case 2:
				I2CAdd=	DIG_IN_B_ADDR;
				break;
			case 3:
				I2CAdd=	DIG_IN_C_ADDR;
				break;
				
		}
		
#ifdef COMBO_UNIT
		for(Channel=0;Channel<2;Channel++) 
#else
		for(Channel=0;Channel<8;Channel++) 
#endif
		{
			switch(Channel)
			{
				case 0:
						I2C_OutBuff[0]=0x84;
						break;
				case 1:
						I2C_OutBuff[0]=0xc4;
						break;
				case 2:
						I2C_OutBuff[0]=0x94;
						break;
				case 3:
						I2C_OutBuff[0]=0xd4;
						break;
				case 4:
						I2C_OutBuff[0]=0xa4;
						break;
				case 5:
						I2C_OutBuff[0]=0xe4;
						break;
				case 6:
						I2C_OutBuff[0]=0xb4;
						break;
				case 7:
						I2C_OutBuff[0]=0xf4;
						break;
						
			}
			
			result = io_out(ioI2C, &I2C_OutBuff, I2CAdd, 1);
			if(!result)
			{
				CPUFault(DEVICE_ID_1UHE_IO,SUP_INPUTS_I2C);
				return;
			}
			result = io_in(ioI2C, I2C_InBuff, I2CAdd,2);
			if(!result)
			{
				CPUFault(DEVICE_ID_1UHE_IO,SUP_INPUTS_I2C);
				return;
			}
			A2DValue[Device-1][Channel]=I2C_InBuff[0];
			A2DValue[Device-1][Channel]=A2DValue[Device-1][Channel]<<8;
			A2DValue[Device-1][Channel]=A2DValue[Device-1][Channel]|I2C_InBuff[1];
		}
	}

#ifdef COMBO_UNIT
	for(Device=1;Device<=1;Device++)
#else
	for(Device=1;Device<=3;Device++)
#endif
	{

		SuperIndex=0;
#ifdef COMBO_UNIT
		for(Channel=0;Channel<2;Channel=Channel+2) 
#else
		for(Channel=0;Channel<8;Channel=Channel+2) 
#endif

		{

			Differential = A2DValue[Device-1][Channel] - A2DValue[Device-1][Channel+1];
			C1=A2DValue[Device-1][Channel];
			C2=A2DValue[Device-1][Channel+1];
		
		
			if(InputSupervised(Device,SuperIndex))
			{	
				
				if((C1> 0xab4) && (C2< 0x54b) && (Differential < 0xbe7))
				{
					SuperIOState[Device-1][SuperIndex]=CONTACT_OPEN;
					BounceWatch(Device,SuperIndex);
					if(DebouncePass(Device,SuperIndex))
					{
						SetSwFault(Device,SuperIndex,FALSE);	//clear any led fault bits for this
						Switch2Led(Device,SuperIndex,FALSE);	//turn led off
						if(DeviceInet)
						{
							if((Device==1) && (SuperIndex==0))
							{
								if(CountSuperIOState[Device-1][SuperIndex] == (SUPER_BOUNCE+1))
									GlobalMute(FALSE);
							}
						}
						else
						{
							if(CountSuperIOState[Device-1][SuperIndex] == (SUPER_BOUNCE+1))
								SetForSend(Device,SuperIndex,CONTACT_OPEN);
						}
						
					}
				}

				else if((C1> 0x8c0) && (C2< 0x73f)&& (Differential < 0x56a))
				{
					SuperIOState[Device-1][SuperIndex]=CONTACT_CLOSED;
					BounceWatch(Device,SuperIndex);
					if(DebouncePass(Device,SuperIndex))
					{
						SetSwFault(Device,SuperIndex,FALSE);	//clear any led fault bits for this
						Switch2Led(Device,SuperIndex,TRUE);	//turn led on
						if(DeviceInet)
						{
							if((Device==1) && (SuperIndex==0))
							{
								if(CountSuperIOState[Device-1][SuperIndex] == (SUPER_BOUNCE+1))
									GlobalMute(TRUE);
							}
							
							if((Device==3) && (SuperIndex==2))
							{
								if(CountSuperIOState[Device-1][SuperIndex] == (SUPER_BOUNCE+1))
									GenFault(DEVICE_ID_1UHE_IO,UPS_AC_FAIL);	//ups ac fail fault
							}
							if((Device==3) && (SuperIndex==3))
							{
								if(CountSuperIOState[Device-1][SuperIndex] == (SUPER_BOUNCE+1))
									GenFault(DEVICE_ID_1UHE_IO,UPS_BATT_FAIL);	//ups batt fail fault
							}

						}
						else
						{
							if((Device==3) && (SuperIndex==2))
							{
								if(CountSuperIOState[Device-1][SuperIndex] == (SUPER_BOUNCE+1))
									GenFault(DEVICE_ID_1UHE_IO,UPS_AC_FAIL);	//ups ac fail fault
							}
							else if((Device==3) && (SuperIndex==3))
							{
								if(CountSuperIOState[Device-1][SuperIndex] == (SUPER_BOUNCE+1))
									GenFault(DEVICE_ID_1UHE_IO,UPS_BATT_FAIL);	//ups batt fail fault
							}
							else
							{
								if(CountSuperIOState[Device-1][SuperIndex] == (SUPER_BOUNCE+1))
									SetForSend(Device,SuperIndex,CONTACT_CLOSED);
							}
						}
					}
						
				}


				else if((C1< 0x8c0) && (C2> 0x73f) && (Differential < 0x181))
				{
						SuperIOState[Device-1][SuperIndex]=SHORT_C1_TO_C2;
						BounceWatch(Device,SuperIndex);
						if(DebouncePass(Device,SuperIndex))
							SetGenFault(SuperIndex,Device,SHORT_C1_TO_C2);
				}

				else if((C1< 0x333)  && (C2< 0x333)&& (Differential< 0x333))
				{
						SuperIOState[Device-1][SuperIndex]=SHORT_C1_TO_GND;
						BounceWatch(Device,SuperIndex);
						if(DebouncePass(Device,SuperIndex))
							SetGenFault(SuperIndex,Device,SHORT_C1_TO_GND);
				}

				else if((C1> 0xd3f) && (C2< 0x800)&& (Differential> 0x800))
				{
						SuperIOState[Device-1][SuperIndex]=OPEN;
						BounceWatch(Device,SuperIndex);
						if(DebouncePass(Device,SuperIndex))
							SetGenFault(SuperIndex,Device,OPEN);
				}

				else if((C1> 0xa00) && (C2< 0x333)&& (Differential> 0x928) )
				{
						SuperIOState[Device-1][SuperIndex]=SHORT_C2_TO_GND;
						BounceWatch(Device,SuperIndex);
						if(DebouncePass(Device,SuperIndex))
							SetGenFault(SuperIndex,Device,SHORT_C2_TO_GND);
				}

				else if((C1< 0x800) && (C2< 0x333)&& (Differential> 0x333) )
				{
						SuperIOState[Device-1][SuperIndex]=SHORT_C2_TO_GND;
						BounceWatch(Device,SuperIndex);
						if(DebouncePass(Device,SuperIndex))
							SetGenFault(SuperIndex,Device,SHORT_C2_TO_GND);
				}

				else
				{
						SuperIOState[Device-1][SuperIndex]=UNKNOWN_IO;
						BounceWatch(Device,SuperIndex);
						if(DebouncePass(Device,SuperIndex))
							SetGenFault(SuperIndex,Device,UNKNOWN_IO);
				}
			//		SendDebug(0x02,SuperIOState[Device-1][SuperIndex]);

			}
			else if(InputNormal(Device,SuperIndex))
			{
				//we are not supervising so just look for open or closed
				if((C1> 0xd3f) && (C2< 0x800)&& (Differential> 0x800))
				{
					SuperIOState[Device-1][SuperIndex]=CONTACT_OPEN;	//this is the same as a supervised OPEN
					BounceWatch(Device,SuperIndex);
					if(DebouncePass(Device,SuperIndex))
					{
						SetSwFault(Device,SuperIndex,FALSE);	//clear any led fault bits for this
						Switch2Led(Device,SuperIndex,FALSE);	//turn led off
						if(DeviceInet)
						{
							if((Device==1) && (SuperIndex==0))
							{
								if(CountSuperIOState[Device-1][SuperIndex] == (SUPER_BOUNCE+1))
									GlobalMute(FALSE);
							}
						}
						else
						{
							if(CountSuperIOState[Device-1][SuperIndex] == (SUPER_BOUNCE+1))
								SetForSend(Device,SuperIndex,CONTACT_OPEN);
						}

					}
				}
				else
				{
					SuperIOState[Device-1][SuperIndex]=CONTACT_CLOSED;	//this is the same as a supervised SHORT_C1_TO_C2
					BounceWatch(Device,SuperIndex);
					if(DebouncePass(Device,SuperIndex))
					{
						SetSwFault(Device,SuperIndex,FALSE);	//clear any led fault bits for this
						Switch2Led(Device,SuperIndex,TRUE);	//turn led on
						if(DeviceInet)
						{
							if((Device==1) && (SuperIndex==0))
							{
								if(CountSuperIOState[Device-1][SuperIndex] == (SUPER_BOUNCE+1))
									GlobalMute(TRUE);
							}
							if((Device==3) && (SuperIndex==2))
							{
								if(CountSuperIOState[Device-1][SuperIndex] == (SUPER_BOUNCE+1))
									GenFault(DEVICE_ID_1UHE_IO,UPS_AC_FAIL);	//ups ac fail fault
							}
							if((Device==3) && (SuperIndex==3))
							{
								if(CountSuperIOState[Device-1][SuperIndex] == (SUPER_BOUNCE+1))
									GenFault(DEVICE_ID_1UHE_IO,UPS_BATT_FAIL);	//ups batt fail fault
							}
						}
						else
						{
							if((Device==3) && (SuperIndex==2))
							{
								if(CountSuperIOState[Device-1][SuperIndex] == (SUPER_BOUNCE+1))
									GenFault(DEVICE_ID_1UHE_IO,UPS_AC_FAIL);	//ups ac fail fault
							}
							else if((Device==3) && (SuperIndex==3))
							{
								if(CountSuperIOState[Device-1][SuperIndex] == (SUPER_BOUNCE+1))
									GenFault(DEVICE_ID_1UHE_IO,UPS_BATT_FAIL);	//ups batt fail fault
							}
							else
							{
								if(CountSuperIOState[Device-1][SuperIndex] == (SUPER_BOUNCE+1))
									SetForSend(Device,SuperIndex,CONTACT_CLOSED);
							}
						}
					}
				}
				
				
			}
			
			
			SuperIndex++;
		}
	}
	//SendDebug(0x8b,SuperIOState[0]);		
	//SendDebug(0x8c,UL2572Mode);		


}

void SetForSend(unsigned short Device,unsigned short SuperIndex,unsigned short SwState)
{
	StatusIOState[Device-1][SuperIndex]=SwState;	//set a flag so we know we have to send it
}

void CheckSendIO()
{
	unsigned short Device,SuperIndex;
	unsigned short IONum=0;
	
	for(Device=0;Device<3;Device++)
	{
		for(SuperIndex=0;SuperIndex<4;SuperIndex++)
		{	
//			if((Device==0)&&(SuperIndex==0))
//			{	
//				//do nothing as this is the mute masking
//			}
//			else
//			{
				IONum++;
				if((StatusIOState[Device][SuperIndex]==CONTACT_CLOSED) || (StatusIOState[Device][SuperIndex]==CONTACT_OPEN))
				{
					if(IONum<11)
					{
						if(DeviceFACPZoneInput)
						{
							SendOpsFireZones();		//something changed so send all inputs to OPs
							ClearSwitchStates();
							return;	//we sent everything
						}
						else
						{
							SendMPICommand(SwitchNumber(Device,SuperIndex),StatusIOState[Device][SuperIndex]);
							StatusIOState[Device][SuperIndex]=SENT_IO_UPDATE;
							return;	//dont send anymore. Wait till next cycle
						}
					}
					
				}
		//	}
		}
	}
}

void SendOpsFireZones()
{
	unsigned char SelHi;
	unsigned char SelLow;
	unsigned i,j;
	unsigned long UpdateTag;
	unsigned short Device,SuperIndex;
	unsigned short PackedByte;
	
	
	if(FireTx==255)
		FireTx=1;	//do not use 0
	else
		FireTx++;
	
		
	
		SelHi = 0x04;
		SelLow = 0x00;

			msg_out.dest_addr.bcast.domain = 0;
			msg_out.dest_addr.bcast.subnet = 0;	//domain broadcast
			msg_out.dest_addr.bcast.type=BROADCAST;
		
			msg_out.dest_addr.bcast.tx_timer=4;
		msg_out.dest_addr.bcast.retry=1;
		msg_out.dest_addr.bcast.rpt_timer=4;
		msg_out.tag = PokeNV;
	    msg_out.service = UNACKD_RPT;
	    msg_out.code = 0x80 | SelHi;
	
		msg_out.data[0] = SelLow;	//set index
	
		msg_out.data[1] =0; //op num
		msg_out.data[2] =127;	//all ops
		msg_out.data[3] =PAGE_ZONE;	//page zone command
		msg_out.data[4] =20; //sub command
		msg_out.data[5] =66;	//fake tx
		msg_out.data[6] =31;	//byte count
		msg_out.data[7]=FireTx;

	
//first 5 switches are non fire/fire
		PackedByte=0;
		if(SuperIOState[0][0]==CONTACT_CLOSED)
			PackedByte=PackedByte | 0x01;
		else
			PackedByte=PackedByte & 0xfe;
		
		if(SuperIOState[0][1]==CONTACT_CLOSED)
			PackedByte=PackedByte | 0x02;
		else
			PackedByte=PackedByte & 0xfd;

		if(SuperIOState[0][2]==CONTACT_CLOSED)
			PackedByte=PackedByte | 0x04;
		else
			PackedByte=PackedByte & 0xfb;

		if(SuperIOState[0][3]==CONTACT_CLOSED)
			PackedByte=PackedByte | 0x08;
		else
			PackedByte=PackedByte & 0xf7;

		if(SuperIOState[1][0]==CONTACT_CLOSED)
			PackedByte=PackedByte | 0x10;
		else
			PackedByte=PackedByte & 0xef;
		
//		if(SuperIOState[1][1]==CONTACT_CLOSED)
//			PackedByte=PackedByte | 0x20;
//		else
//			PackedByte=PackedByte & 0xdf;

		msg_out.data[8]=PackedByte;
		PackedByte=0;

		for(i=0;i<5;i++)
		{
			msg_out.data[9+i]=0;	//unused fire zones
			
		}


//Next 5 switches are Advisory page inputs

		if(SuperIOState[1][1]==CONTACT_CLOSED)
			PackedByte=PackedByte | 0x01;
		else
			PackedByte=PackedByte & 0xfe;

		if(SuperIOState[1][2]==CONTACT_CLOSED)
			PackedByte=PackedByte | 0x02;
		else
			PackedByte=PackedByte & 0xfd;

		if(SuperIOState[1][3]==CONTACT_CLOSED)
			PackedByte=PackedByte | 0x04;
		else
			PackedByte=PackedByte & 0xfb;

		
		if(SuperIOState[2][0]==CONTACT_CLOSED)
			PackedByte=PackedByte | 0x08;
		else
			PackedByte=PackedByte & 0xf7;

		if(SuperIOState[2][1]==CONTACT_CLOSED)
			PackedByte=PackedByte | 0x10;
		else
			PackedByte=PackedByte & 0xef;

//		if(SuperIOState[2][2]==CONTACT_CLOSED)
//			PackedByte=PackedByte | 0x20;
//		else
//			PackedByte=PackedByte & 0xdf;

		msg_out.data[14]=PackedByte;

		for(i=0;i<5;i++)
		{
			msg_out.data[15+i]=0;	//unused non fire zone 
			
		}


		for(i=0;i<6;i++)
		{
			msg_out.data[20+i]=0;	//unused future zones
			
		}
	
		msg_out.data[26]=1;	//fire zone source
		msg_out.data[27]=2;	//Non fire zone source
		msg_out.data[28]=3;	//Advisory page source
		msg_out.data[29]=4;	//future source
		msg_out.data[30]=4;	//future source
		msg_out.data[31]=4;	//future source
		
		
	
	    msg_send();
  
  	
}



void ClearSwitchStates()
{
	unsigned short Device,SuperIndex;
	
	for(Device=0;Device<3;Device++)
	{
		for(SuperIndex=0;SuperIndex<4;SuperIndex++)
		{	
			StatusIOState[Device][SuperIndex]=SENT_IO_UPDATE;	
		}
	}
}


unsigned short SwitchNumber(unsigned short Device, unsigned short SuperIndex)
{
	unsigned short RetVal=0;
	
	switch(Device)
	{
		case 0:
			RetVal= 1+SuperIndex;
			break;
		case 1:
			RetVal= 5+SuperIndex;
			break;
		case 2:
			RetVal= 9+SuperIndex;
			break;
	}
	
	return(RetVal);		
	
}

void GlobalMute(boolean MuteState)
{
	MuteCount=0;
	SendGlobalMute(MuteState,MUTE_TYPE_MASK); //send first one
	MuteRepeat=MUTE_REPEAT;
	CurrentMuteState=MuteState;	//save current state
	CurrentMuteType=MUTE_TYPE_MASK;
}

when(timer_expires(MuteRepeat))
{
	MuteCount++;
	if(MuteCount>=NUM_MUTE_MESSAGES)
	{
		if(CurrentMuteType==MUTE_TYPE_MASK)
		{
			CurrentMuteType=MUTE_TYPE_MUSIC;	//now lets do mute music
			MuteRepeat=MUTE_REPEAT;
			MuteCount=0;
		}
		else
			MuteRepeat=0;	//stop timer
	}
	SendGlobalMute(CurrentMuteState,CurrentMuteType); //send state again
	
}


void SendGlobalMute(boolean MuteState,unsigned MuteType)
{
		unsigned char SelHi;
	unsigned char SelLow;
	unsigned i,j;
	unsigned long UpdateTag;
	
	if(1)
	{
		SelHi = 0x04;
		SelLow = 0x00;

		
	

			msg_out.dest_addr.bcast.domain = 0;
			msg_out.dest_addr.bcast.subnet = 0;	//domain broadcast
			msg_out.dest_addr.bcast.type=BROADCAST;
		
			msg_out.dest_addr.bcast.tx_timer=4;
		msg_out.dest_addr.bcast.retry=1;
		msg_out.dest_addr.bcast.rpt_timer=4;
		msg_out.tag = PokeNV;
	    msg_out.service = UNACKD_RPT;
	    msg_out.code = 0x80 | SelHi;
	
		msg_out.data[0] = SelLow;	//set index
	
		msg_out.data[1] =0; //op num
		msg_out.data[2] =127;	//indiv
		if(MuteType==MUTE_TYPE_MASK)
			msg_out.data[3] =MASK_ATTENUATION;	//mask
		else
			msg_out.data[3] =MUSIC_ATTENUATION;	//music
	
		msg_out.data[4] =20; //chall mute
		msg_out.data[5] =10+MuteState;	//fake tx
		msg_out.data[6] =7;	//byte count
		msg_out.data[7]=MuteState;
		for(i=1;i<25;i++)
		{
			msg_out.data[7+i]=0;
			
		}
	
	
	    msg_send();
  }
  
  
}




boolean InputSupervised(unsigned short Device,unsigned short Index)
{
	boolean retval=FALSE;
	unsigned short DeviceIndex;
	
	if(Device<=3)
	{
		if(Index<4)
		{
			DeviceIndex=(Device-1)*4;
			if(IO_Setting[DeviceIndex+Index] == IO_SUPERVISED)
				retval=TRUE;
			else
				retval=FALSE;
		}
	}
//
//	switch(Index)
//	{
//		case 0:
//				if((SuperEnableBits[Device-1] & 0x01) >0)
//					retval=TRUE;
//				break;
//		case 1:
//				if((SuperEnableBits[Device-1] & 0x02) >0)
//					retval=TRUE;
//				break;
//		case 2:
//				if((SuperEnableBits[Device-1] & 0x04) >0)
//					retval=TRUE;
//				break;
//		case 3:
//				if((SuperEnableBits[Device-1] & 0x08) >0)
//					retval=TRUE;
//				break;
//		case 4:
//				if((SuperEnableBits[Device-1] & 0x10) >0)
//					retval=TRUE;
//				break;
//		case 5:
//				if((SuperEnableBits[Device-1] & 0x20) >0)
//					retval=TRUE;
//				break;
//	}
	
	return(retval);
	
}

boolean InputNormal(unsigned short Device,unsigned short Index)
{
	boolean retval=FALSE;
	unsigned short DeviceIndex;
	
	if(Device<=3)
	{
		if(Index<4)
		{
			DeviceIndex=(Device-1)*4;
			if(IO_Setting[DeviceIndex+Index] == IO_NORMAL)
				retval=TRUE;
			else
				retval=FALSE;
		}
	}
	
	return(retval);
	
}
void ProcessSuperInputs()
{
			if(SuperIOState[0][0] == CONTACT_CLOSED)
			{
				//SendDebug(0x8d,2);		
				if(DebouncePass(1,0))
				{
					if(CountSuperIOState[0][0] == (SUPER_BOUNCE+1))
					{
						// take action						
					}
						//SendDebug(0x8d,3);		
				}
					
			}
			if(SuperIOState[0][0] == CONTACT_OPEN)
			{
				if(DebouncePass(1,0))
				{
					if(CountSuperIOState[0][0] == (SUPER_BOUNCE+1))
					{
						// take action
					}
				}
					
			}

}

void BounceWatch(unsigned short Device,unsigned short ioIndex)
{
	if(SuperIOState[Device-1][ioIndex]==LastSuperIOState[Device-1][ioIndex])
	{
			CountSuperIOState[Device-1][ioIndex]++;
			if(CountSuperIOState[Device-1][ioIndex]>100)
				CountSuperIOState[Device-1][ioIndex]=100;	//just stop it at 100
	}
	else
	{
			CountSuperIOState[Device-1][ioIndex]=1;
			LastSuperIOState[Device-1][ioIndex]=SuperIOState[Device-1][ioIndex];

	}
	
			
}

boolean DebouncePass(unsigned short Device,unsigned short ioIndex)
{
	boolean retval;
	
	if(CountSuperIOState[Device-1][ioIndex]>SUPER_BOUNCE)
		retval= TRUE;
	else
		retval= FALSE;
		
	return(retval);
}


void SetGenFault(unsigned int SwIndex,unsigned int Device, unsigned int Fault)
{
	switch(SwIndex)
	{
		case 0:
			switch(Device)
			{
				case 1:
					if(Fault==SHORT_C1_TO_C2)
						GenFault(DEVICE_ID_1UHE_IO,SIO_A1_SC1TOC2);
					else if(Fault==SHORT_C1_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_A1_SC1TOGND);
					else if(Fault==SHORT_C2_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_A1_SC2TOGND);
					else if(Fault==OPEN)
						GenFault(DEVICE_ID_1UHE_IO,SIO_A1_OPEN);
					else if(Fault==UNKNOWN_IO)
						GenFault(DEVICE_ID_1UHE_IO,SIO_A1_UNKNOWN);
					break;
				case 2:
					if(Fault==SHORT_C1_TO_C2)
						GenFault(DEVICE_ID_1UHE_IO,SIO_B1_SC1TOC2);
					else if(Fault==SHORT_C1_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_B1_SC1TOGND);
					else if(Fault==SHORT_C2_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_B1_SC2TOGND);
					else if(Fault==OPEN)
						GenFault(DEVICE_ID_1UHE_IO,SIO_B1_OPEN);
					else if(Fault==UNKNOWN_IO)
						GenFault(DEVICE_ID_1UHE_IO,SIO_B1_UNKNOWN);
					break;
				case 3:
					if(Fault==SHORT_C1_TO_C2)
						GenFault(DEVICE_ID_1UHE_IO,SIO_C1_SC1TOC2);
					else if(Fault==SHORT_C1_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_C1_SC1TOGND);
					else if(Fault==SHORT_C2_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_C1_SC2TOGND);
					else if(Fault==OPEN)
						GenFault(DEVICE_ID_1UHE_IO,SIO_C1_OPEN);
					else if(Fault==UNKNOWN_IO)
						GenFault(DEVICE_ID_1UHE_IO,SIO_C1_UNKNOWN);
					break;
			}
			break;
		case 1:
			switch(Device)
			{
				case 1:
					if(Fault==SHORT_C1_TO_C2)
						GenFault(DEVICE_ID_1UHE_IO,SIO_A2_SC1TOC2);
					else if(Fault==SHORT_C1_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_A2_SC1TOGND);
					else if(Fault==SHORT_C2_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_A2_SC2TOGND);
					else if(Fault==OPEN)
						GenFault(DEVICE_ID_1UHE_IO,SIO_A2_OPEN);
					else if(Fault==UNKNOWN_IO)
						GenFault(DEVICE_ID_1UHE_IO,SIO_A2_UNKNOWN);
					break;
				case 2:
					if(Fault==SHORT_C1_TO_C2)
						GenFault(DEVICE_ID_1UHE_IO,SIO_B2_SC1TOC2);
					else if(Fault==SHORT_C1_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_B2_SC1TOGND);
					else if(Fault==SHORT_C2_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_B2_SC2TOGND);
					else if(Fault==OPEN)
						GenFault(DEVICE_ID_1UHE_IO,SIO_B2_OPEN);
					else if(Fault==UNKNOWN_IO)
						GenFault(DEVICE_ID_1UHE_IO,SIO_B2_UNKNOWN);
					break;
				case 3:
					if(Fault==SHORT_C1_TO_C2)
						GenFault(DEVICE_ID_1UHE_IO,SIO_C2_SC1TOC2);
					else if(Fault==SHORT_C1_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_C2_SC1TOGND);
					else if(Fault==SHORT_C2_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_C2_SC2TOGND);
					else if(Fault==OPEN)
						GenFault(DEVICE_ID_1UHE_IO,SIO_C2_OPEN);
					else if(Fault==UNKNOWN_IO)
						GenFault(DEVICE_ID_1UHE_IO,SIO_C2_UNKNOWN);
					break;
			}
			break;
		case 2:
			switch(Device)
			{
				case 1:
					if(Fault==SHORT_C1_TO_C2)
						GenFault(DEVICE_ID_1UHE_IO,SIO_A3_SC1TOC2);
					else if(Fault==SHORT_C1_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_A3_SC1TOGND);
					else if(Fault==SHORT_C2_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_A3_SC2TOGND);
					else if(Fault==OPEN)
						GenFault(DEVICE_ID_1UHE_IO,SIO_A3_OPEN);
					else if(Fault==UNKNOWN_IO)
						GenFault(DEVICE_ID_1UHE_IO,SIO_A3_UNKNOWN);
					break;
				case 2:
					if(Fault==SHORT_C1_TO_C2)
						GenFault(DEVICE_ID_1UHE_IO,SIO_B3_SC1TOC2);
					else if(Fault==SHORT_C1_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_B3_SC1TOGND);
					else if(Fault==SHORT_C2_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_B3_SC2TOGND);
					else if(Fault==OPEN)
						GenFault(DEVICE_ID_1UHE_IO,SIO_B3_OPEN);
					else if(Fault==UNKNOWN_IO)
						GenFault(DEVICE_ID_1UHE_IO,SIO_B3_UNKNOWN);
					break;
				case 3:
					if(Fault==SHORT_C1_TO_C2)
						GenFault(DEVICE_ID_1UHE_IO,SIO_C3_SC1TOC2);
					else if(Fault==SHORT_C1_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_C3_SC1TOGND);
					else if(Fault==SHORT_C2_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_C3_SC2TOGND);
					else if(Fault==OPEN)
						GenFault(DEVICE_ID_1UHE_IO,SIO_C3_OPEN);
					else if(Fault==UNKNOWN_IO)
						GenFault(DEVICE_ID_1UHE_IO,SIO_C3_UNKNOWN);
					break;
			}
			break;
		case 3:
			switch(Device)
			{
				case 1:
					if(Fault==SHORT_C1_TO_C2)
						GenFault(DEVICE_ID_1UHE_IO,SIO_A4_SC1TOC2);
					else if(Fault==SHORT_C1_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_A4_SC1TOGND);
					else if(Fault==SHORT_C2_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_A4_SC2TOGND);
					else if(Fault==OPEN)
						GenFault(DEVICE_ID_1UHE_IO,SIO_A4_OPEN);
					else if(Fault==UNKNOWN_IO)
						GenFault(DEVICE_ID_1UHE_IO,SIO_A4_UNKNOWN);
					break;
				case 2:
					if(Fault==SHORT_C1_TO_C2)
						GenFault(DEVICE_ID_1UHE_IO,SIO_B4_SC1TOC2);
					else if(Fault==SHORT_C1_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_B4_SC1TOGND);
					else if(Fault==SHORT_C2_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_B4_SC2TOGND);
					else if(Fault==OPEN)
						GenFault(DEVICE_ID_1UHE_IO,SIO_B4_OPEN);
					else if(Fault==UNKNOWN_IO)
						GenFault(DEVICE_ID_1UHE_IO,SIO_B4_UNKNOWN);
					break;
				case 3:
					if(Fault==SHORT_C1_TO_C2)
						GenFault(DEVICE_ID_1UHE_IO,SIO_C4_SC1TOC2);
					else if(Fault==SHORT_C1_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_C4_SC1TOGND);
					else if(Fault==SHORT_C2_TO_GND)
						GenFault(DEVICE_ID_1UHE_IO,SIO_C4_SC2TOGND);
					else if(Fault==OPEN)
						GenFault(DEVICE_ID_1UHE_IO,SIO_C4_OPEN);
					else if(Fault==UNKNOWN_IO)
						GenFault(DEVICE_ID_1UHE_IO,SIO_C4_UNKNOWN);
					break;
			}
			break;
			
	}
	SetSwFault(Device,SwIndex,TRUE);	//set the fault flag for this switch position

	
}

void Switch2Led(unsigned short Device, unsigned short SwIndex, boolean State)
{
	switch(Device)
	{
		case 1:
			switch(SwIndex)
			{
				case 0:
					UpdateLEDStates(LED_FTR_1A,State);
					break;					
				case 1:
					UpdateLEDStates(LED_FTR_2A,State);
					break;					
				case 2:
					UpdateLEDStates(LED_FTR_3A,State);
					break;					
				case 3:
					UpdateLEDStates(LED_FTR_4A,State);
					break;
			}
			break;
		case 2:
			switch(SwIndex)
			{
				case 0:
					UpdateLEDStates(LED_FTR_1B,State);
					break;					
				case 1:
					UpdateLEDStates(LED_FTR_2B,State);
					break;					
				case 2:
					UpdateLEDStates(LED_FTR_3B,State);
					break;					
				case 3:
					UpdateLEDStates(LED_FTR_4B,State);
					break;
			}
			break;
		case 3:
			switch(SwIndex)
			{
				case 0:
					UpdateLEDStates(LED_FTR_1C,State);
					break;					
				case 1:
					UpdateLEDStates(LED_FTR_2C,State);
					break;					
				case 2:
					UpdateLEDStates(LED_FTR_3C,State);
					break;					
				case 3:
					UpdateLEDStates(LED_FTR_4C,State);
					break;
			}
			break;
	}						
	
}


void SetSwFault(unsigned short Device, unsigned short SwIndex, boolean State)
{
	switch(Device)
	{
		case 1:
			switch(SwIndex)
			{
				case 0:
					if(!State)
						FaultBitsA=FaultBitsA&(~LED_1A_MASK);	//clear the bit to turn clear fault
					else
						FaultBitsA=FaultBitsA|LED_1A_MASK; //set the bit to set fault
					break;					
				case 1:
					if(!State)
						FaultBitsA=FaultBitsA&(~LED_2A_MASK);	//clear the bit to turn clear fault
					else
						FaultBitsA=FaultBitsA|LED_2A_MASK; //set the bit to set fault
					break;					
				case 2:
					if(!State)
						FaultBitsA=FaultBitsA&(~LED_3A_MASK);	//clear the bit to turn clear fault
					else
						FaultBitsA=FaultBitsA|LED_3A_MASK; //set the bit to set fault
					break;					
				case 3:
					if(!State)
						FaultBitsA=FaultBitsA&(~LED_4A_MASK);	//clear the bit to turn clear fault
					else
						FaultBitsA=FaultBitsA|LED_4A_MASK; //set the bit to set fault
					break;					
			}	
			break;
		case 2:
			switch(SwIndex)
			{
				case 0:
					if(!State)
						FaultBitsA=FaultBitsA&(~LED_1B_MASK);	//clear the bit to turn clear fault
					else
						FaultBitsA=FaultBitsA|LED_1B_MASK; //set the bit to set fault
					break;					
				case 1:
					if(!State)
						FaultBitsA=FaultBitsA&(~LED_2B_MASK);	//clear the bit to turn clear fault
					else
						FaultBitsA=FaultBitsA|LED_2B_MASK; //set the bit to set fault
					break;					
				case 2:
					if(!State)
						FaultBitsA=FaultBitsA&(~LED_3B_MASK);	//clear the bit to turn clear fault
					else
						FaultBitsA=FaultBitsA|LED_3B_MASK; //set the bit to set fault
					break;					
				case 3:
					if(!State)
						FaultBitsA=FaultBitsA&(~LED_4B_MASK);	//clear the bit to turn clear fault
					else
						FaultBitsA=FaultBitsA|LED_4B_MASK; //set the bit to set fault
					break;					
			}	
			break;
		case 3:
			switch(SwIndex)
			{
				case 0:
					if(!State)
						FaultBitsB=FaultBitsB&(~LED_1C_MASK);	//clear the bit to turn clear fault
					else
						FaultBitsB=FaultBitsB|LED_1C_MASK; //set the bit to set fault
					break;					
				case 1:
					if(!State)
						FaultBitsB=FaultBitsB&(~LED_2C_MASK);	//clear the bit to turn clear fault
					else
						FaultBitsB=FaultBitsB|LED_2C_MASK; //set the bit to set fault
					break;					
				case 2:
					if(!State)
						FaultBitsB=FaultBitsB&(~LED_3C_MASK);	//clear the bit to turn clear fault
					else
						FaultBitsB=FaultBitsB|LED_3C_MASK; //set the bit to set fault
					break;					
				case 3:
					if(!State)
						FaultBitsB=FaultBitsB&(~LED_4C_MASK);	//clear the bit to turn clear fault
					else
						FaultBitsB=FaultBitsB|LED_4C_MASK; //set the bit to set fault
					break;					
			}	
			break;
	}
	

//SendDebug(0x08,FaultBitsA);


}


void WaitMessage()
{
	strcpy(LCDBuff," Enter Password ");
	LCD_String(0,0);
	strcpy(LCDBuff,"    to start    ");
	LCD_String(1,0);
	CurrentMode=STARTUP_MODE;
	//UsingZone=FALSE;
}
void StartMessage()
{
	strcpy(LCDBuff," Sound Masking  ");
	LCD_String(0,0);
	strcpy(LCDBuff,"    Headend    ");
	LCD_String(1,0);
//	i=ConvNumToBuf(LCDColor,2,0);	//print code
//	LCD_String(1,14);
	CurrentMode=STARTUP_MODE;
	//UsingZone=FALSE;
}


void DefaultDisplay()
{
	LCDControl=0x00;
	WriteLCDControl();	

	msec_delay(15);
	
	SendCommand(0x38); // 8 bit bus, 2 line, 5x8, need to delay by 37us
	msec_delay(DELAY37USEC);//wait 37 usec

	SendCommand(0x38);// 8 bit bus, 2 line, 5x8, need to delay by 37us
	msec_delay(DELAY37USEC);//wait 37 usec
	SendCommand(0x0c);// display on, cursor off, blink off
	msec_delay(DELAY37USEC); //wait 37 usec

	SendCommand(0x01);// display clear
	msec_delay(DELAY1POINT52MS); //wait 1.52ms

	SendCommand(0x06);// entry mode- cursor/blink moves right and ddram address inc by 1
	msec_delay(DELAY37USEC); //wait 37 usec


}

void SendCommand(unsigned int databyte)
{
	SelectBus(BUS_A);
	I2C_OutBuff[0]=0x0a;	//olat reg
	I2C_OutBuff[1]=databyte;
	result = io_out(ioI2C, &I2C_OutBuff, DATA_ADDR, 2);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,LCD_DATA_I2C);

	LCDControl=0x08;
	WriteLCDControl();	
	LCDControl=0x00;
	WriteLCDControl();	
}

void SendData(unsigned int databyte)
{
	SelectBus(BUS_A);
	
	I2C_OutBuff[0]=0x0a;	//olat reg
	I2C_OutBuff[1]=databyte;
	result = io_out(ioI2C, &I2C_OutBuff, DATA_ADDR, 2);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,LCD_DATA_I2C);

	LCDControl=0x28;
	WriteLCDControl();	

	LCDControl=0x20;
	WriteLCDControl();	
}

void WriteLCDControl()
{
		SelectBus(BUS_A);
		I2C_OutBuff[0]=0x0a;	//olat reg
		I2C_OutBuff[1]=LCDControl | LCDColor | FaultLeds;	//clear E, rw, RS
		result = io_out(ioI2C, &I2C_OutBuff, CONTROL_ADDR, 2);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,LCD_CONTROL_I2C);
}

boolean SelectBus(short Bus)
{



	switch(Bus)
	{
		case BUS_DIRECT:
			I2C_OutBuff[0]=0x00;	//disable all busses
			break;
		case BUS_A:
			I2C_OutBuff[0]=0x01;
			break;
		case BUS_B:
			I2C_OutBuff[0]=0x02;
			break;
		case BUS_C:
			I2C_OutBuff[0]=0x04;
			break;
		case BUS_D:
			I2C_OutBuff[0]=0x08;
			break;
			
	}

	result = io_out(ioI2C, &I2C_OutBuff, I2C_MUX_SW_ADDR, 1);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,I2C_MUX_SW_I2C);
	return result;
		
}


void CPUFault(unsigned int Neuron,unsigned int Fault)
{
	boolean NewFault=FALSE;

	//SendDebug(0x01,Fault);
	
	switch(Neuron)
	{
		case DEVICE_ID_1UHE_IO:
			if(Fault==NO_CPU_FAULTS)
			{
				ClearFaults(DEVICE_ID_1UHE_IO);
				HaveHEIOCPUFaults=FALSE;
				break;
			}
			if(Fault<NUM_CPU_FAULTS)
			{
				if(CpuFaults[Fault]==0)
					NewFault=TRUE;
				if(CpuFaults[Fault]<255)
					CpuFaults[Fault]=CpuFaults[Fault]+1;
				HaveHEIOCPUFaults=TRUE;
			}
			break;
			
			
	}	

	if(HaveHEIOCPUFaults)
		HaveCPUFaults=TRUE;
	else
		HaveCPUFaults=FALSE;
	

	if(HaveCPUFaults)
	{
			CPUFaultLED(1);	
			WriteAuxRelays(RELAY_1,0);
			if(NewFault)
			{
				LaunchFault();
				SendListenerFault(CPU_FAULT,Neuron,Fault);
			}
	
	}
	else
		CPUFaultLED(0);	
	
}

void ClearFaults(unsigned int Neuron)
{
	int i;
	
	switch(Neuron)
	{
		case DEVICE_ID_1UHE_IO:
			for(i=0;i<NUM_CPU_FAULTS;i++)
				CpuFaults[i]=0;
			for(i=0;i<NUM_GEN_FAULTS;i++)
			{
				GenFaults[i]=0;
			}
			HaveHEIOCPUFaults=FALSE;
			HaveHEIOGENFaults=FALSE;
			break;
	}
		
	if(HaveHEIOCPUFaults)
		HaveCPUFaults=TRUE;
	else
		HaveCPUFaults=FALSE;
	
	if(HaveHEIOGENFaults)
		HaveGenFaults=TRUE;
	else
		HaveGenFaults=FALSE;


	if(HaveCPUFaults)
	{
		CPUFaultLED(1);	
		WriteAuxRelays(RELAY_1,0);
	}
	else
		CPUFaultLED(0);	
	
	if(HaveGenFaults)
	{
		WriteAuxRelays(RELAY_1,0);
		GenFaultLED(1);	
	}
	else
		GenFaultLED(0);	

}

void GenFault(unsigned int Neuron,unsigned int Fault)
{
	boolean NewFault=FALSE;
	//unsigned short OPNum;
	unsigned short OpFault;
	


		switch(Neuron)
		{
			case DEVICE_ID_1UHE_IO:
				if(Fault<NUM_GEN_FAULTS)
				{
					if(GenFaults[Fault]==0)
						NewFault=TRUE;
					if(GenFaults[Fault]<255)
						GenFaults[Fault]=GenFaults[Fault]+1;
					HaveHEIOGENFaults=TRUE;
				}
				break;
				
		}	
	
	
	if(HaveHEIOGENFaults)
		HaveGenFaults=TRUE;
	else
		HaveGenFaults=FALSE;
	

	if(HaveGenFaults)
	{
		GenFaultLED(1);	
		WriteAuxRelays(RELAY_1,0);

			if(NewFault)
			{
				LaunchFault();
				SendListenerFault(GENERAL_FAULT,Neuron,Fault);
			}

		//SendListenerFault(GENERAL_FAULT,Neuron,Fault);
	}
	else
		GenFaultLED(0);	
	
}

void GenFaultLED(unsigned int State)
{
		WriteFaultSerLeds(LED_GEN_F,State);
//		WriteFaultSerLeds(LED_GEN_F,1);
}

void LaunchFault()
{
	RunningFault=TRUE;
	FaultAlert();
	TroubleTick=10;
	
}

void SendMPICommand(unsigned SwitchNum,unsigned SwitchState)
{

	unsigned char SelHi;
	unsigned char SelLow;
	unsigned j;
	unsigned long UpdateTag;
	
		SelHi = 0x3f;
		SelLow = 0xf6;
	
		
		// set outgoing address to be Subnet node based
		msg_out.dest_addr.snode.domain = 0;
		msg_out.dest_addr.snode.node = 2;
		msg_out.dest_addr.snode.subnet = 1;
		msg_out.dest_addr.snode.type=SUBNET_NODE;
		msg_out.dest_addr.snode.retry =1;
		msg_out.dest_addr.snode.tx_timer=2;
	
		
		
		msg_out.tag = PokeNV;
	    msg_out.service = ACKD;
	    msg_out.code = 0x80 | SelHi;
	
		msg_out.data[0] = SelLow;	//set index

		
		msg_out.data[1] =201;
		msg_out.data[2] =0x00;
		msg_out.data[3] =30;	//special
		msg_out.data[4] =5; //sub command
		msg_out.data[5] =0x55; //tx
		msg_out.data[6] =9;	//byte cnt
		msg_out.data[7] =(char)CurrentUnitNum;
		msg_out.data[8] =(char)SwitchNum;
		msg_out.data[9] =(char)SwitchState;
		for(j=10;j<25;j++)
			msg_out.data[j] =0;
		
	
	    msg_send();
  
  post_events();
  
  
}



void SendListenerFault(unsigned FaultType,unsigned Neuron, unsigned Fault)
{

	unsigned char SelHi;
	unsigned char SelLow;
	unsigned j;
	unsigned long UpdateTag;
	
		SelHi = 0x04;
		SelLow = 0x06;
	
		
		// set outgoing address to be Subnet node based
		msg_out.dest_addr.snode.domain = 0;
		msg_out.dest_addr.snode.node = 4;
		msg_out.dest_addr.snode.subnet = 4;
		msg_out.dest_addr.snode.type=SUBNET_NODE;
		msg_out.dest_addr.snode.retry =1;
		msg_out.dest_addr.snode.tx_timer=6;
	
		
		
		msg_out.tag = PokeNV;
	    msg_out.service = ACKD;
	    msg_out.code = 0x80 | SelHi;
	
		msg_out.data[0] = SelLow;	//set index

		
		msg_out.data[1] =FaultType;
		msg_out.data[2] =Neuron;
		msg_out.data[3] =Fault;	
	
	
	    msg_send();
  
  post_events();
  
  
} 


void FaultAlert()
{
	// say ent to ack, clr to clear
	//also go to fisrt alarm
	PasswordValid=TRUE;
	KeyDeadTimer=NO_INPUT_TIMEOUT;
//	CurrentMode = DIAG_MODE;
//	strcpy(LCDBuff,"Diag    ");
//	LCD_String(0,0);
	CurrentFaultIndex=0;
	CurrentFaultArray=ARRAY_NONE;
	CurrentFaultIndex=GetNextCPUFaultDown();
	CurrentFunc=FAULTS;
	Functions();
	CurrentSelectLocation=SELECT_SUBOPTION;	//position over mute

//	DiagFunctions();
	PlayFX(FX_MESSAGE_2);
}

unsigned int GetNextCPUFaultDown()
{
	unsigned int j;
	
	if(!HaveHEIOCPUFaults && !HaveHEIOGENFaults)
	{
			CurrentFaultIndex=0;
			CurrentFaultArray=ARRAY_NONE;
			return(CurrentFaultIndex);
	}
	

	if(CurrentFaultArray!=ARRAY_NONE)
	{
		CurrentFaultIndex++;	//we are in teh process of looking through all arrays so dont look at the last fault
	}
	else
		CurrentFaultArray=ARRAY_KEYPAD_CPU; //this is our first pass at checking
	
	if(CurrentFaultArray==ARRAY_ENDOFLIST)
	{
		CurrentFaultArray=ARRAY_KEYPAD_CPU;	//we are at teh top of teh list and have been asked to go down
		CurrentFaultIndex=0;
	}
	

	if(CurrentFaultArray==ARRAY_KEYPAD_CPU)
	{
		if(HaveHEIOCPUFaults)
		{
			for(j=CurrentFaultIndex;j<NUM_CPU_FAULTS;j++)
			{
				if(CpuFaults[j]>0)
				{
					CurrentFaultIndex=j;
					return(CurrentFaultIndex);	//found a fault
				}
			}
		}
		CurrentFaultArray=ARRAY_KEYPAD_GEN;	//move to next array
		CurrentFaultIndex=0;
		
	}
	if(CurrentFaultArray==ARRAY_KEYPAD_GEN)
	{
		if(HaveHEIOGENFaults)
		{
			for(j=CurrentFaultIndex;j<NUM_GEN_FAULTS;j++)
			{
				if(GenFaults[j]>0)
				{
					CurrentFaultIndex=j;
					return(CurrentFaultIndex);	//found a fault
				}
			}
			CurrentFaultArray=ARRAY_ENDOFLIST;	//we ran out of entries
			CurrentFaultIndex=0;
		}
		else
		{
			//we got here because no other array had faults
			CurrentFaultArray=ARRAY_ENDOFLIST;	//we ran out of entries
			CurrentFaultIndex=0;
		}
		
	}

	return(CurrentFaultIndex);
}

unsigned int GetNextCPUFaultUp()
{
	signed int j;
	
	if(!HaveHEIOCPUFaults && !HaveHEIOGENFaults)
	{
			CurrentFaultIndex=0;
			CurrentFaultArray=ARRAY_NONE;
			return(CurrentFaultIndex);
	}
	

	if(CurrentFaultArray!=ARRAY_NONE)
	{
		switch(CurrentFaultArray)
		{
			case ARRAY_KEYPAD_GEN:
				if(CurrentFaultIndex==0)
				{
					CurrentFaultArray=ARRAY_KEYPAD_CPU;
					CurrentFaultIndex=NUM_CPU_FAULTS-1;
				}
				else
					CurrentFaultIndex--;
				break;
			case ARRAY_KEYPAD_CPU:
				if(CurrentFaultIndex==0)
				{
					CurrentFaultArray=ARRAY_ENDOFLIST;
					CurrentFaultIndex=0;
				}
				else
					CurrentFaultIndex--;
				break;
			case ARRAY_ENDOFLIST:
					CurrentFaultArray=ARRAY_KEYPAD_GEN;	//we at at teh bottom of teh list and we have been asked to go up
					CurrentFaultIndex=NUM_GEN_FAULTS-1;
				break;
				
		}
	}
	else
		CurrentFaultArray=ARRAY_KEYPAD_GEN; //this is our first pass at checking


	if(CurrentFaultArray==ARRAY_KEYPAD_GEN)
	{
		if(HaveHEIOGENFaults)
		{
			for(j=CurrentFaultIndex;j>=0;j--)
			{
				if(GenFaults[j]>0)
				{
					CurrentFaultIndex=j;
					return(CurrentFaultIndex);	//found a fault
				}
			}
		}
		CurrentFaultArray=ARRAY_KEYPAD_CPU;	//move to next array
		CurrentFaultIndex=NUM_CPU_FAULTS-1;
		
	}

	if(CurrentFaultArray==ARRAY_KEYPAD_CPU)
	{
		if(HaveHEIOCPUFaults)
		{
			for(j=CurrentFaultIndex;j>=0;j--)
			{
				if(CpuFaults[j]>0)
				{
					CurrentFaultIndex=j;
					return(CurrentFaultIndex);	//found a fault
				}
			}
			CurrentFaultArray=ARRAY_ENDOFLIST;	//move to next array
			CurrentFaultIndex=0;
		}
		else
		{
			//we got here because no other array had faults
			CurrentFaultArray=ARRAY_ENDOFLIST;	//we ran out of entries
			CurrentFaultIndex=0;
		}
		
	}

	
	return(CurrentFaultIndex);
}



void LCD_String(unsigned row, unsigned col)
{
	//row = 0 for line 1, 1 for line 2	
	int i;
		
	if(row ==0)
	{
		if(col<MAX_COLUMNS)
		{
			SendCommand(0x80 | col);// set ddram address
			msec_delay(DELAY37USEC); //wait 37 usec
		}				
	}
	if(row==1)
	{
		if(col<MAX_COLUMNS)
		{
			SendCommand(0x80 | (0x40+col));// set ddram address
			msec_delay(DELAY37USEC); //wait 37 usec
		}				
	}
	
	for(i=0;i<MAX_COLUMNS;i++)
	{
		if(LCDBuff[i]==0)
			return;
		else
			SendData(LCDBuff[i]);
	}
	
	
}

void CPUFaultLED(unsigned int State)
{
		WriteFaultSerLeds(LED_CPU_F,State);
}

void SetupI2C()
{
	//setup i2c chips for displays
	SelectBus(BUS_A);
	I2C_OutBuff[0]=0;	//direction reg
	I2C_OutBuff[1]=0;	//make all outputs
	result = io_out(ioI2C, &I2C_OutBuff, DATA_ADDR, 2);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,LCD_DATA_I2C);


	I2C_OutBuff[0]=0;	//direction reg
	I2C_OutBuff[1]=0;	//make all outputs
	result = io_out(ioI2C, &I2C_OutBuff, CONTROL_ADDR, 2);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,LCD_CONTROL_I2C);

#ifndef COMBO_UNIT

	//for mcp23017 for Aux relays leds
	SelectBus(BUS_A);
	I2C_OutBuff[0]=0x14;	//olat reg
	I2C_OutBuff[1]=0xff;	//make all high for A
	I2C_OutBuff[2]=0xff;	//make all high For B
	result = io_out(ioI2C, &I2C_OutBuff, DIG_IN_LED_ADDR, 3);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,DIG_IN_LED_I2C);


	I2C_OutBuff[0]=0x00;	//Dir reg
	I2C_OutBuff[1]=0x00;	//make all output for A
	I2C_OutBuff[2]=0x00;	//make all output For B
	result = io_out(ioI2C, &I2C_OutBuff, DIG_IN_LED_ADDR, 3);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,DIG_IN_LED_I2C);
#endif

	//for mcp23017 for Aux relays leds 2
	SelectBus(CFG_BUS);
	I2C_OutBuff[0]=0x14;	//olat reg
	I2C_OutBuff[1]=0xff;	//make all high for A
	I2C_OutBuff[2]=0xff;	//make all high For B
	result = io_out(ioI2C, &I2C_OutBuff, CFG_FLED_LED_ADDR, 3);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,CFG_FLED_I2C);


	I2C_OutBuff[0]=0x00;	//Dir reg
	I2C_OutBuff[1]=0x00;	//make all output for A
	I2C_OutBuff[2]=0xff;	//make all inputs For B
	result = io_out(ioI2C, &I2C_OutBuff, CFG_FLED_LED_ADDR, 3);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,CFG_FLED_I2C);





#ifndef COMBO_UNIT

// for mcp23017 for aux relays on back panel
	SelectBus(BUS_A);
	I2C_OutBuff[0]=0x14;	//olat reg
	I2C_OutBuff[1]=0xff;	//make all high for A
	result = io_out(ioI2C, &I2C_OutBuff, AUX_RELAY_ADDR, 2);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,AUX_RELAY_I2C);

	I2C_OutBuff[0]=0x00;	//Dir reg
	I2C_OutBuff[1]=0x00;	//make all output for A
	I2C_OutBuff[2]=0xff;	//make inputs For B
	result = io_out(ioI2C, &I2C_OutBuff, AUX_RELAY_ADDR, 3);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,AUX_RELAY_I2C);
#endif

#ifdef COMBO_UNIT
	(void)SelectBus(BUS_C);
	I2C_OutBuff[0]=0x00;	//Dir reg
	I2C_OutBuff[1]=0xff;	//make all in for A
	I2C_OutBuff[2]=0xff;	//make all in For B
	I2C_OutBuff[3]=0x00;	//polarity
	I2C_OutBuff[4]=0x00;	//polarity
	I2C_OutBuff[5]=0x80;	//GPIENA
	I2C_OutBuff[6]=0x00;	//GPIENB
	I2C_OutBuff[7]=0x00;	//DefValA
	I2C_OutBuff[8]=0x00;	//DefValB
	I2C_OutBuff[9]=0x80;	//IntConA
	I2C_OutBuff[10]=0x00;	//IntConB


	result = io_out(ioI2C, &I2C_OutBuff, LCD_IO_ADDR_1, 11);
	result = io_out(ioI2C, &I2C_OutBuff, LCD_IO_ADDR_2, 11);
#endif
	

}

#ifdef COMBO_UNIT
void PaintLCD_Info()
{

	Readlcd=LCDScan;



}



when(timer_expires(Readlcd))
{
	if(CurrentFunc==LCD_INFO)
	{
		Readlcd=LCDScan;	//lets do this again

		ReadLCDBits(OP_1);	
		ReadLCDBits(OP_2);	
//		ReadLCDBits(OP_3);	
//		ReadLCDBits(OP_4);			
	}

}

#endif

void SetupI2C2()
{
		//for mcp23017 for FX Sounds
	SelectBus(BUS_A);
	I2C_OutBuff[0]=0x12;	//GPIO reg
#ifdef COMBO_UNIT
	I2C_OutBuff[1]=0xfd;	//mute on
	
#else
	I2C_OutBuff[1]=CurrentFX_A;	//make all high for A
#endif

	I2C_OutBuff[2]=0xff;	//make all high For B
	result = io_out(ioI2C, &I2C_OutBuff, FX_AUDIO_ADDR, 3);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,FX_AUDIO_I2C);


	I2C_OutBuff[0]=0x00;	//Dir reg
	//OutBuff[1]=0xE0;	//make mix for A

	I2C_OutBuff[1]=0x01;	//make mix for A, enabled mute of amp

	I2C_OutBuff[2]=0x00;	//make all out For B
	result = io_out(ioI2C, &I2C_OutBuff, FX_AUDIO_ADDR, 3);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,FX_AUDIO_I2C);

}


when(timer_expires(TroubleTick))
{
	FaultAlert();
}


when(timer_expires(KeyDeadTimer))
{
/*	if(CurrentPanelPriority==FACP_PRIORITY)
	{
		if(CurrentEMMode!=FACP_EMERGENCY)
		{
			WaitMessage();
			Password_pointer=0;
			KeyPressed=KEY_NONE;
		}
			
	}
	else
	{
		WaitMessage();
		Password_pointer=0;
		KeyPressed=KEY_NONE;
	}*/


}


int ConvNumToBuf(long Num, int places,int decpos)
{
	// plaaces is the minimum number of characters to return for each digit, not inc the dec point
	// leading zeros will be suppressed 
	//decpos = where the dec is, ex  xx.x is 1
	
	long Hundreds;
	long Tens;
	long Ones;
	boolean Hide=TRUE;	//hide leading zeros
	int CurrentBuffIndex=0;
	
	
	Hundreds = Num/100;
	Tens = (Num-(Hundreds*100))/10;
	Ones = Num - (Hundreds*100) - (Tens * 10);
	
	if(Hundreds>0)
		Hide=FALSE;	// not a leading zero
	
	if(Hide)
	{
		if(places>2)
		{
			//only add this character if we wanted a min of 3 places. otherwise throw it away
			LCDBuff[CurrentBuffIndex]=0x20;	//make a space to hide leading 0
			CurrentBuffIndex++;
		}
	}
	else
	{
			LCDBuff[CurrentBuffIndex]=(unsigned short)(0x30|Hundreds);
			CurrentBuffIndex++;
			Hide=FALSE;	//we are past leading zeros
			if(decpos==2)
			{
				LCDBuff[CurrentBuffIndex]=0x2e;
				CurrentBuffIndex++;
				
			}
			
	}

	if(Tens>0)
		Hide=FALSE;	// not a leading zero
	
	if(Hide)
	{
		if(places>1)
		{
			//only add this character if we wanted a min of 2 places. otherwise throw it away
			LCDBuff[CurrentBuffIndex]=0x20;	//make a space to hide leading 0
			CurrentBuffIndex++;
		}
	}
	else
	{
		LCDBuff[CurrentBuffIndex]=(unsigned short)(0x30|Tens);
		CurrentBuffIndex++;
		Hide=FALSE;	//we are past leading zeros
			if(decpos==1)
			{
				LCDBuff[CurrentBuffIndex]=0x2e;
				CurrentBuffIndex++;
				
			}
	}

		LCDBuff[CurrentBuffIndex]=(unsigned short)(0x30|Ones);
		CurrentBuffIndex++;
		LCDBuff[CurrentBuffIndex]=0;	//null
		return(CurrentBuffIndex);	//return the number of charactyers generated
	
}
 
 
 
void SendDebug(unsigned DebugByte,unsigned DataByte)
{

	unsigned char SelHi;
	unsigned char SelLow;
	unsigned j;
	unsigned long UpdateTag;
	
	if(DEBUG_MESSAGES)
	{
		SelHi = 0x06;
		SelLow = 0x03;
	
		
		// set outgoing address to be Subnet node based
		msg_out.dest_addr.snode.domain = 0;
		msg_out.dest_addr.snode.node = 4;
		msg_out.dest_addr.snode.subnet = 5;
		msg_out.dest_addr.snode.type=SUBNET_NODE;
	
		
		
		msg_out.tag = PokeNV;
	    msg_out.service = UNACKD;
	    msg_out.code = 0x80 | SelHi;
	
		msg_out.data[0] = SelLow;	//set index
	
		msg_out.data[1] =(char)DebugByte;
		msg_out.data[2] =(char)DataByte;
		msg_out.data[3] =0x00;
		msg_out.data[4] =0x00;
		msg_out.data[5] =0x00;
		msg_out.data[6] =0x07;
		
	
	    msg_send();
  }
  
  
} 


 
void UpdateLEDStates(unsigned Led, boolean State)
{
	boolean result;
	


		switch(Led)
		{
#ifdef COMBO_UNIT
			case LED_POWER:
					if(State)
						LedsA=LedsA&(~LED_POWER_MASK);	//clear the bit to turn led on
					else
						LedsA=LedsA|LED_POWER_MASK; //set the bit to turn led off
					break;
#endif					
			case LED_FTR_1A:
					if(State)
						LedsA=LedsA&(~LED_1A_MASK);	//clear the bit to turn led on
					else
						LedsA=LedsA|LED_1A_MASK; //set the bit to turn led off
					break;					
			case LED_FTR_2A:
					if(State)
						LedsA=LedsA&(~LED_2A_MASK);	//clear the bit to turn led on
					else
						LedsA=LedsA|LED_2A_MASK; //set the bit to turn led off
					break;					
			case LED_FTR_3A:
					if(State)
						LedsA=LedsA&(~LED_3A_MASK);	//clear the bit to turn led on
					else
						LedsA=LedsA|LED_3A_MASK; //set the bit to turn led off
					break;					
			case LED_FTR_4A:
					if(State)
						LedsA=LedsA&(~LED_4A_MASK);	//clear the bit to turn led on
					else
						LedsA=LedsA|LED_4A_MASK; //set the bit to turn led off
					break;					
			case LED_FTR_1B:
					if(State)
						LedsA=LedsA&(~LED_1B_MASK);	//clear the bit to turn led on
					else
						LedsA=LedsA|LED_1B_MASK; //set the bit to turn led off
					break;					
			case LED_FTR_2B:
					if(State)
						LedsA=LedsA&(~LED_2B_MASK);	//clear the bit to turn led on
					else
						LedsA=LedsA|LED_2B_MASK; //set the bit to turn led off
					break;					
			case LED_FTR_3B:
					if(State)
						LedsA=LedsA&(~LED_3B_MASK);	//clear the bit to turn led on
					else
						LedsA=LedsA|LED_3B_MASK; //set the bit to turn led off
					break;					
			case LED_FTR_4B:
					if(State)
						LedsA=LedsA&(~LED_4B_MASK);	//clear the bit to turn led on
					else
						LedsA=LedsA|LED_4B_MASK; //set the bit to turn led off
					break;					
			case LED_FTR_1C:
					if(State)
						LedsB=LedsB&(~LED_1C_MASK);	//clear the bit to turn led on
					else
						LedsB=LedsB|LED_1C_MASK; //set the bit to turn led off
					break;					
			case LED_FTR_2C:
					if(State)
						LedsB=LedsB&(~LED_2C_MASK);	//clear the bit to turn led on
					else
						LedsB=LedsB|LED_2C_MASK; //set the bit to turn led off
					break;					
			case LED_FTR_3C:
					if(State)
						LedsB=LedsB&(~LED_3C_MASK);	//clear the bit to turn led on
					else
						LedsB=LedsB|LED_3C_MASK; //set the bit to turn led off
					break;					
			case LED_FTR_4C:
					if(State)
						LedsB=LedsB&(~LED_4C_MASK);	//clear the bit to turn led on
					else
						LedsB=LedsB|LED_4C_MASK; //set the bit to turn led off
					break;					
			case LED_RLY1:
#ifdef COMBO_UNIT
					if(State)
						LedsA=LedsA&(~LED_RLY1_MASK);	//clear the bit to turn led on
					else
						LedsA=LedsA|LED_RLY1_MASK; //set the bit to turn led off
					break;					
#else
					if(State)
						LedsB=LedsB&(~LED_RLY1_MASK);	//clear the bit to turn led on
					else
						LedsB=LedsB|LED_RLY1_MASK; //set the bit to turn led off
					break;					
#endif
			case LED_RLY2:
					if(State)
						LedsB=LedsB&(~LED_RLY2_MASK);	//clear the bit to turn led on
					else
						LedsB=LedsB|LED_RLY2_MASK; //set the bit to turn led off
					break;					
			case LED_RLY3:
					if(State)
						LedsB=LedsB&(~LED_RLY3_MASK);	//clear the bit to turn led on
					else
						LedsB=LedsB|LED_RLY3_MASK; //set the bit to turn led off
					break;					
			case LED_AUX:
#ifdef COMBO_UNIT
					if(State)
						LedsA=LedsA&(~LED_AUX_MASK);	//clear the bit to turn led on
					else
						LedsA=LedsA|LED_AUX_MASK; //set the bit to turn led off
					break;					
#else
					if(State)
						LedsB=LedsB&(~LED_AUX_MASK);	//clear the bit to turn led on
					else
						LedsB=LedsB|LED_AUX_MASK; //set the bit to turn led off
					break;					
#endif
			case LED_ALL:
					if(State)
					{
						LedsA=LedsA&0x00;	//clear all the bits to turn led on
						LedsB=LedsA&0x00;	//clear all the bits to turn led on
					}
					else
					{
						LedsA=LedsA|0xff; //set all the bits to turn all leds off
						LedsB=LedsA|0xff; //set all the bits to turn all leds off
					}
					break;		
			default:
				return;	//should not happen			
		}
	
	
}

void PaintLeds()
{
#ifdef COMBO_UNIT
	SelectBus(BUS_B);
		I2C_OutBuff[0]=0x14;	//olat reg
		if(FaultToggle)
		{
			I2C_OutBuff[1]=LedsA &(~FaultBitsA);	
			//I2C_OutBuff[2]=LedsB &(~FaultBitsB);	
		}
		else
		{
			I2C_OutBuff[1]=LedsA | FaultBitsA;	
			//I2C_OutBuff[2]=LedsB | FaultBitsB;	
			
		}
		
		result = io_out(ioI2C, &I2C_OutBuff, DIG_IN_LED_ADDR, 3);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,DIG_IN_LED_I2C);
		return;
#else
	SelectBus(BUS_A);
		I2C_OutBuff[0]=0x14;	//olat reg
		if(FaultToggle)
		{
			I2C_OutBuff[1]=LedsA &(~FaultBitsA);	
			I2C_OutBuff[2]=LedsB &(~FaultBitsB);	
		}
		else
		{
			I2C_OutBuff[1]=LedsA | FaultBitsA;	
			I2C_OutBuff[2]=LedsB | FaultBitsB;	
			
		}
		
		result = io_out(ioI2C, &I2C_OutBuff, DIG_IN_LED_ADDR, 3);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,DIG_IN_LED_I2C);
		return;
#endif	
}


boolean WriteMCP45HV(short Address,short Bus, unsigned short Pot)
{
	//support for MAX14662

	SelectBus(Bus);
		
	OutBuff[0]=0x00;	//write to address 0 whihc is the only pot we are using
	OutBuff[1]=Pot;

	result = io_out(ioI2C, &OutBuff, Address, 2);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,POT_I2C);

	return(result);

}

void WriteFaultSerLeds(unsigned Led, boolean State)
{
	boolean result;
	
#ifdef COMBO_UNIT
	switch(Led)
	{
		case LED_CPU_F:
				if(State)
					LedsA=LedsA&(~LED_CPUF_MASK);	//clear the bit to turn led on
				else
					LedsA=LedsA|LED_CPUF_MASK; //set the bit to turn led off
				break;					
		case LED_GEN_F:
				if(State)
					LedsA=LedsA&(~LED_GENF_MASK);	//clear the bit to turn led on
				else
					LedsA=LedsA|LED_GENF_MASK; //set the bit to turn led off
				break;					
		case LED_ALL:
				if(State)
					LedsA=LedsA&0xcf;	//clear all the fault bits to turn led on
				else
					LedsA=LedsA|0x30; //set all the fault bitd to turn all leds off
				break;		
		default:
			return;	//should not happen			
	}

	PaintLeds();

#else
	SelectBus(CFG_BUS);

	switch(Led)
	{
		case LED_CPU_F:
				if(State)
					FaultSerLEDs=FaultSerLEDs&(~LED_CPUF_MASK);	//clear the bit to turn led on
				else
					FaultSerLEDs=FaultSerLEDs|LED_CPUF_MASK; //set the bit to turn led off
				break;					
		case LED_GEN_F:
				if(State)
					FaultSerLEDs=FaultSerLEDs&(~LED_GENF_MASK);	//clear the bit to turn led on
				else
					FaultSerLEDs=FaultSerLEDs|LED_GENF_MASK; //set the bit to turn led off
				break;					
		case LED_SERIAL:
				if(State)
					FaultSerLEDs=FaultSerLEDs&(~LED_SER_MASK);	//clear the bit to turn led on
				else
					FaultSerLEDs=FaultSerLEDs|LED_SER_MASK; //set the bit to turn led off
				break;					
		case LED_ALL:
				if(State)
					FaultSerLEDs=FaultSerLEDs&0x1f;	//clear all the bits to turn led on
				else
					FaultSerLEDs=FaultSerLEDs|0xe0; //set all the bitd to turn all leds off
				break;		
		default:
			return;	//should not happen			
	}
		I2C_OutBuff[0]=0x14;	//olat reg
		I2C_OutBuff[1]=FaultSerLEDs;	
		result = io_out(ioI2C, &I2C_OutBuff, CFG_FLED_LED_ADDR, 2);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,CFG_FLED_I2C);
		return;
#endif
	
}

void WriteAuxRelays(unsigned Relay, boolean State)
{
	boolean result;
	
#ifdef COMBO_UNIT
	
	switch(Relay)
	{
		case RELAY_1:	
			if(State)
				RelayStates=RelayStates&(~RELAY1_MASK);	//clear the bit to turn led on
			else
				RelayStates=RelayStates|RELAY1_MASK; //set the bit to turn led off
			UpdateLEDStates(LED_RLY1,State);
			break;					
		case RELAY_ALL:	
			if(State)
				RelayStates=0xf7;	//clear the bit to turn led on
			else
				RelayStates=0xff; //set the bit to turn led off
			UpdateLEDStates(LED_RLY1,State);
			break;					
			
	}
#else
	switch(Relay)
	{
		case RELAY_1:	
			if(State)
				RelayStates=RelayStates&(~RELAY1_MASK);	//clear the bit to turn led on
			else
				RelayStates=RelayStates|RELAY1_MASK; //set the bit to turn led off
			UpdateLEDStates(LED_RLY1,State);
			break;					
		case RELAY_2:	
			if(State)
				RelayStates=RelayStates&(~RELAY2_MASK);	//clear the bit to turn led on
			else
				RelayStates=RelayStates|RELAY2_MASK; //set the bit to turn led off
			UpdateLEDStates(LED_RLY2,State);
			break;					
		case RELAY_3:	
			if(State)
				RelayStates=RelayStates&(~RELAY3_MASK);	//clear the bit to turn led on
			else
				RelayStates=RelayStates|RELAY3_MASK; //set the bit to turn led off
			UpdateLEDStates(LED_RLY3,State);
			break;					
		case RELAY_ALL:	
			if(State)
				RelayStates=0xf8;	//clear the bit to turn led on
			else
				RelayStates=0xff; //set the bit to turn led off
			UpdateLEDStates(LED_RLY1,State);
			UpdateLEDStates(LED_RLY2,State);
			UpdateLEDStates(LED_RLY3,State);
			break;					
			
	}
#endif
	
	SelectBus(BUS_A);

		I2C_OutBuff[0]=0x14;	//olat reg

#ifdef COMBO_UNIT
		if(CurrentFXMute)
		{
			I2C_OutBuff[1]=RelayStates & 0xf8;	//clear lower bits that are related to fxaudio
			I2C_OutBuff[1]=I2C_OutBuff[1] | 0x00;	//mute on
		}
		else
		{
			I2C_OutBuff[1]=RelayStates & 0xf8;	//clear lower bits that are related to fxaudio
			I2C_OutBuff[1]=I2C_OutBuff[1] | 0x02;	//mute off
		}
#else
		I2C_OutBuff[1]=RelayStates;	

#endif


		result = io_out(ioI2C, &I2C_OutBuff, AUX_RELAY_ADDR, 2);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,AUX_RELAY_I2C);
		return;
	
}
 

void Functions()
{
	switch(CurrentFunc)
	{
		case MASK_VOL:
			strcpy(LCDBuff,"Mask Vol  ");
			break;
		case MASK_CONTOUR_F:
			strcpy(LCDBuff,"Mask Cntr ");
			break;
		case MASK_MUTE:
			strcpy(LCDBuff,"Mask Mute ");
			break;
		case MUSIC_VOL:
			strcpy(LCDBuff,"Music Vol ");
			break;
		case MUSIC_MUTE:
			strcpy(LCDBuff,"Music Mute");
			break;
		case PAGE_VOL:
			strcpy(LCDBuff,"Page Vol  ");
			break;
		case PAGE_MUTE:
			strcpy(LCDBuff,"Page Mute ");
			break;
		case IP_ADD:
			strcpy(LCDBuff,"IP Info   ");
			break;
		case VERSIONS:
			strcpy(LCDBuff,"Versions  ");
			break;
#ifdef COMBO_UNIT
		case RCA_VOLUME:
			strcpy(LCDBuff,"RCA Volume      ");
			break;
		case LCD_INFO:
			//(void)strcpy(LCDBuff,"OP #1           ");
			(void)strcpy(LCDBuff,"OP 1 Status>    ");
			LCD_String(0,0);
			(void)strcpy(LCDBuff,"OP 2 Status>    ");
			LCD_String(1,0);
			PaintFunctions();
			return;
			//(void)strcpy(LCDBuff," U1  U2  U3  U4 ");
			//(void)strcpy(LCDBuff," OP# 1   OP #2  ");
			break;	
#endif
		case VOLTAGES:
			strcpy(LCDBuff,"Voltages  ");
			break;
		case FAULTS:
			strcpy(LCDBuff,"Faults    ");
			break;
		case IO_SETUP:
			strcpy(LCDBuff,"IO Setup  ");
			break;
		case TEST_TONE:
			strcpy(LCDBuff,"Test Tone ");
			break;
		case UNIT_NUMBER:
			strcpy(LCDBuff,"Unit Num  ");
			break;
	}
	
	LCD_String(0,0);
	PaintFunctions();

}

void SendMaskingMute(unsigned OP,unsigned Ch, unsigned Mute)
{
	
	int i;
	
		if(CurrentOPOption==OPTION_ZONE)
		{
			if(OP==0)
				return;	//a real zone hasn't been set so dont send anything
			else
			{
			TempCommand.OPAddress = CurrentZoneNum;
			TempCommand.OPGroupSelect = MASK_GROUP;
			TempCommand.OPSubCommand= 20;
			}
		}
		else if(CurrentOPOption==OPTION_OP_ALL)
		{
			TempCommand.OPAddress = 0;
			TempCommand.OPGroupSelect = GLOBAL_SELECT;
			TempCommand.OPSubCommand= 20;
		}
		else
		{
			TempCommand.OPAddress = CurrentOPNum;
			if(Ch==0)
			{

				TempCommand.OPGroupSelect = INDIVIDUAL;
				TempCommand.OPSubCommand= 20;	//all channels
			}
			else
			{
				TempCommand.OPGroupSelect = INDIVIDUAL;
				TempCommand.OPSubCommand= 15+Ch;	//one channel
			}
		}
		TempCommand.OPCommandCat= MASK_ATTENUATION;
		
		TempCommand.OPTransactionID= NextTx();
		TempCommand.ByteCount=7;
		TempCommand.OPData[0]=Mute;
		for(i=1;i<25;i++)
			TempCommand.OPData[i]=0;
			
		SendOPMess();
		//PaintMasking();
//		ClearDisplay();
//		strcpy(LCDBuff,"Masking Set");
//		LCD_String(0,0);
//		strcpy(LCDBuff,"Channel 1 64db");
//		LCD_String(1,0);

}

void SendPageMute(unsigned OP,unsigned Ch, unsigned Mute)
{
	
	int i;
	
		if(CurrentOPOption==OPTION_ZONE)
		{
			if(OP==0)
				return;	//a real zone hasn't been set so dont send anything
			else
			{
			TempCommand.OPAddress = CurrentZoneNum;
			TempCommand.OPGroupSelect = PAGE_GROUP;
			TempCommand.OPSubCommand= 20;
			}
		}
		else if(CurrentOPOption==OPTION_OP_ALL)
		{
			TempCommand.OPAddress = 0;
			TempCommand.OPGroupSelect = GLOBAL_SELECT;
			TempCommand.OPSubCommand= 20;
		}
		else
		{
			TempCommand.OPAddress = CurrentOPNum;
			if(Ch==0)
			{

				TempCommand.OPGroupSelect = INDIVIDUAL;
				TempCommand.OPSubCommand= 20;	//all channels
			}
			else
			{
				TempCommand.OPGroupSelect = INDIVIDUAL;
				TempCommand.OPSubCommand= 15+Ch;	//one channel
			}
		}
		TempCommand.OPCommandCat= PAGE_ATTENUATION;
		
		TempCommand.OPTransactionID= NextTx();
		TempCommand.ByteCount=7;
		TempCommand.OPData[0]=Mute;
		for(i=1;i<25;i++)
			TempCommand.OPData[i]=0;
			
		SendOPMess();
		//PaintMasking();
//		ClearDisplay();
//		strcpy(LCDBuff,"Masking Set");
//		LCD_String(0,0);
//		strcpy(LCDBuff,"Channel 1 64db");
//		LCD_String(1,0);

}

void SendMusicMute(unsigned OP,unsigned Ch, unsigned Mute)
{
	
	int i;
	
		if(CurrentOPOption==OPTION_ZONE)
		{
			if(OP==0)
				return;	//a real zone hasn't been set so dont send anything
			else
			{
			TempCommand.OPAddress = CurrentZoneNum;
			TempCommand.OPGroupSelect = MUSIC_GROUP;
			TempCommand.OPSubCommand= 20;
			}
		}
		else if(CurrentOPOption==OPTION_OP_ALL)
		{
			TempCommand.OPAddress = 0;
			TempCommand.OPGroupSelect = GLOBAL_SELECT;
			TempCommand.OPSubCommand= 20;
		}
		else
		{
			TempCommand.OPAddress = CurrentOPNum;
			if(Ch==0)
			{

				TempCommand.OPGroupSelect = INDIVIDUAL;
				TempCommand.OPSubCommand= 20;	//all channels
			}
			else
			{
				TempCommand.OPGroupSelect = INDIVIDUAL;
				TempCommand.OPSubCommand= 15+Ch;	//one channel
			}
		}
		TempCommand.OPCommandCat= MUSIC_ATTENUATION;
		
		TempCommand.OPTransactionID= NextTx();
		TempCommand.ByteCount=7;
		TempCommand.OPData[0]=Mute;
		for(i=1;i<25;i++)
			TempCommand.OPData[i]=0;
			
		SendOPMess();
		//PaintMasking();
//		ClearDisplay();
//		strcpy(LCDBuff,"Masking Set");
//		LCD_String(0,0);
//		strcpy(LCDBuff,"Channel 1 64db");
//		LCD_String(1,0);

}

void SendTestTone(unsigned OP,unsigned Ch, unsigned Tone)
{
	
	int i;
	
		if(CurrentOPOption==OPTION_ZONE)
		{
			return;
		}
		else
		{
			TempCommand.OPAddress = CurrentOPNum;
				TempCommand.OPGroupSelect = INDIVIDUAL;
				TempCommand.OPSubCommand= 1;	//only subcommand 1 works- all channels play tone
		
		}
		TempCommand.OPCommandCat= DIAGNOSTICS;
		
		TempCommand.OPTransactionID= NextTx();
		TempCommand.ByteCount=9;
		TempCommand.OPData[0]=232;
		TempCommand.OPData[1]=3;
		if(Tone==1)
			TempCommand.OPData[2]=49;
		else
			TempCommand.OPData[2]=0;
			
		for(i=3;i<25;i++)
			TempCommand.OPData[i]=0;
			
		SendOPMess();
		//PaintMasking();
//		ClearDisplay();
//		strcpy(LCDBuff,"Masking Set");
//		LCD_String(0,0);
//		strcpy(LCDBuff,"Channel 1 64db");
//		LCD_String(1,0);

}


void SendMaskingVol(unsigned OP,unsigned Ch, unsigned Vol)
{
	
	int i;
	
//		TempCommand.OPAddress = OP;
		if(CurrentOPOption==OPTION_ZONE)
		{
			if(OP==0)
				return;	//a real zone hasn't been set so dont send anything
			else
			{
			TempCommand.OPAddress = CurrentZoneNum;
			TempCommand.OPGroupSelect = MASK_GROUP;
			TempCommand.OPSubCommand= 5;
			}
		}
		else if(CurrentOPOption==OPTION_OP_ALL)
		{
			TempCommand.OPAddress = 0;
			TempCommand.OPGroupSelect = GLOBAL_SELECT;
			TempCommand.OPSubCommand= 5;
		}
		else
		{
			TempCommand.OPAddress = CurrentOPNum;
			if(Ch==0)
			{

				TempCommand.OPGroupSelect = INDIVIDUAL;
				TempCommand.OPSubCommand= 5;	//all channels
			}
			else
			{
				TempCommand.OPGroupSelect = INDIVIDUAL;
				TempCommand.OPSubCommand= Ch;	//one channel
			}
		}
		TempCommand.OPCommandCat= MASK_ATTENUATION;
		
		TempCommand.OPTransactionID= NextTx();
		TempCommand.ByteCount=7;
		TempCommand.OPData[0]=Vol;
		for(i=1;i<25;i++)
			TempCommand.OPData[i]=0;
			
		SendOPMess();
		//PaintMasking();
//		ClearDisplay();
//		strcpy(LCDBuff,"Masking Set");
//		LCD_String(0,0);
//		strcpy(LCDBuff,"Channel 1 64db");
//		LCD_String(1,0);

}

void SendMaskingCont(unsigned OP,unsigned Ch, unsigned Cont)
{
		int i;
	
		if(CurrentOPOption==OPTION_ZONE)
		{
			if(OP==0)
				return;	//a real zone hasn't been set so dont send anything
			else
			{
			TempCommand.OPAddress = CurrentZoneNum;
			TempCommand.OPGroupSelect = MASK_GROUP;
			TempCommand.OPSubCommand= 5;
			}
		}
		else if(CurrentOPOption==OPTION_OP_ALL)
		{
			TempCommand.OPAddress = 0;
			TempCommand.OPGroupSelect = GLOBAL_SELECT;
			TempCommand.OPSubCommand= 5;
		}
		else
		{
			TempCommand.OPAddress = OP;
			if(Ch==0)
			{

				TempCommand.OPGroupSelect = INDIVIDUAL;
				TempCommand.OPSubCommand= 5;	//all channels
			}
			else
			{
				TempCommand.OPGroupSelect = INDIVIDUAL;
				TempCommand.OPSubCommand= Ch;	//one channel
			}
		}
		TempCommand.OPCommandCat= MASK_CONTOUR;
		TempCommand.OPTransactionID= NextTx();
		TempCommand.ByteCount=7;
		TempCommand.OPData[0]=Cont;
		for(i=1;i<25;i++)
			TempCommand.OPData[i]=0;
			
		SendOPMess();

}

void SendMusicVol(unsigned OP,unsigned Ch, unsigned Vol)
{
	
	int i;
	
		if(CurrentOPOption==OPTION_ZONE)
		{
			if(OP==0)
				return;	//a real zone hasn't been set so dont send anything
			else
			{
			TempCommand.OPAddress = CurrentZoneNum;
			TempCommand.OPGroupSelect = MUSIC_GROUP;
			TempCommand.OPSubCommand= 5;
			}
		}
		else if(CurrentOPOption==OPTION_OP_ALL)
		{
			TempCommand.OPAddress = 0;
			TempCommand.OPGroupSelect = GLOBAL_SELECT;
			TempCommand.OPSubCommand= 5;
		}
		else
		{
			TempCommand.OPAddress = OP;
			if(Ch==0)
			{

				TempCommand.OPGroupSelect = INDIVIDUAL;
				TempCommand.OPSubCommand= 5;	//all channels
			}
			else
			{
				TempCommand.OPGroupSelect = INDIVIDUAL;
				TempCommand.OPSubCommand= Ch;	//one channel
			}
		}
		TempCommand.OPCommandCat= MUSIC_ATTENUATION;
		
		TempCommand.OPTransactionID= NextTx();
		TempCommand.ByteCount=7;
		TempCommand.OPData[0]=Vol;
		for(i=1;i<25;i++)
			TempCommand.OPData[i]=0;
			
		SendOPMess();
}

void SendPageVol(unsigned OP,unsigned Ch, unsigned Vol)
{
	
	int i;
	
		if(CurrentOPOption==OPTION_ZONE)
		{
			if(OP==0)
				return;	//a real zone hasn't been set so dont send anything
			else
			{
			TempCommand.OPAddress = CurrentZoneNum;
			TempCommand.OPGroupSelect = PAGE_GROUP;
			TempCommand.OPSubCommand= 5;
			}
		}
		else if(CurrentOPOption==OPTION_OP_ALL)
		{
			TempCommand.OPAddress = 0;
			TempCommand.OPGroupSelect = GLOBAL_SELECT;
			TempCommand.OPSubCommand= 5;
		}
		else
		{
			TempCommand.OPAddress = OP;
			if(Ch==0)
			{

				TempCommand.OPGroupSelect = INDIVIDUAL;
				TempCommand.OPSubCommand= 5;	//all channels
			}
			else
			{
				TempCommand.OPGroupSelect = INDIVIDUAL;
				TempCommand.OPSubCommand= Ch;	//one channel
			}
		}
		TempCommand.OPCommandCat= PAGE_ATTENUATION;
		
		TempCommand.OPTransactionID= NextTx();
		TempCommand.ByteCount=7;
		TempCommand.OPData[0]=Vol;
		for(i=1;i<25;i++)
			TempCommand.OPData[i]=0;
			
		SendOPMess();
}


void ItemUp()
{
	switch(CurrentFunc)
	{
		case MASK_VOL:
			switch(CurrentSelectLocation)
			{
				case SELECT_VALUE:
					OPInfo[CurrentOPChNum].MaskVol++;
					if(OPInfo[CurrentOPChNum].MaskVol>97)
						OPInfo[CurrentOPChNum].MaskVol=97;
					SendMaskingVol(CurrentOPNum,CurrentOPChNum,OPInfo[CurrentOPChNum].MaskVol);	//routine will determnine if zoning is needed
					break;
				case SELECT_OPTION:
					IncreaseZoneOP();
					break;
				case SELECT_SUBOPTION:
					IncreaseChannel();
					break;
			}
			break;
		case MASK_CONTOUR_F:
			switch(CurrentSelectLocation)
			{
				case SELECT_VALUE:
					OPInfo[CurrentOPChNum].MaskContour++;
					if(OPInfo[CurrentOPChNum].MaskContour>29)
						OPInfo[CurrentOPChNum].MaskContour=29;
					SendMaskingCont(CurrentOPNum,CurrentOPChNum,OPInfo[CurrentOPChNum].MaskContour);	//routine will determnine if zoning is needed
					break;
				case SELECT_OPTION:
					IncreaseZoneOP();
					break;
				case SELECT_SUBOPTION:
					IncreaseChannel();
					break;
			}
			break;
		case MASK_MUTE:
			switch(CurrentSelectLocation)
			{
				case SELECT_VALUE:
					OPInfo[CurrentOPChNum].MaskMute = !OPInfo[CurrentOPChNum].MaskMute;
					SendMaskingMute(CurrentOPNum,CurrentOPChNum,OPInfo[CurrentOPChNum].MaskMute);	//routine will determnine if zoning is needed
					//SendTestTone(CurrentOPNum,CurrentOPChNum,1);	//routine will determnine if zoning is needed
					break;
				case SELECT_OPTION:
					IncreaseZoneOP();
					break;
				case SELECT_SUBOPTION:
					IncreaseChannel();
					break;
			}
			break;
		case MUSIC_VOL:
			switch(CurrentSelectLocation)
			{
				case SELECT_VALUE:
					OPInfo[CurrentOPChNum].MusicVol++;
					if(OPInfo[CurrentOPChNum].MusicVol>49)
						OPInfo[CurrentOPChNum].MusicVol=49;
					SendMusicVol(CurrentOPNum,CurrentOPChNum,OPInfo[CurrentOPChNum].MusicVol);	//routine will determnine if zoning is needed
					break;
				case SELECT_OPTION:
					IncreaseZoneOP();
					break;
				case SELECT_SUBOPTION:
					IncreaseChannel();
					break;
			}
			break;
		case MUSIC_MUTE:
			switch(CurrentSelectLocation)
			{
				case SELECT_VALUE:
					OPInfo[CurrentOPChNum].MusicMute = !OPInfo[CurrentOPChNum].MusicMute;
					SendMusicMute(CurrentOPNum,CurrentOPChNum,OPInfo[CurrentOPChNum].MusicMute);	//routine will determnine if zoning is needed
					break;
				case SELECT_OPTION:
					IncreaseZoneOP();
					break;
				case SELECT_SUBOPTION:
					IncreaseChannel();
					break;
			}
			break;
		case PAGE_VOL:
			switch(CurrentSelectLocation)
			{
				case SELECT_VALUE:
					OPInfo[CurrentOPChNum].PageVol++;
					if(OPInfo[CurrentOPChNum].PageVol>49)
						OPInfo[CurrentOPChNum].PageVol=49;
					SendPageVol(CurrentOPNum,CurrentOPChNum,OPInfo[CurrentOPChNum].PageVol);	//routine will determnine if zoning is needed
					break;
				case SELECT_OPTION:
					IncreaseZoneOP();
					break;
				case SELECT_SUBOPTION:
					IncreaseChannel();
					break;
			}
			break;
		case PAGE_MUTE:
			switch(CurrentSelectLocation)
			{
				case SELECT_VALUE:
					OPInfo[CurrentOPChNum].PageMute = !OPInfo[CurrentOPChNum].PageMute;
					SendPageMute(CurrentOPNum,CurrentOPChNum,OPInfo[CurrentOPChNum].PageMute);	//routine will determnine if zoning is needed
					break;
				case SELECT_OPTION:
					IncreaseZoneOP();
					break;
				case SELECT_SUBOPTION:
					IncreaseChannel();
					break;
			}
			break;
		case IO_SETUP:
			switch(CurrentSelectLocation)
			{
				case SELECT_VALUE:
					if(IO_Setting[CurrentIO]==IO_DISABLED)
						IO_Setting[CurrentIO]=IO_NORMAL;
					else if(IO_Setting[CurrentIO]==IO_NORMAL)
						IO_Setting[CurrentIO]=IO_SUPERVISED;
					else
						IO_Setting[CurrentIO]=IO_DISABLED;
					break;
				case SELECT_OPTION:
					IncreaseIOCh();
					break;
//				case SELECT_SUBOPTION:
//					IncreaseChannel();
//					break;
			}
			break;
		case UNIT_NUMBER:
			CurrentUnitNum++;
			if(CurrentUnitNum>10)
				CurrentUnitNum=10;
			break;
#ifdef COMBO_UNIT
		case RCA_VOLUME:
			Pot_Audio++;
			if(Pot_Audio>=NUM_VOLUME_STEPS)
				Pot_Audio=NUM_VOLUME_STEPS-1;
			CurrentVolStep=Pot_Audio;	//pull back flash versions
			AudioVolume=VolumeSteps[CurrentVolStep];
			
			result=WriteMCP45HV(POT_ADDR,BUS_A,AudioVolume);
			
			break;
#endif
		case TEST_TONE:
			switch(CurrentSelectLocation)
			{
				case SELECT_VALUE:
					OPInfo[CurrentOPChNum].TestTone = !OPInfo[CurrentOPChNum].TestTone;
					SendTestTone(CurrentOPNum,CurrentOPChNum,OPInfo[CurrentOPChNum].TestTone);	//routine will determnine if zoning is needed
					break;
				case SELECT_OPTION:
					IncreaseZoneOP();
					break;
				case SELECT_SUBOPTION:
					IncreaseChannel();
					break;
			}
			break;
		case FAULTS:
			switch(CurrentSelectLocation)
			{
				case SELECT_VALUE:
					CurrentFaultIndex=GetNextCPUFaultUp();	//position pointers to first fault if there is one
					break;
				case SELECT_OPTION:
					ClearCurrentFault();
					break;
				case SELECT_SUBOPTION:
					SilenceAlarm();
					break;
			}
			break;
//		case IP_ADD:
//			strcpy(LCDBuff,"IP Info   ");
//			break;
//		case VOLTAGES:
//			strcpy(LCDBuff,"Voltages  ");
//			break;
//		case FAULTS:
//			strcpy(LCDBuff,"Faults    ");
//			break;
//		case UNIT_NUMBER:
//			strcpy(LCDBuff,"Unit Num  ");
//			break;
	}
	
	PaintFunctions();

}

void ItemDown()
{
	switch(CurrentFunc)
	{
		case MASK_VOL:
			switch(CurrentSelectLocation)
			{
				case SELECT_VALUE:
					
					if(OPInfo[CurrentOPChNum].MaskVol>0)
						OPInfo[CurrentOPChNum].MaskVol--;
					SendMaskingVol(CurrentOPNum,CurrentOPChNum,OPInfo[CurrentOPChNum].MaskVol);
					break;
				case SELECT_OPTION:
					DecreaseZoneOP();
					break;
				case SELECT_SUBOPTION:
					DecreaseChannel();
					break;
			}
			break;
		case MASK_CONTOUR_F:
			switch(CurrentSelectLocation)
			{
				case SELECT_VALUE:
					if(OPInfo[CurrentOPChNum].MaskContour>1)
						OPInfo[CurrentOPChNum].MaskContour--;
					SendMaskingCont(CurrentOPNum,CurrentOPChNum,OPInfo[CurrentOPChNum].MaskContour);	//routine will determnine if zoning is needed
					break;
				case SELECT_OPTION:
					DecreaseZoneOP();
					break;
				case SELECT_SUBOPTION:
					DecreaseChannel();
					break;
			}
			break;
		case MASK_MUTE:
			switch(CurrentSelectLocation)
			{
				case SELECT_VALUE:
					OPInfo[CurrentOPChNum].MaskMute = !OPInfo[CurrentOPChNum].MaskMute;
					SendMaskingMute(CurrentOPNum,CurrentOPChNum,OPInfo[CurrentOPChNum].MaskMute);	//routine will determnine if zoning is needed
					//SendTestTone(CurrentOPNum,CurrentOPChNum,0);	//routine will determnine if zoning is needed
					break;
				case SELECT_OPTION:
					DecreaseZoneOP();
					break;
				case SELECT_SUBOPTION:
					DecreaseChannel();
					break;
			}
			break;
		case MUSIC_VOL:
			switch(CurrentSelectLocation)
			{
				case SELECT_VALUE:
					if(OPInfo[CurrentOPChNum].MusicVol>0)
						OPInfo[CurrentOPChNum].MusicVol--;
					SendMusicVol(CurrentOPNum,CurrentOPChNum,OPInfo[CurrentOPChNum].MusicVol);	//routine will determnine if zoning is needed
					break;
				case SELECT_OPTION:
					DecreaseZoneOP();
					break;
				case SELECT_SUBOPTION:
					DecreaseChannel();
					break;
			}
			break;
		case MUSIC_MUTE:
			switch(CurrentSelectLocation)
			{
				case SELECT_VALUE:
					OPInfo[CurrentOPChNum].MusicMute = !OPInfo[CurrentOPChNum].MusicMute;
					SendMusicMute(CurrentOPNum,CurrentOPChNum,OPInfo[CurrentOPChNum].MusicMute);	//routine will determnine if zoning is needed
					break;
				case SELECT_OPTION:
					DecreaseZoneOP();
					break;
				case SELECT_SUBOPTION:
					DecreaseChannel();
					break;
			}
			break;
		case PAGE_VOL:
			switch(CurrentSelectLocation)
			{
				case SELECT_VALUE:
					if(OPInfo[CurrentOPChNum].PageVol>0)
						OPInfo[CurrentOPChNum].PageVol--;
					SendPageVol(CurrentOPNum,CurrentOPChNum,OPInfo[CurrentOPChNum].PageVol);	//routine will determnine if zoning is needed
					break;
				case SELECT_OPTION:
					DecreaseZoneOP();
					break;
				case SELECT_SUBOPTION:
					DecreaseChannel();
					break;
			}
			break;
		case PAGE_MUTE:
			switch(CurrentSelectLocation)
			{
				case SELECT_VALUE:
					OPInfo[CurrentOPChNum].PageMute = !OPInfo[CurrentOPChNum].PageMute;
					SendPageMute(CurrentOPNum,CurrentOPChNum,OPInfo[CurrentOPChNum].PageMute);	//routine will determnine if zoning is needed
					break;
				case SELECT_OPTION:
					DecreaseZoneOP();
					break;
				case SELECT_SUBOPTION:
					DecreaseChannel();
					break;
			}
			break;
		case TEST_TONE:
			switch(CurrentSelectLocation)
			{
				case SELECT_VALUE:
					OPInfo[CurrentOPChNum].TestTone = !OPInfo[CurrentOPChNum].TestTone;
					SendTestTone(CurrentOPNum,CurrentOPChNum,OPInfo[CurrentOPChNum].TestTone);	//routine will determnine if zoning is needed
					break;
				case SELECT_OPTION:
					DecreaseZoneOP();
					break;
				case SELECT_SUBOPTION:
					DecreaseChannel();
					break;
			}
			break;
		case IO_SETUP:
			switch(CurrentSelectLocation)
			{
				case SELECT_VALUE:
					if(IO_Setting[CurrentIO]==IO_SUPERVISED)
						IO_Setting[CurrentIO]=IO_NORMAL;
					else if(IO_Setting[CurrentIO]==IO_NORMAL)
						IO_Setting[CurrentIO]=IO_DISABLED;
					else
						IO_Setting[CurrentIO]=IO_SUPERVISED;
					break;
				case SELECT_OPTION:
					DecreaseIOCh();
					break;
//				case SELECT_SUBOPTION:
//					IncreaseChannel();
//					break;
			}
			break;
		case UNIT_NUMBER:
			if(CurrentUnitNum>1)
				CurrentUnitNum--;
			break;
#ifdef COMBO_UNIT
		case RCA_VOLUME:
			if(Pot_Audio>0)
				Pot_Audio--;
			CurrentVolStep=Pot_Audio;	//pull back flash versions
			AudioVolume=VolumeSteps[CurrentVolStep];
			result=WriteMCP45HV(POT_ADDR,BUS_A,AudioVolume);

			break;
#endif
		case FAULTS:
			switch(CurrentSelectLocation)
			{
				case SELECT_VALUE:
					CurrentFaultIndex=GetNextCPUFaultDown();	//position pointers to first fault if there is one
					break;
				case SELECT_OPTION:
					ClearCurrentFault();
					break;
				case SELECT_SUBOPTION:
					SilenceAlarm();
					break;
			}
			break;
			
	}
	
	PaintFunctions();

}

void IncreaseIOCh()
{
	CurrentIO++;
	if(CurrentIO >= NUM_IO)
		CurrentIO=0;
}

void DecreaseIOCh()
{
	if(CurrentIO > 0)
		CurrentIO--;
	else
		CurrentIO=NUM_IO-1;
}

void IncreaseZoneOP()
{
	switch(CurrentOPOption)
	{
		case OPTION_OP_ALL:
#ifdef COMBO_UNIT
			CurrentOPNum=1;
			CurrentOPChNum=0;
			PaintChNum();
			CurrentOPOption=OPTION_OP;
			GetOPValues();
#endif
			break;			
		case 	OPTION_OP:
			CurrentOPNum++;
#ifdef COMBO_UNIT
			if(CurrentOPNum >2)
			{
				CurrentOPNum=1;
				CurrentOPOption=OPTION_OP_ALL;
				CurrentOPChNum=0;
				PaintChNum();

			}	
#else
			if(CurrentOPNum >200)
				CurrentOPNum=200;
#endif

			if(CurrentOPNum <= 100)
			{
					OP_Subnet=2;
					OP_Node=CurrentOPNum;
			}
			else
			{
					OP_Subnet=3;
					OP_Node=CurrentOPNum-100;
			}
			//GetOPValues();
			FetchWait=FETCH_WAIT_TIME;	//hold off fetching op data in case we are scrolling through ops
			break;
		case OPTION_ZONE:
			if(CurrentZoneNum<=255)
			{	
				CurrentZoneNum++;
			}
			break;
	}
	
}

void DecreaseZoneOP()
{
	switch(CurrentOPOption)
	{
		case OPTION_OP_ALL:
#ifdef COMBO_UNIT
			CurrentOPNum=2;
			CurrentOPChNum=0;
			PaintChNum();
			CurrentOPOption=OPTION_OP;
			GetOPValues();

#endif
			break;			
		case OPTION_OP:
			
#ifdef COMBO_UNIT
			if(CurrentOPNum >1)
				CurrentOPNum--;
			else
			{
				CurrentOPNum=1;
				CurrentOPOption=OPTION_OP_ALL;
				CurrentOPChNum=0;
				PaintChNum();

			}	
#else
			if(CurrentOPNum >1)
				CurrentOPNum--;
#endif

			//if(CurrentOPNum >1)
			//	CurrentOPNum--;
			if(CurrentOPNum <= 100)
			{
					OP_Subnet=2;
					OP_Node=CurrentOPNum;
			}
			else
			{
					OP_Subnet=3;
					OP_Node=CurrentOPNum-100;
			}
			//GetOPValues();
			FetchWait=FETCH_WAIT_TIME;	//hold off fetching op data in case we are scrolling through ops
			break;
		case OPTION_ZONE:
			if(CurrentZoneNum>1)
			{	
				CurrentZoneNum--;
			}
			break;
	}
	
}

void IncreaseChannel()
{
		CurrentOPChNum++;
		if(CurrentOPChNum >4)
			CurrentOPChNum=0;
	
}

void DecreaseChannel()
{
		
		if(CurrentOPChNum >0)
			CurrentOPChNum--;
		else
			CurrentOPChNum=4;
	
}

void OptionChange()
{
	switch(CurrentFunc)
	{
		case MASK_VOL:
		case MASK_CONTOUR_F:
		case MASK_MUTE:
		case MUSIC_VOL:
		case MUSIC_MUTE:
		case PAGE_VOL:
		case PAGE_MUTE:
		case TEST_TONE:
			if(CurrentOPOption==OPTION_OP)
			{
				CurrentOPOption=OPTION_ZONE;
				OP_State=OP_ALIVE;
			}
			else
				CurrentOPOption=OPTION_OP;
			break;
		case IP_ADD:
			switch(CurrentIPOption)		
			{
				case OPTION_ADDRESS:
					CurrentIPOption=OPTION_SUBNET;
					break;
				case OPTION_SUBNET:
					CurrentIPOption=OPTION_GW;
					break;
				case OPTION_GW:
					CurrentIPOption=OPTION_MAC;
					break;
				case OPTION_MAC:
					CurrentIPOption=OPTION_ADDRESS;
					break;
			}
			break;
		case VERSIONS:
			switch(CurrentVerOption)		
			{
				case OPTION_NEURON:
					CurrentVerOption=OPTION_SM2;
					break;
				case OPTION_SM2:
					CurrentVerOption=OPTION_NEURON;
					break;
			}
			break;
#ifdef COMBO_UNIT
		case VOLTAGES:
			switch(CurrentDiagVoltage)
			{
				case 	V_15V:
					CurrentDiagVoltage=V_5V_M;
					break;
				case 	V_5V_M:
					CurrentDiagVoltage=V_5V;
					break;
				case 	V_5V:
					CurrentDiagVoltage=V_5V_2;
					break;
				case 	V_5V_2:
					CurrentDiagVoltage=V_5V_H;
					break;
				case 	V_5V_H:
					CurrentDiagVoltage=V_5V_ISO_H;
					break;
				case 	V_5V_ISO_H:
					CurrentDiagVoltage=V_33V;
					break;
				case 	V_33V:
					CurrentDiagVoltage=V_33V_2;
					break;
				case 	V_33V_2:
					CurrentDiagVoltage=V_33V_H;
					break;
				case 	V_33V_H:
					CurrentDiagVoltage=V_126V;
					break;
				case 	V_126V:
					CurrentDiagVoltage=V_126V_2;
					break;
				case 	V_126V_2:
					CurrentDiagVoltage=V_15V;
					break;
				
			}
			break;
#else
		case VOLTAGES:
			switch(CurrentDiagVoltage)
			{
				case 	V_24V:
					CurrentDiagVoltage=V_5V;
					break;
				case 	V_5V:
					CurrentDiagVoltage=V_ISO_5V;
					break;
				case 	V_ISO_5V:
					CurrentDiagVoltage=V_33V;
					break;
				case 	V_33V:
					CurrentDiagVoltage=V_24V;
					break;
				
			}
			break;
#endif
			
	}
	CurrentSelectLocation=SELECT_OPTION;
	PaintFunctions();
}

//void FunctionChange()
//{
//	CurrentFunc++;
//	if(CurrentFunc >=NUM_FUNC)
//		CurrentFunc=0;
//
//	if(!DeviceInet)
//	{
//		if(CurrentFunc<IP_ADD)
//			CurrentFunc=IP_ADD; //skip all inet functions
//		
//	}
//	
//
//	if(CurrentFunc==IP_ADD)
//		CurrentFunc++;	//skip for now
//
//	if(CurrentFunc==VOLTAGES)
//	{
//		if(PcbVer<1)
//			CurrentFunc++;	//skip, no hw
//	}
//	if(CurrentFunc==TEST_TONE)
//		CurrentFunc++;	//skip. tone has issue on op side	
//	if(CurrentFunc==UNIT_NUMBER)
//		CurrentFunc++;	//skip for now
//
//	if(CurrentFunc >=NUM_FUNC)
//		CurrentFunc=0;
//	CurrentSelectLocation=SELECT_OPTION;
//	Functions();
//}

void FunctionChangeInet()
{
	switch(CurrentFunc)
	{
		case MASK_VOL:
			CurrentFunc=MASK_CONTOUR_F;
			break;
		case MASK_CONTOUR_F:
			CurrentFunc=MASK_MUTE;
			break;
		case MASK_MUTE:
			CurrentFunc=MUSIC_VOL;
			break;
		case MUSIC_VOL:
			CurrentFunc=MUSIC_MUTE;
			break;
		case MUSIC_MUTE:
#ifdef COMBO_UNIT
			CurrentFunc=IP_ADD;	//no paging on combo
#else
			CurrentFunc=PAGE_VOL;
#endif
			break;
		case PAGE_VOL:
			CurrentFunc=PAGE_MUTE;
			break;
		case PAGE_MUTE:
			CurrentFunc=IP_ADD;
			break;
		case IP_ADD:
			if(PcbVer>0)
				CurrentFunc=VOLTAGES;
			else
				CurrentFunc=FAULTS;
			break;
		case VOLTAGES:
			CurrentFunc=FAULTS;
			break;
#ifdef COMBO_UNIT
		case FAULTS:
			CurrentFunc=VERSIONS;
			break;
#else
		case FAULTS:
			CurrentFunc=IO_SETUP;
			break;
#endif
		case IO_SETUP:
			CurrentFunc=VERSIONS;
			break;
#ifdef COMBO_UNIT
		case VERSIONS:
			CurrentFunc=RCA_VOLUME;
			break;
		case RCA_VOLUME:
			CurrentFunc=LCD_INFO;
			break;
		case LCD_INFO:
			CurrentFunc=MASK_VOL;
			GetOPValues(); //see if op is here
			break;
#else
		case VERSIONS:
			CurrentFunc=MASK_VOL;
			GetOPValues(); //see if op is here
			break;

#endif
			
	}
	CurrentSelectLocation=SELECT_VALUE;
	Functions();

}

void FunctionChangeNform()
{
	switch(CurrentFunc)
	{
		case IP_ADD:
			if(PcbVer>0)
				CurrentFunc=VOLTAGES;
			else
				CurrentFunc=FAULTS;
			break;
		case VOLTAGES:
			CurrentFunc=FAULTS;
			break;
		case FAULTS:
			CurrentFunc=IO_SETUP;
			break;
		case IO_SETUP:
			CurrentFunc=VERSIONS;
			break;
		case VERSIONS:
			CurrentFunc=IP_ADD;
			
	}
	CurrentSelectLocation=SELECT_VALUE;
	Functions();
	
}

 
void PaintFunctions()
{
	switch(CurrentFunc)
	{
		case MASK_VOL:
			PaintMaskVolume();
			break;
		case MASK_CONTOUR_F:
			PaintMaskContour();
			break;
		case MASK_MUTE:
			PaintMaskMute();
			break;
		case MUSIC_VOL:
			PaintMusicVolume();
			break;
		case MUSIC_MUTE:
			PaintMusicMute();
			break;
		case PAGE_VOL:
			PaintPageVolume();
			break;
		case PAGE_MUTE:
			PaintPageMute();
			break;
		case IP_ADD:
			PaintIP_Address();
			break;
		case VERSIONS:
			PaintVersions();
			break;
#ifdef COMBO_UNIT
		case RCA_VOLUME:
			PaintRCA();
			break;
		case LCD_INFO:
			PaintLCD_Info();
			break;
#endif
		case VOLTAGES:
			PaintVoltages();
			break;
		case FAULTS:
			PaintFaults();
			break;
		case IO_SETUP:
			PaintIO();
			break;
		case TEST_TONE:
			PaintTestTone();
			break;
		case UNIT_NUMBER:
			PaintUnitNumber();
			break;
	}

	PaintSelection();	//this should be the last paint so cursor blinks in correct location
}

void PaintOPOption()
{
	switch(CurrentOPOption)
	{
		case OPTION_OP:
		case OPTION_OP_ALL:
			PaintOPNum();
			if(CurrentFunc != TEST_TONE)
				PaintChNum();
			else
				PaintSubOptionBlank();			
			
			break;
		case OPTION_ZONE:
			PaintZoneNum();
			PaintSubOptionBlank();			
			break;	
	}

}



void PaintIPOption()
{
	switch(CurrentIPOption)
	{
		case OPTION_ADDRESS:
			strcpy(LCDBuff,"  Addr");
			LCD_String(0,10);
			i=ConvNumToBuf(AddrOctet[0],3,0);	
			LCD_String(1,0);
			strcpy(LCDBuff,".");
			LCD_String(1,3);
			i=ConvNumToBuf(AddrOctet[1],3,0);	
			LCD_String(1,4);
			strcpy(LCDBuff,".");
			LCD_String(1,7);
			i=ConvNumToBuf(AddrOctet[2],3,0);	
			LCD_String(1,8);
			strcpy(LCDBuff,".");
			LCD_String(1,11);
			i=ConvNumToBuf(AddrOctet[3],3,0);	
			LCD_String(1,12);
			strcpy(LCDBuff," ");
			LCD_String(1,15);
			break;
		case OPTION_SUBNET:
			strcpy(LCDBuff,"Subnet");
			LCD_String(0,10);
			i=ConvNumToBuf(SubOctet[0],3,0);	
			LCD_String(1,0);
			strcpy(LCDBuff,".");
			LCD_String(1,3);
			i=ConvNumToBuf(SubOctet[1],3,0);	
			LCD_String(1,4);
			strcpy(LCDBuff,".");
			LCD_String(1,7);
			i=ConvNumToBuf(SubOctet[2],3,0);	
			LCD_String(1,8);
			strcpy(LCDBuff,".");
			LCD_String(1,11);
			i=ConvNumToBuf(SubOctet[3],3,0);	
			LCD_String(1,12);
			strcpy(LCDBuff," ");
			LCD_String(1,15);
			break;
		case OPTION_GW:
			strcpy(LCDBuff,"    GW");
			LCD_String(0,10);
			i=ConvNumToBuf(GwOctet[0],3,0);	
			LCD_String(1,0);
			strcpy(LCDBuff,".");
			LCD_String(1,3);
			i=ConvNumToBuf(GwOctet[1],3,0);	
			LCD_String(1,4);
			strcpy(LCDBuff,".");
			LCD_String(1,7);
			i=ConvNumToBuf(GwOctet[2],3,0);	
			LCD_String(1,8);
			strcpy(LCDBuff,".");
			LCD_String(1,11);
			i=ConvNumToBuf(GwOctet[3],3,0);	
			LCD_String(1,12);
			strcpy(LCDBuff," ");
			LCD_String(1,15);
			break;
		case OPTION_MAC:
			strcpy(LCDBuff,"   MAC");
			LCD_String(0,10);
			strcpy(LCDBuff,"    ");
			LCD_String(1,0);
			HexToStr(MACOctet[0]);
			LCD_String(1,4);
			HexToStr(MACOctet[1]);
			LCD_String(1,6);
			HexToStr(MACOctet[2]);
			LCD_String(1,8);
			HexToStr(MACOctet[3]);
			LCD_String(1,10);
			HexToStr(MACOctet[4]);
			LCD_String(1,12);
			HexToStr(MACOctet[5]);
			LCD_String(1,14);
			break;
	}


}

void HexToStr(unsigned short Value)
{
	unsigned short Upper,Lower;
	
	Lower=Value&0x0f;
	Upper=(Value>>4)&0x0f;
	
	if(Lower<10)
	{
		Lower=Lower| 0x30;
	}
	else
	{
		Lower=55+Lower;	
	}
		
	if(Upper<10)
	{
		Upper=Upper| 0x30;
	}
	else
	{
		Upper=55+Upper;	
	}
	
	LCDBuff[0]=Upper;
	LCDBuff[1]=Lower;
	LCDBuff[2]=0;
}

void PaintSubOptionBlank()
{
	strcpy(LCDBuff,"        ");
	LCD_String(1,0);

}

void PaintOPNum()
{
if(CurrentOPOption==OPTION_OP_ALL)
{
		strcpy(LCDBuff,"OP ALL");
	LCD_String(0,10);
	//i=ConvNumToBuf(CurrentOPNum,3,0);	
//	LCD_String(0,13);

}
else
{
	strcpy(LCDBuff," OP");
	LCD_String(0,10);
	i=ConvNumToBuf(CurrentOPNum,3,0);	
	LCD_String(0,13);
}
}

void PaintIONum()
{
	strcpy(LCDBuff," IO-");
	LCD_String(0,10);
	i=ConvNumToBuf(CurrentIO+1,2,0);	
	LCD_String(0,14);

}


void SelectChange()
{

	switch(CurrentSelectLocation)
	{
		case SELECT_OPTION:
			switch(CurrentFunc)
			{
				//these cases dont have sub obtions in zn mode
				case MASK_VOL:
				case MUSIC_VOL:
				case PAGE_VOL:
				case MASK_CONTOUR_F:
				case MASK_MUTE:
				case MUSIC_MUTE:
				case PAGE_MUTE:
					if(CurrentOPOption==OPTION_OP)
						CurrentSelectLocation=SELECT_SUBOPTION;
					else
						CurrentSelectLocation=SELECT_VALUE;
					break;
				case TEST_TONE:
				case IO_SETUP:
					CurrentSelectLocation=SELECT_VALUE;
					break;
				case FAULTS: 
					CurrentSelectLocation=SELECT_SUBOPTION;
					break;
			}
			break;
		case SELECT_SUBOPTION:
			CurrentSelectLocation=SELECT_VALUE;
			break;
		case SELECT_VALUE:
			CurrentSelectLocation=SELECT_OPTION;
			break;	
	}
	
	PaintSelection();
	
}

void PaintSelection()
{
	unsigned short OptionLoc=0;
	unsigned short SubOptionLoc=0;
	unsigned short ValueLoc=0;

	switch(CurrentFunc)
	{
		case MASK_VOL:
		case MUSIC_VOL:
		case PAGE_VOL:
			OptionLoc=0x0f;
			SubOptionLoc=0x47;
			ValueLoc=0x4c;
			break;
		case MASK_CONTOUR_F:
		case MASK_MUTE:
		case MUSIC_MUTE:
		case PAGE_MUTE:
		case UNIT_NUMBER:
		case TEST_TONE:
		case IO_SETUP:
			OptionLoc=0x0f;
			SubOptionLoc=0x47;
			ValueLoc=0x4f;
			break;
#ifdef COMBO_UNIT
		case RCA_VOLUME:
			OptionLoc=0x0f;
			SubOptionLoc=0x47;
			ValueLoc=0x4f;
			SendCommand(0x0c);// display on, cursor off, blink off
			msec_delay(DELAY37USEC); //wait 37 usec
			return;
			break;
		case LCD_INFO:
#endif
		case IP_ADD:
		case VERSIONS:
		case VOLTAGES:
			SendCommand(0x0c);// display on, cursor off, blink off
			msec_delay(DELAY37USEC); //wait 37 usec
			return;
			break;
		case FAULTS:
			OptionLoc=0x0f;
			SubOptionLoc=0x43;
			ValueLoc=0x4f;
			break;

	}



	switch(CurrentSelectLocation)
	{
		case SELECT_OPTION:
			
			SendCommand(0x0d);// display on, cursor off, blink on
			msec_delay(DELAY37USEC); //wait 37 usec
			SendCommand(0x80 | OptionLoc);// goto pos
			msec_delay(DELAY37USEC); //wait 37 usec
			break;
		case SELECT_SUBOPTION:
			SendCommand(0x0d);// display on, cursor off, blink off
			msec_delay(DELAY37USEC); //wait 37 usec
			SendCommand(0x80 | SubOptionLoc);// goto pos 
			msec_delay(DELAY37USEC); //wait 37 usec
			break;
		case SELECT_VALUE:
			SendCommand(0x0d);// display on, cursor off, blink off
			msec_delay(DELAY37USEC); //wait 37 usec
			SendCommand(0x80 | ValueLoc);// goto pos 
			msec_delay(DELAY37USEC); //wait 37 usec
			break;
	}
	
}


void PaintChNum()
{
	if(CurrentOPChNum==0)
	{
		strcpy(LCDBuff,"Chan ALL");
		LCD_String(1,0);
	}
	else
	{
		strcpy(LCDBuff,"Chan   ");
		LCD_String(1,0);
		switch(CurrentOPChNum)
		{
			case 1:
				strcpy(LCDBuff,"A");
				break;
			case 2:
				strcpy(LCDBuff,"B");
				break;
			case 3:
				strcpy(LCDBuff,"C");
				break;
			case 4:
				strcpy(LCDBuff,"D");
				break;
				
		}
//		i=ConvNumToBuf(CurrentOPChNum,2,0);	
		LCD_String(1,7);
	}
}

void PaintZoneNum()
{
	strcpy(LCDBuff," ZN");
	LCD_String(0,10);
	i=ConvNumToBuf(CurrentZoneNum,3,0);	
	LCD_String(0,13);
	
}

void PaintMaskdbVol(short val)
{
	int i;
	long smval;
	
	switch(OP_State)
	{
		case OP_ALIVE:
			smval=val;
			smval=smval*5;
			smval=smval+340;
			
			//smval=340+(val*5);
			
			strcpy(LCDBuff," ");	
			LCD_String(1,8);
			
			i=ConvNumToBuf(smval,3,1);
			LCD_String(1,9);

			strcpy(LCDBuff," dB");	
			LCD_String(1,13);
			break;
		case OP_FETCHING:
			strcpy(LCDBuff,"..Wait..");	
			LCD_String(1,8);
			break;
		case OP_OFFLINE:
			strcpy(LCDBuff," Offline");	
			LCD_String(1,8);
			break;
	}
	

	
}

#ifdef COMBO_UNIT
void PaintRCAVal()
{
	unsigned short lcdCount,i;
	
			//strcpy(LCDBuff,"     ");	
			//LCD_String(1,8);
			
//			i=ConvNumToBuf(VolumeSteps[Pot_Audio],3,0);

			lcdCount=BarSteps[Pot_Audio];
			if(lcdCount<=16)
			{
				for(i=0;i<16;i++)
				{
					if(i<lcdCount)
						LCDBuff[i]=0xff;	//block
					else
						LCDBuff[i]=0x20;	//space
					
				}
				LCDBuff[16]=0;	//null
				//LCD_String(1,13);
				LCD_String(1,0);
			}
//			strcpy(LCDBuff," dB");	
//			LCD_String(1,13);

	

	
}

#endif



void PaintMaskContdb(short val)
{
	int i;

	switch(OP_State)
	{
		case OP_ALIVE:
			strcpy(LCDBuff,"     ");	
			LCD_String(1,8);
			
			if(val >=15)
			{
				strcpy(LCDBuff,"+");	
				LCD_String(1,13);
				i=ConvNumToBuf((val-15),2,0);
			}
			else
			{
				strcpy(LCDBuff,"-");	
				LCD_String(1,13);
				i=ConvNumToBuf((15-val),2,0);
			}
			
			LCD_String(1,14);	
			break;
		case OP_FETCHING:
			strcpy(LCDBuff,"..Wait..");	
			LCD_String(1,8);
			break;
		case OP_OFFLINE:
			strcpy(LCDBuff," Offline");	
			LCD_String(1,8);
			break;
	}
	


//	strcpy(LCDBuff," dB");	
//	LCD_String(1,13);
	
}

void PaintPageVoldb(short val)
{
	int i;
	long smval;
	
	switch(OP_State)
	{
		case OP_ALIVE:
			smval=val;
			smval=smval*10;
			smval=smval+340;
			
			//smval=340+(val*5);
			
			strcpy(LCDBuff," ");	
			LCD_String(1,8);
			
			i=ConvNumToBuf(smval,3,1);
			LCD_String(1,9);

			strcpy(LCDBuff," dB");	
			LCD_String(1,13);
			break;
		case OP_FETCHING:
			strcpy(LCDBuff,"..Wait..");	
			LCD_String(1,8);
			break;
		case OP_OFFLINE:
			strcpy(LCDBuff," Offline");	
			LCD_String(1,8);
			break;
	}

	
}

void PaintMusicVoldb(short val)
{
	int i;
	long smval;
	
	switch(OP_State)
	{
		case OP_ALIVE:
			smval=val;
			smval=smval*10;
			smval=smval+340;
			
			//smval=340+(val*5);
			
			strcpy(LCDBuff," ");	
			LCD_String(1,8);
			
			i=ConvNumToBuf(smval,3,1);
			LCD_String(1,9);

			strcpy(LCDBuff," dB");	
			LCD_String(1,13);
			break;
		case OP_FETCHING:
			strcpy(LCDBuff,"..Wait..");	
			LCD_String(1,8);
			break;
		case OP_OFFLINE:
			strcpy(LCDBuff," Offline");	
			LCD_String(1,8);
			break;
	}
	
}

void ReadLCDBits(enum OPS OP)
{
	unsigned long temp,temp2,temp3;//,temp4;
	unsigned short iAddress;
	unsigned short LcdAddr;
	
	boolean result;
	(void)SelectBus(BUS_C);
//	OutBuff[0]=0x12;	// this is the address of GPIOA. B is right after it. So, just do a write to set teh address register to 12
	OutBuff[0]=0x0e;	// this is the address of INTCAPA. B is right after it. So, just do a write to set teh address register to 10

	switch(OP)
	{
		case 	OP_1:
			iAddress=LCD_IO_ADDR_1;
			LcdAddr=0;
			break;
		case 	OP_2:
			iAddress=LCD_IO_ADDR_2;
			LcdAddr=1; //4
			break;
			
	}


	result = io_out(ioI2C, &OutBuff, iAddress, 1);	// this just sends the address byte. no data bytes are written
	if(!result)
			return;	//device isnt there 
			//CPUFault(DEVICE_ID_KEYPAD,FX_AUDIO_I2C);

	result = io_in(ioI2C, InBuff, iAddress, 6);	// now read in the A and B gpio register
//		if(!result)
//			CPUFault(DEVICE_ID_KEYPAD,FX_AUDIO_I2C);


//A is digit 3 with upper bit from 2nd digit
//B is digit 1 and 2 except lsb which is in A 
//buff[4]= A
//buff[5]= B
	temp=InBuff[5];	//B
	temp=temp<<8;	//B now upper byte
	temp=temp|InBuff[4]; //B and A  in long word
	temp2=temp>>8;	//digit 2
	temp3=temp2>>7;	//digit 1
	//temp4=temp3>>1;	//led
	
	LCD_Digit3[OP]=InBuff[4] & 0x7f;	//clear led bit
	LCD_Digit2[OP]=(unsigned short)(temp2 & 0x007f);	//clear digit 1
	LCD_Digit1[OP]=(unsigned short)(temp3 & 0x0001);	//clear upper stuff
	//StatusLED[OP]=temp4 & 0x0001;
	StatusLED[OP]=InBuff[0] & 0x80;	//from intcapa
	
	//(void)strcpy(LCDBuff,"Line 1");
	if(LCD_Digit1[OP])
		LCDBuff[0]='1';
	else
		LCDBuff[0]=' ';	//no leading digit
	
	LCDBuff[1]=LookupLCD(LCD_Digit2[OP]);
	LCDBuff[2]=LookupLCD(LCD_Digit3[OP]);
	if(StatusLED[OP] ==0)
		LCDBuff[3]=0x5f;
	else
		LCDBuff[3]=0xff;
	
	LCDBuff[4]=0;
	LCD_String(LcdAddr,12);
	
//	OutBuff[0]=0x10;	// this is the address of INTCAPA. B is right after it. So, just do a write to set teh address register to 10
//	result = io_out(ioI2C, &OutBuff, iAddress, 1);	// this just sends the address byte. no data bytes are written
//	result = io_in(ioI2C, InBuff, iAddress, 1);	// now read in the A and B gpio register

	
//	SendDebug(0x01,LCD_Digit1[OP]);
//	SendDebug(0x02,LCD_Digit2[OP]);
//	SendDebug(0x03,LCD_Digit3[OP]);
//	SendDebug(0x04,StatusLED[OP]);
//	SendDebug(0x05,OP);
//	SendDebug(0x06,InBuff[4]);
//	SendDebug(0x07,InBuff[5]);

	
}

char LookupLCD(unsigned short segments)
{
	char retval=' ';
	
	switch(segments)
	{
		case 0x7d:
			retval='0';
			break;
		case 0x30:
			retval='1';
			break;
		case 0x6e:
			retval='2';
			break;
		case 0x7a:
			retval='3';
			break;
		case 0x33:
			retval='4';
			break;
		case 0x5b:
			retval='5';
			break;
		case 0x1f:
			retval='6';
			break;
		case 0x70:
			retval='7';
			break;
		case 0x7f:
			retval='8';
			break;
		case 0x73:
			retval='9';
			break;
		case 0x77:
			retval='A';
			break;
		case 0x66:
			retval='B';	//this was a new value I added to show B in stead of 6
			break;
		case 0x4d:
			retval='C';
			break;
		case 0x3e:
			retval='D';
			break;
		case 0x0d:
			retval='L';
			break;
				
	}
	return(retval);
}



void GetOPValues()
{
	if(CurrentOPOption == OPTION_OP_ALL)
		return;
		
	OP_State=OP_FETCHING;
	Functions();
	SendGetVolumes(MASK_ATTENUATION_CONTOUR);	
	for(i=0;i<5;i++)
	{
		OPInfo[i].MaskMute=FALSE;
		OPInfo[i].PageMute=FALSE;
		OPInfo[i].MusicMute=FALSE;
		
	}
}


void PaintMaskVolume()
{
	PaintOPOption();
	PaintMaskdbVol(OPInfo[CurrentOPChNum].MaskVol);	
}



void PaintMaskContour()
{
	PaintOPOption();
	PaintMaskContdb(OPInfo[CurrentOPChNum].MaskContour);
}

void PaintMaskMute()
{
	PaintOPOption();
	PaintMute(OPInfo[CurrentOPChNum].MaskMute);
}

void PaintMusicVolume()
{
	PaintOPOption();
	PaintMusicVoldb(OPInfo[CurrentOPChNum].MusicVol);
}

void PaintMusicMute()
{
	PaintOPOption();
	PaintMute(OPInfo[CurrentOPChNum].MusicMute);
}

void PaintPageVolume()
{
	PaintOPOption();
	PaintPageVoldb(OPInfo[CurrentOPChNum].PageVol);
}

void PaintPageMute()
{
	PaintOPOption();
	PaintMute(OPInfo[CurrentOPChNum].PageMute);
}

void PaintIO()
{
	PaintIONum();
	PaintSubOptionBlank();			
	PaintIOMode(IO_Setting[CurrentIO]);
}

#ifdef COMBO_UNIT
void PaintRCA()
{
	//clear upper right
	PaintSubOptionBlank();			
	PaintRCAVal();
}
#endif


void PaintTestTone()
{
	PaintOPOption();
	PaintTestToneState(OPInfo[CurrentOPChNum].TestTone);
}

void PaintIP_Address()
{
	PaintIPOption();
	
}

void PaintVersions()
{
	PaintVerOption();
	
}

void PaintVerOption()
{
	switch(CurrentVerOption)
	{
		case OPTION_NEURON:
#ifdef COMBO_UNIT
			strcpy(LCDBuff,"          ");
			LCD_String(1,0);
#endif
			strcpy(LCDBuff,"Neuron");
			LCD_String(0,10);
			i=ConvNumToBuf(NEURON_SW_VERSION_MAJOR,2,0);	
			LCD_String(1,0);
			strcpy(LCDBuff,".");
			LCD_String(1,3);
			i=ConvNumToBuf(NEURON_SW_VERSION_MINOR,2,0);	
			LCD_String(1,4);
			strcpy(LCDBuff,".");
			LCD_String(1,7);
			i=ConvNumToBuf(NEURON_SW_VERSION_PATCH,2,0);	
			LCD_String(1,8);
//			strcpy(LCDBuff,".");
//			LCD_String(1,11);
//			i=ConvNumToBuf(AddrOctet[3],3,0);	
//			LCD_String(1,12);
			strcpy(LCDBuff,"      ");
			LCD_String(1,10);
			break;
		case OPTION_SM2:
			strcpy(LCDBuff,"   SM2");
			LCD_String(0,10);
			i=ConvNumToBuf(SM2_VER_MAJOR,2,0);	
			LCD_String(1,0);
			strcpy(LCDBuff,".");
			LCD_String(1,3);
			i=ConvNumToBuf(SM2_VER_MINOR,2,0);	
			LCD_String(1,4);
			strcpy(LCDBuff,".");
			LCD_String(1,7);
			i=ConvNumToBuf(SM2_VER_PATCH,2,0);	
			LCD_String(1,8);
//			strcpy(LCDBuff,".");
//			LCD_String(1,11);
//			i=ConvNumToBuf(AddrOctet[3],3,0);	
//			LCD_String(1,12);
			strcpy(LCDBuff,"      ");
			LCD_String(1,10);
			break;

	}


}

void PaintMute(boolean state)
{
	if(state)
		strcpy(LCDBuff,"   MUTED");
	else
		strcpy(LCDBuff," UNMUTED");
	
	LCD_String(1,8);
		
		
}

void PaintIOMode(enum IO_MODES mode)
{

	switch(mode)
	{
		case 	IO_DISABLED:
			strcpy(LCDBuff,"Disabled");
			break;
		case IO_NORMAL:
			strcpy(LCDBuff,"  Normal");
			break;
		case IO_SUPERVISED:
			strcpy(LCDBuff,"Supervsd");
			break;
	}
	
	
	LCD_String(1,8);
		
		
}

void PaintTestToneState(boolean state)
{
	if(state)
		strcpy(LCDBuff,"Tone On ");
	else
		strcpy(LCDBuff,"Tone Off");
	
	LCD_String(1,8);
		
		
}

void PaintVoltages()
{
	PaintSubOptionBlank();
	PaintVoltageValues();
}

void PaintVoltageValues()
{
	switch(CurrentDiagVoltage)
	{

#ifdef COMBO_UNIT
		case V_33V:
			strcpy(LCDBuff,"A 3.3v");
			LCD_String(0,10);
			strcpy(LCDBuff,"      ");
			LCD_String(1,0);
			fl_to_ascii(&V3Point3Volt,LCDBuff,2,6);
			LCD_String(1,6);
			strcpy(LCDBuff," volts");
			LCD_String(1,10);
			break;
		case V_5V:
			strcpy(LCDBuff,"A 5.0v");
			LCD_String(0,10);
			strcpy(LCDBuff,"      ");
			LCD_String(1,0);
			fl_to_ascii(&V5Volt,LCDBuff,2,6);
			LCD_String(1,6);
			strcpy(LCDBuff," volts");
			LCD_String(1,10);
			break;
		case V_126V:
			strcpy(LCDBuff,"A 1.26");
			LCD_String(0,10);
			strcpy(LCDBuff,"      ");
			LCD_String(1,0);
			fl_to_ascii(&V1Point26Volt,LCDBuff,2,6);
			LCD_String(1,6);
			strcpy(LCDBuff," volts");
			LCD_String(1,10);
			break;
		case V_15V:
			strcpy(LCDBuff,"A  15v");
			LCD_String(0,10);
			strcpy(LCDBuff,"      ");
			LCD_String(1,0);
			fl_to_ascii(&V15Volt,LCDBuff,1,5);
			LCD_String(1,6);
			strcpy(LCDBuff," volts");
			LCD_String(1,10);
			break;
		case V_33V_2:
			strcpy(LCDBuff,"B 3.3v");
			LCD_String(0,10);
			strcpy(LCDBuff,"      ");
			LCD_String(1,0);
			fl_to_ascii(&V3Point3Volt_2,LCDBuff,2,6);
			LCD_String(1,6);
			strcpy(LCDBuff," volts");
			LCD_String(1,10);
			break;
		case V_33V_H:
			strcpy(LCDBuff,"H 3.3v");
			LCD_String(0,10);
			strcpy(LCDBuff,"      ");
			LCD_String(1,0);
			fl_to_ascii(&V3Point3Volt_H,LCDBuff,2,6);
			LCD_String(1,6);
			strcpy(LCDBuff," volts");
			LCD_String(1,10);
			break;
		case V_5V_2:
			strcpy(LCDBuff,"B 5.0v");
			LCD_String(0,10);
			strcpy(LCDBuff,"      ");
			LCD_String(1,0);
			fl_to_ascii(&V5Volt_2,LCDBuff,2,6);
			LCD_String(1,6);
			strcpy(LCDBuff," volts");
			LCD_String(1,10);
			break;		
		case V_5V_H:
			strcpy(LCDBuff,"H 5.0v");
			LCD_String(0,10);
			strcpy(LCDBuff,"      ");
			LCD_String(1,0);
			fl_to_ascii(&V5Volt_H,LCDBuff,2,6);
			LCD_String(1,6);
			strcpy(LCDBuff," volts");
			LCD_String(1,10);
			break;		
		case V_5V_ISO_H:
			strcpy(LCDBuff,"HI 5.0");
			LCD_String(0,10);
			strcpy(LCDBuff,"      ");
			LCD_String(1,0);
			fl_to_ascii(&V5Volt_ISO_H,LCDBuff,2,6);
			LCD_String(1,6);
			strcpy(LCDBuff," volts");
			LCD_String(1,10);
			break;		
		case V_126V_2:
			strcpy(LCDBuff,"B 1.26");
			LCD_String(0,10);
			strcpy(LCDBuff,"      ");
			LCD_String(1,0);
			fl_to_ascii(&V1Point26Volt_2,LCDBuff,2,6);
			LCD_String(1,6);
			strcpy(LCDBuff," volts");
			LCD_String(1,10);
			break;
		case V_5V_M:
			strcpy(LCDBuff,"   -5v");
			LCD_String(0,10);
			strcpy(LCDBuff,"     -");
			LCD_String(1,0);
			fl_to_ascii(&V5Volt_M,LCDBuff,2,6);
			LCD_String(1,6);
			strcpy(LCDBuff," volts");
			LCD_String(1,10);
			break;
#else
		case V_33V:
			strcpy(LCDBuff,"  3.3v");
			LCD_String(0,10);
			strcpy(LCDBuff,"      ");
			LCD_String(1,0);
			fl_to_ascii(&V3Point3Volt,LCDBuff,2,6);
			LCD_String(1,6);
			strcpy(LCDBuff," volts");
			LCD_String(1,10);
			break;
		case V_5V:
			strcpy(LCDBuff,"  5.0v");
			LCD_String(0,10);
			strcpy(LCDBuff,"      ");
			LCD_String(1,0);
			fl_to_ascii(&V5Volt,LCDBuff,2,6);
			LCD_String(1,6);
			strcpy(LCDBuff," volts");
			LCD_String(1,10);
			break;
		case V_ISO_5V:
			strcpy(LCDBuff,"I 5.0v");
			LCD_String(0,10);
			strcpy(LCDBuff,"      ");
			LCD_String(1,0);
			fl_to_ascii(&V5isoVolt,LCDBuff,2,6);
			LCD_String(1,6);
			strcpy(LCDBuff," volts");
			LCD_String(1,10);
			break;
		case V_24V:
			strcpy(LCDBuff,"   24v");
			LCD_String(0,10);
			strcpy(LCDBuff,"      ");
			LCD_String(1,0);
			fl_to_ascii(&V24Volt,LCDBuff,1,5);
			LCD_String(1,6);
			strcpy(LCDBuff," volts");
			LCD_String(1,10);
			break;
#endif


	}
		
		
}


void PaintFaults()
{
	//CurrentFaultIndex=GetNextCPUFaultDown();
	strcpy(LCDBuff," Clear");
	LCD_String(0,10);
	strcpy(LCDBuff,"Mute    ");
	LCD_String(1,0);
	//PaintSubOptionBlank();
	PaintCurrentFault();
}

void PaintUnitNumber()
{
	strcpy(LCDBuff,"    ");
	LCD_String(0,10);
	i=ConvNumToBuf(CurrentUnitNum,2,0);	
	LCD_String(0,14);
	strcpy(LCDBuff,"Enter new unit #");
	LCD_String(1,0);
}

//void PaintCurrentFault()
//{
//	strcpy(LCDBuff,"Gen 1-99");
//	LCD_String(1,8);
//	
//}

void PaintCurrentFault()
{
	unsigned int CurrentFault=0;
	
		
		if((CurrentFaultArray!=ARRAY_NONE) && (CurrentFaultArray!=ARRAY_ENDOFLIST))
		{

			if(CurrentFaultArray==ARRAY_STARTOFLIST)
			{
					CurrentFaultArray=ARRAY_NONE;
					CurrentFaultIndex=GetNextCPUFaultDown();	// this is teh first time we are dispalying faults so search for first one
					
			}


			//we have a cpu fault someplace
			//strcpy(LCDBuff,"ID              ");
			//LCD_String(1,0);

			switch(CurrentFaultArray)
			{
//				case ARRAY_MPI_CPU:
//					strcpy(LCDBuff,"CPU 2-");
//					LCD_String(1,8);
//					i=ConvNumToBuf(CurrentFaultIndex,2,0);	//print code
//					LCD_String(1,14);
//					break;
				case ARRAY_KEYPAD_CPU:
					strcpy(LCDBuff,"CPU 8-");
					LCD_String(1,8);
					i=ConvNumToBuf(CurrentFaultIndex,2,0);	//print code
					LCD_String(1,14);
					break;
//				case ARRAY_MASTER_CPU:
//					strcpy(LCDBuff,"CPU 3-");
//					LCD_String(1,8);
//					i=ConvNumToBuf(CurrentFaultIndex,2,0);	//print code
//					LCD_String(1,14);
//					break;
//				case ARRAY_MPI_GEN:
//					strcpy(LCDBuff,"GEN 2-");
//					LCD_String(1,8);
//					i=ConvNumToBuf(CurrentFaultIndex,2,0);	//print code
//					LCD_String(1,14);
//					break;
				case ARRAY_KEYPAD_GEN:
					strcpy(LCDBuff,"GEN 8-");
					LCD_String(1,8);
					i=ConvNumToBuf(CurrentFaultIndex,2,0);	//print code
					LCD_String(1,14);
					break;
				default:
					strcpy(LCDBuff,"NoFaults");	//probbaly shouldnt have got here but sometimes we do
					LCD_String(1,8);
					break;	
			}
			
			
		}
		else
		{
			if(CurrentFaultArray==ARRAY_NONE)
			{
				strcpy(LCDBuff,"NoFaults");
				LCD_String(1,8);
			}
			else
			{
				strcpy(LCDBuff,"End List");
				LCD_String(1,8);
			}
			
		}
		
	
		
	
}


void SilenceAlarm()
{
	TroubleTick=0;	//stop alerts
	RunningFault=FALSE;	//only new faults should trigger message
	SilenceTimeout=43200;	//12 hrs
	SilenceLoop=0;
	SetAmpMute(TRUE);	//mute amp

//	AudioChannel=0;
//	SelectAudioChannel(AudioChannel);	//prematurely kill audio, but let the contact check continue

//	ClearDisplay();
	strcpy(LCDBuff," Alarm Silenced ");
	LCD_String(0,0);
	PasswordValid=TRUE;
	KeyDeadTimer=NO_INPUT_TIMEOUT;
//	CurrentMode = DIAG_MODE;
//	strcpy(LCDBuff,"Diag    ");
//	LCD_String(0,0);
//	CurrentDiagFunc=DIAG_FAULTS;
//	DiagFunctions();
	CurrentFunc=FAULTS;
	Functions();

}


void ClearCurrentFault()
{
		int j;
		boolean StillFaults=FALSE;
		
		switch(CurrentFaultArray)
		{
			case ARRAY_KEYPAD_GEN:
				if(CurrentFaultIndex<NUM_GEN_FAULTS)
				{
					GenFaults[CurrentFaultIndex]=0;
				//	KeypadGenFaultOPNum[CurrentFaultIndex]=0;
					SendListenerFault(GENERAL_FAULT_CLEAR,DEVICE_ID_1UHE_IO,CurrentFaultIndex);	//clear tye fault on the ilon side
					CurrentFaultIndex=GetNextCPUFaultDown();
					StillFaults=FALSE;
					for(j=0;j<NUM_GEN_FAULTS;j++)
					{
						if(GenFaults[j]>0)
							StillFaults=TRUE;
					}
					//if((KeypadGenFaults[OP_SPEAKER]==0) && (KeypadGenFaults[OP_AMP]==0) && (KeypadGenFaults[OP_TEMP]==0))
					//	HaveOPGenFaults=FALSE;
					if(!StillFaults)
					{
						HaveHEIOGENFaults=FALSE;	//there are no more faults for this array
						if(HaveHEIOGENFaults)
							HaveGenFaults=TRUE;
						else
							HaveGenFaults=FALSE;

						if(!HaveGenFaults)
						{
								GenFaultLED(0);	
								if(!HaveCPUFaults)
								{
										//SendMPICommand(2,0);	//tell mpi to turn off fault relay
										WriteAuxRelays(RELAY_1,1);
										ClearFaultAlarm();
								}
										
						}
					}
					
					PaintFaults();
				}
				break;
			case ARRAY_KEYPAD_CPU:
				if(CurrentFaultIndex<NUM_CPU_FAULTS)
				{
					CpuFaults[CurrentFaultIndex]=0;
					SendListenerFault(CPU_FAULT_CLEAR,DEVICE_ID_1UHE_IO,CurrentFaultIndex);	//clear tye fault on the ilon side
					CurrentFaultIndex=GetNextCPUFaultDown();
					StillFaults=FALSE;
					for(j=0;j<NUM_CPU_FAULTS;j++)
					{
						if(CpuFaults[j]>0)
							StillFaults=TRUE;
					}
					if(!StillFaults)
					{
						HaveHEIOCPUFaults=FALSE;	//there are no more faults for this array
						if(HaveHEIOCPUFaults)
							HaveCPUFaults=TRUE;
						else
							HaveCPUFaults=FALSE;

						if(!HaveCPUFaults)
						{
								CPUFaultLED(0);	
								if(!HaveGenFaults)
								{
										//SendMPICommand(2,0);	//tell mpi to turn off fault relay
										WriteAuxRelays(RELAY_1,1);
										ClearFaultAlarm();
								}
										
						}
					}
					PaintFaults();
				}
				break;
			case ARRAY_ENDOFLIST:
				break;	//should not happen
			case ARRAY_NONE:
				break;	//should not happen
				
		}

}

when(timer_expires(SilenceTimeout))
{
	SilenceLoop++;
	if(SilenceLoop==2)
	{
		SilenceTimeout=0;	//stop time out
		SilenceLoop=0;
		LaunchFault();
	}
}
 
 
void ClearFaultAlarm()
{
	TroubleTick=0;	//stop alerts
	RunningFault=FALSE;	//only new faults should trigger message
	SilenceTimeout=0;	//stop
//	AudioChannel=0;
//	SelectAudioChannel(AudioChannel);	//prematurely kill audio, but let the contact check continue
	
}


void PlayFX(unsigned int Message)
{
	if(PcbVer<1)
		return; //we dont have the hw for this
//	AudioChannel=7;
//	SelectAudioChannel(AudioChannel);
	SetAmpMute(FALSE);	//unmute amp
	WriteFXAudio(Message); 
	FXBounce=200;
	CheckFXPlayContact=2;
	
}

boolean WriteFXAudio(unsigned Value)
{
		SelectBus(BUS_A);
		I2C_OutBuff[0]=0x13;	//GPIO reg for B
		I2C_OutBuff[1]=Value;	
		result = io_out(ioI2C, &I2C_OutBuff, FX_AUDIO_ADDR, 2);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,FX_AUDIO_I2C);
		return(result);
	
}

//boolean WriteFXAudio2(unsigned Value)
boolean WriteFXAudio2()
{
		SelectBus(BUS_A);
		I2C_OutBuff[0]=0x12;	//GPIO reg for A
		//CurrentFX_A=Value&0x1f;	//just save the actuation bits
#ifdef COMBO_UNIT
		if(CurrentFXMute)
		{
			I2C_OutBuff[1]=RelayStates & 0xf8;	//clear lower bits that are related to fxaudio
			I2C_OutBuff[1]=I2C_OutBuff[1] | 0x00;	//mute on
		}
		else
		{
			I2C_OutBuff[1]=RelayStates & 0xf8;	//clear lower bits that are related to fxaudio
			I2C_OutBuff[1]=I2C_OutBuff[1] | 0x02;	//mute off
		}

#else
		if(CurrentFXMute)
			I2C_OutBuff[1]=0x00;	//mute on
		else
			I2C_OutBuff[1]=0x02;	//mute off
#endif
//		I2C_OutBuff[2]=Value;	
		
		result = io_out(ioI2C, &I2C_OutBuff, FX_AUDIO_ADDR, 2);
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,FX_AUDIO_I2C);
		return(result);
	
}


when(timer_expires(CheckFXPlayContact))
{
	if(!ReadFXPlayContact())
	{
		SetAmpMute(TRUE);	//mute amp
//		AudioChannel=0;
//		SelectAudioChannel(AudioChannel);	
	}
	else
		CheckFXPlayContact=2;
	
	
}



when(timer_expires(FXBounce))
{
	WriteFXAudio(0xff); //release
	//WriteFXAudio2(0xff); //release

}

void SetAmpMute(boolean Mute)
{
		if(Mute)
			CurrentFXMute=TRUE;	
			//CurrentFX_A=CurrentFX_A&0xbf; // mute on
		else
			CurrentFXMute=FALSE;	
//			CurrentFX_A=CurrentFX_A|0x40; //mute off
		WriteFXAudio2();
	
}


boolean ReadFXPlayContact()
{
//support for mcp23017

	boolean result;
	unsigned long Buttons;
	boolean RetVal;
	
	SelectBus(BUS_A);
	I2C_OutBuff[0]=0x12;	// this is the address of GPIOA. B is right after it. So, just do a write to set teh address register to 12
	result = io_out(ioI2C, &I2C_OutBuff, FX_AUDIO_ADDR, 1);	// this just sends the address byte. no data bytes are written
			if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,FX_AUDIO_I2C);

	result = io_in(ioI2C, I2C_InBuff, FX_AUDIO_ADDR, 1);	// now read in the A gpio register
		if(!result)
			CPUFault(DEVICE_ID_1UHE_IO,FX_AUDIO_I2C);

	if((I2C_InBuff[0]&0x01)==0)
	{
		RetVal=TRUE;

	}
	else
		RetVal=FALSE;
	
	return RetVal;
	
}

void PreInstall(void)
{
	//check to see if this is a virgin neuron. If so, set up address tables.

	
		//setup incoming fault ping from keypad
	nv_copy = *(access_nv(nv_table_index(NM_Request))); // look at selh to see if it is already set
	
	if(nv_copy.nv_selector_hi != FAULT_PING_SEL_HI)
	{
		nv_copy.nv_selector_hi=FAULT_PING_SEL_HI;// modify the selector so the ilon can talk to it
		nv_copy.nv_selector_lo=FAULT_PING_SEL_LOW;
		update_nv(&nv_copy,nv_table_index(NM_Request));	//write it back
		

	}

}




